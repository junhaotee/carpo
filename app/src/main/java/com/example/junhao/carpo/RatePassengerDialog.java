package com.example.junhao.carpo;

import android.app.Activity;
import android.content.DialogInterface;
//import android.media.Image;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
//import android.app.Fragment;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;


public class RatePassengerDialog extends DialogFragment {

    boolean         firstClicked=false;
    Spinner         rate_passenger_dialog_spinner_comment_example;
    Button          rate_passenger_dialog_btn_confirm_rating;
    String          user_given_rating="p_rating_good";
    EditText        rate_passenger_dialog_comment;
    ArrayList       commentExampleList;
    ArrayAdapter    commentExampleListAdapter;

    ImageView       rate_passenger_dialog_profile_pic,
                    rate_passenger_iv_rating_bad,
                    rate_passenger_iv_rating_average,
                    rate_passenger_iv_rating_good;

    TextView        rate_passenger_dialog_profile_verified,
                    rate_passenger_dialog_profile_driver_rate,
                    rate_passenger_dialog_name,
                    rate_passenger_dialog_driver_hometown,
                    rate_passenger_dialog_profile_contact,
                    rate_passenger_dialog_reply_rate,
                    rate_passenger_dialog_reply_within_hour,
                    rate_passenger_dialog_rating_count,
                    rate_passenger_tv_rating_stat;

    public RatePassengerDialog() { }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_rate_passenger_dialog, container, false);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);

        getDialog().setTitle("聯絡乘客");

        setupViews();

        setupViewsData();

    }

    void setupViews(){

        commentExampleList                              = new ArrayList();
        commentExampleListAdapter                       = new ArrayAdapter(getContext(),R.layout.custom_drawer_item,commentExampleList);

        rate_passenger_dialog_comment                   = (EditText)getView().findViewById(R.id.rate_passenger_dialog_comment);

        rate_passenger_dialog_profile_pic               = (ImageView)getView().findViewById(R.id.rate_passenger_dialog_profile_pic);
        rate_passenger_dialog_profile_verified          = (TextView)getView().findViewById(R.id.rate_passenger_dialog_profile_verified);
        rate_passenger_dialog_profile_driver_rate       = (TextView)getView().findViewById(R.id.rate_passenger_dialog_profile_driver_rate);
        rate_passenger_dialog_name                      = (TextView)getView().findViewById(R.id.rate_passenger_dialog_name);
        rate_passenger_dialog_driver_hometown           = (TextView)getView().findViewById(R.id.rate_passenger_dialog_driver_hometown);
        rate_passenger_dialog_profile_contact           = (TextView)getView().findViewById(R.id.rate_passenger_dialog_profile_contact);
        rate_passenger_dialog_reply_rate                = (TextView)getView().findViewById(R.id.rate_passenger_dialog_reply_rate);
        rate_passenger_dialog_reply_within_hour         = (TextView)getView().findViewById(R.id.rate_passenger_dialog_reply_within_hour);
        rate_passenger_dialog_rating_count              = (TextView)getView().findViewById(R.id.rate_passenger_dialog_rating_count);
        rate_passenger_tv_rating_stat                   = (TextView)getView().findViewById(R.id.rate_passenger_tv_rating_stat);

        rate_passenger_iv_rating_bad                    = (ImageView)getView().findViewById(R.id.rate_passenger_iv_rating_bad);
        rate_passenger_iv_rating_average                = (ImageView)getView().findViewById(R.id.rate_passenger_iv_rating_average);
        rate_passenger_iv_rating_good                   = (ImageView)getView().findViewById(R.id.rate_passenger_iv_rating_good);

        rate_passenger_dialog_spinner_comment_example   = (Spinner)getView().findViewById(R.id.rate_passenger_dialog_spinner_comment_example);

        rate_passenger_dialog_btn_confirm_rating        = (Button)getView().findViewById(R.id.rate_passenger_dialog_btn_confirm_rating);
    }

    void setupViewsData(){

        new GetProfilePic().execute(CheckPassengerHistoryTab.chosenPassengerHistoryItem.getItemValue("owner"), rate_passenger_dialog_profile_pic);

        String name             = CheckPassengerHistoryTab.chosenPassengerHistoryItem.getItemValue("fName")+ CheckPassengerHistoryTab.chosenPassengerHistoryItem.getItemValue("lName");
        String showLine         = (Integer.parseInt(CheckPassengerHistoryTab.chosenPassengerHistoryItem.getItemValue("showLine"))==1)?"賴:"+ CheckPassengerHistoryTab.chosenPassengerHistoryItem.getItemValue("line"):"";
        String verified         = (Integer.parseInt(CheckPassengerHistoryTab.chosenPassengerHistoryItem.getItemValue("verified"))==1)?"✓ 手機認證":"手機未認證";
        String driver_hometown  = CheckPassengerHistoryTab.chosenPassengerHistoryItem.getItemValue("hometown_name");
        if((Integer.parseInt(CheckPassengerHistoryTab.chosenPassengerHistoryItem.getItemValue("verified"))!=1))
            rate_passenger_dialog_profile_verified.setTextColor(ContextCompat.getColor(getContext(), R.color.grey));

        rate_passenger_dialog_name.setText(name);
        rate_passenger_dialog_profile_contact.setText(showLine);
        rate_passenger_dialog_profile_verified.setText(verified);
        rate_passenger_dialog_driver_hometown.setText(driver_hometown);

        getDialog().setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                getTargetFragment().onActivityResult(-1, -1, null);
            }
        });

        rate_passenger_iv_rating_good.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetAllRatingImageAlpha();
                rate_passenger_iv_rating_good.setAlpha((float) 1.0);
                rate_passenger_tv_rating_stat.setText("給予乘客好評");
                user_given_rating="p_rating_good";
                rate_passenger_dialog_spinner_comment_example.setSelection(0);
            }
        });

        rate_passenger_iv_rating_average.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetAllRatingImageAlpha();
                rate_passenger_iv_rating_average.setAlpha((float) 1.0);
                rate_passenger_tv_rating_stat.setText("給予乘客中評");
                user_given_rating="p_rating_average";
                rate_passenger_dialog_spinner_comment_example.setSelection(1);
            }
        });

        rate_passenger_iv_rating_bad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetAllRatingImageAlpha();
                rate_passenger_iv_rating_bad.setAlpha((float) 1.0);
                rate_passenger_tv_rating_stat.setText("給予乘客差評");
                user_given_rating="p_rating_bad";
                rate_passenger_dialog_spinner_comment_example.setSelection(2);
            }
        });

        rate_passenger_dialog_spinner_comment_example.setAdapter(commentExampleListAdapter);
        commentExampleList.add("推薦的好乘客，會選擇再次共乘");
        commentExampleList.add("基本上是個好乘客，但還有共乘的經驗還有進步空間");
        commentExampleList.add("與乘客的共乘經驗很不愉快");
        commentExampleListAdapter.notifyDataSetChanged();

        rate_passenger_dialog_spinner_comment_example.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                TextView chosenItem=(TextView)view;
                rate_passenger_dialog_comment.setText(chosenItem.getText());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) { }
        });

        rate_passenger_dialog_btn_confirm_rating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!firstClicked){
                    firstClicked=true;
                    rate_passenger_dialog_btn_confirm_rating.setText("再次點擊確認");
                    return;
                }

                Intent commentData=new Intent();
                try {
                    commentData.putExtra("user_comment", URLEncoder.encode(rate_passenger_dialog_comment.getText()+"", "UTF-8"));
                    commentData.putExtra("user_given_rating",user_given_rating);
                    commentData.putExtra("fk_carpool_id", CheckPassengerHistoryTab.chosenPassengerHistoryItem.getItemValue("cId"));
                    commentData.putExtra("fk_user_fb_rates",MainActivity.fb_profile_id);
                    commentData.putExtra("fk_user_fb_rated", CheckPassengerHistoryTab.chosenPassengerHistoryItem.getItemValue("fk_user_fb"));
                    commentData.putExtra("fb", CheckPassengerHistoryTab.chosenPassengerHistoryItem.getItemValue("fk_user_fb"));
                    commentData.putExtra("passenger_request_id", CheckPassengerHistoryTab.chosenPassengerHistoryItem.getItemValue("passenger_request_id"));

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                getTargetFragment().onActivityResult(getTargetRequestCode(),getTargetRequestCode(),commentData);
                getDialog().dismiss();
            }
        });
    }



    void resetAllRatingImageAlpha(){
        rate_passenger_iv_rating_bad.setAlpha((float)0.05);
        rate_passenger_iv_rating_average.setAlpha((float)0.05);
        rate_passenger_iv_rating_good.setAlpha((float)0.05);
    }


    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onPause(){
        super.onPause();
        dismiss();
    }

    class GetProfilePic extends AsyncTask<Object,Void,Bitmap> {

        ImageView fb_profile_pic;
        String fbId;

        @Override
        protected Bitmap doInBackground(Object... params) {
            Bitmap profilePic=null;
            fb_profile_pic=(ImageView)params[1];
            fbId=(String)params[0];

            try {

                URL profileURL=new URL("https://graph.facebook.com/"+fbId+"/picture?type=square");
                Log.d("pp", "https://graph.facebook.com/" + fbId + "/picture?type=square");
                URLConnection urlConn=profileURL.openConnection();
                urlConn.setReadTimeout(1500);
                urlConn.setConnectTimeout(1500);
                profilePic= BitmapFactory.decodeStream((InputStream) urlConn.getContent());

            }
            catch (MalformedURLException e) { e.printStackTrace(); }
            catch (IOException e) { e.printStackTrace(); }

            return profilePic;
        }

        @Override
        protected void onPostExecute(Bitmap profilePic){
            if(profilePic==null) return;

            fb_profile_pic.setImageBitmap(profilePic);

        }
    }
}
