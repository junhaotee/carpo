package com.example.junhao.carpo;


import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class CarpoolRuleDialog extends DialogFragment {

    RadioGroup  carpool_rule_dialog_food,
                carpool_rule_dialog_luggage,
                carpool_rule_dialog_stopby,
                carpool_rule_dialog_payment,
                carpool_rule_dialog_identity;

    RadioButton carpool_rule_dialog_food_yes,
                carpool_rule_dialog_food_no,
                carpool_rule_dialog_luggage_yes,
                carpool_rule_dialog_luggage_no,
                carpool_rule_dialog_stopby_yes,
                carpool_rule_dialog_stopby_no,
                carpool_rule_dialog_payment_yes,
                carpool_rule_dialog_payment_no,
                carpool_rule_dialog_identity_yes,
                carpool_rule_dialog_identity_no;

    Button      carpool_rule_dialog_savebtn;

    public CarpoolRuleDialog() { }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_carpool_rule_dialog, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){

        super.onActivityCreated(savedInstanceState);
        getDialog().setTitle(getString(R.string.carpool_preference));
        getDialog().setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                getTargetFragment().onActivityResult(-1, -1, null);
            }
        });

        setupViews();

        setupListeners();

        setupDefaultTicketIfAvaiable();

    }

    public void setupViews(){

        carpool_rule_dialog_savebtn     = (Button)getView().findViewById(R.id.carpool_rule_dialog_savebtn);
        carpool_rule_dialog_food_yes    = (RadioButton)getView().findViewById(R.id.carpool_rule_dialog_food_yes);
        carpool_rule_dialog_food_no     = (RadioButton)getView().findViewById(R.id.carpool_rule_dialog_food_no);
        carpool_rule_dialog_luggage_yes = (RadioButton)getView().findViewById(R.id.carpool_rule_dialog_luggage_yes);
        carpool_rule_dialog_luggage_no  = (RadioButton)getView().findViewById(R.id.carpool_rule_dialog_luggage_no);
        carpool_rule_dialog_stopby_yes  = (RadioButton)getView().findViewById(R.id.carpool_rule_dialog_stopby_yes);
        carpool_rule_dialog_stopby_no   = (RadioButton)getView().findViewById(R.id.carpool_rule_dialog_stopby_no);
        carpool_rule_dialog_payment_yes = (RadioButton)getView().findViewById(R.id.carpool_rule_dialog_payment_yes);
        carpool_rule_dialog_payment_no  = (RadioButton)getView().findViewById(R.id.carpool_rule_dialog_payment_no);
        carpool_rule_dialog_identity_yes= (RadioButton)getView().findViewById(R.id.carpool_rule_dialog_identity_yes);
        carpool_rule_dialog_identity_no = (RadioButton)getView().findViewById(R.id.carpool_rule_dialog_identity_no);

    }

    View.OnClickListener saveBtnListener=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent data=new Intent();

            data.putExtra("carpool_food",carpool_rule_dialog_food_yes.isChecked());
            data.putExtra("carpool_luggage",carpool_rule_dialog_luggage_yes.isChecked());
            data.putExtra("carpool_stopby",carpool_rule_dialog_stopby_yes.isChecked());
            data.putExtra("carpool_payafterward",carpool_rule_dialog_payment_yes.isChecked());
            data.putExtra("carpool_identity_check",carpool_rule_dialog_identity_yes.isChecked());

            getTargetFragment().onActivityResult(getTargetRequestCode(),getTargetRequestCode(),data);
            getDialog().dismiss();
        }
    };

    void setupListeners(){
        carpool_rule_dialog_savebtn.setOnClickListener(saveBtnListener);
    }

    void setupDefaultTicketIfAvaiable(){
        //immeidate return if no previous state saved
        if(!CreateCarpoolInvitation.carpoolData.hasExtra("carpool_food"))return;

        carpool_rule_dialog_food_yes.setChecked(CreateCarpoolInvitation.carpoolData.getExtras().getBoolean("carpool_food"));
        carpool_rule_dialog_food_no.setChecked(!carpool_rule_dialog_food_yes.isChecked());

        carpool_rule_dialog_luggage_yes.setChecked(CreateCarpoolInvitation.carpoolData.getExtras().getBoolean("carpool_luggage"));
        carpool_rule_dialog_luggage_no.setChecked(!carpool_rule_dialog_luggage_yes.isChecked());

        carpool_rule_dialog_stopby_yes.setChecked(CreateCarpoolInvitation.carpoolData.getExtras().getBoolean("carpool_stopby"));
        carpool_rule_dialog_stopby_no.setChecked(!carpool_rule_dialog_stopby_yes.isChecked());

        carpool_rule_dialog_payment_yes.setChecked(CreateCarpoolInvitation.carpoolData.getExtras().getBoolean("carpool_payafterward"));
        carpool_rule_dialog_payment_no.setChecked(!carpool_rule_dialog_payment_yes.isChecked());

        carpool_rule_dialog_identity_yes.setChecked(CreateCarpoolInvitation.carpoolData.getExtras().getBoolean("carpool_identity_check"));
        carpool_rule_dialog_identity_no.setChecked(!carpool_rule_dialog_identity_yes.isChecked());

    }
}
