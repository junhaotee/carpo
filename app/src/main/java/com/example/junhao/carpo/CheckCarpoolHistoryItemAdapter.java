package com.example.junhao.carpo;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;


public class CheckCarpoolHistoryItemAdapter extends ArrayAdapter<CheckCarpoolHistoryItem> {

    RelativeLayout container;

    public CheckCarpoolHistoryItemAdapter(Context context, ArrayList listItems) {
        super(context, R.layout.carpool_history_item, listItems);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){

        View row = convertView;
        CheckCarpoolHistoryItemViewHolder holder;

        if(row==null) {
            LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(getContext().LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.carpool_history_item, parent, false);
            holder = new CheckCarpoolHistoryItemViewHolder(row);
            row.setTag(holder);
        }else {
            holder = (CheckCarpoolHistoryItemViewHolder)row.getTag();
        }

        //if no result if found
        if(getItem(position).isItemNull()){
            return row;
        }

        container=holder.rlayout;

        /* Set data to views */

        holder.carpool_history_item_src_name.setText(getItem(position).getItemValue("start"));
        holder.carpool_history_item_dst_name.setText(getItem(position).getItemValue("destination"));

        setupDateAndTime(getItem(position).getItemValue("timeDepart"), holder.carpool_history_item_dom_1, holder.carpool_history_item_month_1, holder.carpool_history_item_hour_1, holder.carpool_history_item_min_1, holder.carpool_history_item_ampm_1);

        int hasInterval=Integer.parseInt(getItem(position).getItemValue("hasInterval"));

        if(hasInterval==1){ //has an interval then set text, else set visibility to gone
            Log.d("hasInterval","executed");
            setupDateAndTime(getItem(position).getItemValue("timeDepart2").toString(), holder.carpool_history_item_dom_1, holder.carpool_history_item_month_1, holder.carpool_history_item_hour_2, holder.carpool_history_item_min_2, holder.carpool_history_item_ampm_2);
            holder.carpool_history_item_interval_2.setVisibility(View.VISIBLE);
        }else{
            holder.carpool_history_item_interval_2.setVisibility(View.GONE);
        }

        String isTimeNegoable=(Integer.parseInt(getItem(position).getItemValue("isAnytime"))==1)?"任意時間":(Integer.parseInt(getItem(position).getItemValue("timeNego"))==1)?"可議":"";
        holder.carpool_history_item_time_is_negoable.setText(isTimeNegoable);

//        holder.carpool_history_item_vehicle_name.setText(getItem(position).getItemValue("vehicle_name"));

        int totalCost=Integer.parseInt(getItem(position).getItemValue("costpp"))*Integer.parseInt(getItem(position).getItemValue("num_seat_requested"));
        holder.carpool_history_item_totalcost.setText(totalCost+"");

        holder.carpool_history_item_seat_count.setText(getItem(position).getItemValue("num_seat_requested"));

        holder.carpool_history_item_passenger_name.setText(getItem(position).getItemValue("fName")+getItem(position).getItemValue("lName"));

        String ratingStatus="";

        if(getItem(position).getItemValue("request_status").toString().equals("driver_approved")){

            if(getItem(position).getItemValue("fk_user_rating_id_on_driver").toString().equals("0")){
                ratingStatus="ℹ 車主待評";
                holder.carpool_history_item_rating_status.setTextColor(Color.parseColor("#ffbf00"));
            }else{
                Log.d("rating",getItem(position).getItemValue("user_given_rating").toString());
                if(getItem(position).getItemValue("user_given_rating").toString().equals("d_rating_good")){
                    ratingStatus="\uD83D\uDE04 給予很好評價\n\""+getItem(position).getItemValue("user_comment").toString()+"\"";
                    holder.carpool_history_item_rating_status.setTextColor(Color.parseColor("#009933"));
                }else if(getItem(position).getItemValue("user_given_rating").toString().equals("d_rating_average")){
                    ratingStatus="\uD83D\uDE13 給予普通評價\n\""+getItem(position).getItemValue("user_comment").toString()+"\"";
                    holder.carpool_history_item_rating_status.setTextColor(Color.parseColor("#663300"));
                }else if(getItem(position).getItemValue("user_given_rating").toString().equals("d_rating_bad")){
                    ratingStatus="\uD83D\uDE24 給予不滿意評價\n\""+getItem(position).getItemValue("user_comment").toString()+"\"";
                    holder.carpool_history_item_rating_status.setTextColor(Color.parseColor("#ff704d"));
                }
            }
        }else if(getItem(position).getItemValue("request_status").toString().equals("driver_declined")){
            ratingStatus="座位已滿";
        }else if(getItem(position).getItemValue("request_status").toString().equals("driver_canceled")){
            ratingStatus="已被司機取消";
            holder.carpool_history_item_rating_status.setTextColor(Color.parseColor("#ff704d"));
        }else if(getItem(position).getItemValue("request_status").toString().equals("passenger_canceled")){
            ratingStatus="已取消";
            holder.carpool_history_item_rating_status.setTextColor(Color.parseColor("#ff704d"));
        }else if(getItem(position).getItemValue("request_status").toString().equals("passenger_canceled_after_approved")){
            ratingStatus="已臨時取消";
            holder.carpool_history_item_rating_status.setTextColor(Color.parseColor("#ff704d"));
        }

        holder.carpool_history_item_rating_status.setText(ratingStatus);

        holder.carpool_history_item_request_timeago.setText(TimeAgo.getTimeAgo(getItem(position).getItemValue("date_passenger_request")));

        //pass user facebook id and imageview of the profile picture
        new GetProfilePic().execute(getItem(position).getItemValue("fk_user_fb"), holder.carpool_history_item_fb_profile_pic);

        return row;
    }

    public void setupDateAndTime(String datetimeFormat,TextView dom,TextView month,TextView hour,TextView minute,TextView ampm){

        Log.d("datetimeFormat",datetimeFormat);

        //string processing to extract ymd hms
        String[] dateTime=datetimeFormat.split(" ");
        String[] ymd=dateTime[0].split("-");
        String[] hms=dateTime[1].split(":");

        //find day of week
        // Note that Month value is 0-based. e.g., 0 for January.
        Calendar calendar = new GregorianCalendar(Integer.parseInt(ymd[0]),Integer.parseInt(ymd[1])-1,Integer.parseInt(ymd[2]));
        int day = calendar.get(Calendar.DAY_OF_WEEK);

        String dow="";
        switch (day) {
            case Calendar.MONDAY:{dow="一";break;}
            case Calendar.TUESDAY:{dow="二";break;}
            case Calendar.WEDNESDAY:{dow="三";break;}
            case Calendar.THURSDAY:{dow="四";break;}
            case Calendar.FRIDAY:{dow="五";break;}
            case Calendar.SATURDAY:{dow="六";break;}
            case Calendar.SUNDAY:{dow="日";break;}
        }

        //removing 0 prefix for month,day,hour,minute
        if(Integer.parseInt(ymd[1].charAt(0)+"")==0)
            ymd[1]=ymd[1].charAt(1)+"";

        if(Integer.parseInt(ymd[2].charAt(0)+"")==0)
            ymd[2]=ymd[2].charAt(1)+"";

        if(Integer.parseInt(hms[0].charAt(0) + "")==0)
            hms[0]=hms[0].charAt(1)+"";

        //detect AM or PM
        String AMPM=(Integer.parseInt(hms[0])>=12)?"PM":"AM";

        //make hour 13 to 1
        if(Integer.parseInt(hms[0])>=13){
            hms[0]=" "+(Integer.parseInt(hms[0])-12)+"";
        }

        //set formatted date output to ui
        dom.setText("("+dow+") "+ymd[2]);
        month.setText(ymd[1]);
        hour.setText(hms[0]);
        minute.setText(hms[1]);
        ampm.setText(AMPM);

    }

    class GetProfilePic extends AsyncTask<Object,Void,Bitmap> {

        ImageView fb_profile_pic;
        String fbId;

        @Override
        protected Bitmap doInBackground(Object... params) {
            Bitmap profilePic=null;
            fb_profile_pic=(ImageView)params[1];
            fbId=(String)params[0];

            try {

                URL profileURL=new URL("https://graph.facebook.com/"+fbId+"/picture?type=square");
                Log.d("pp","https://graph.facebook.com/"+fbId+"/picture?type=square");
                URLConnection urlConn=profileURL.openConnection();
                urlConn.setReadTimeout(1500);
                urlConn.setConnectTimeout(1500);
                profilePic= BitmapFactory.decodeStream((InputStream) urlConn.getContent());

            }
            catch (MalformedURLException e) { e.printStackTrace(); }
            catch (IOException e) { e.printStackTrace(); }

            return profilePic;
        }

        @Override
        protected void onPostExecute(Bitmap profilePic){
            if(profilePic==null) return;
            fb_profile_pic.setImageBitmap(profilePic);
        }
    }
}

class CheckCarpoolHistoryItemViewHolder {

    TextView
            carpool_history_item_src_name,
            carpool_history_item_dst_name,
            carpool_history_item_dom_1,
            carpool_history_item_month_1,
            carpool_history_item_hour_1,
            carpool_history_item_min_1,
            carpool_history_item_ampm_1,
            carpool_history_item_hour_2,
            carpool_history_item_min_2,
            carpool_history_item_ampm_2,
            carpool_history_item_time_is_negoable,
            carpool_history_item_vehicle_name,
            carpool_history_item_totalcost,
            carpool_history_item_seat_count,
            carpool_history_item_request_timeago,
            carpool_history_item_passenger_name,
            carpool_history_item_rating_status;

    ImageView
            carpool_history_item_fb_profile_pic;

    RelativeLayout
            rlayout;

    LinearLayout
            carpool_history_item_interval_2;

    CheckCarpoolHistoryItemViewHolder(View v) {

        carpool_history_item_src_name=(TextView)v.findViewById(R.id.carpool_history_item_src_name);
        carpool_history_item_dst_name=(TextView)v.findViewById(R.id.carpool_history_item_dst_name);
        carpool_history_item_dom_1=(TextView)v.findViewById(R.id.carpool_history_item_dom_1);
        carpool_history_item_month_1=(TextView)v.findViewById(R.id.carpool_history_item_month_1);
        carpool_history_item_hour_1=(TextView)v.findViewById(R.id.carpool_history_item_hour_1);
        carpool_history_item_min_1=(TextView)v.findViewById(R.id.carpool_history_item_min_1);
        carpool_history_item_ampm_1=(TextView)v.findViewById(R.id.carpool_history_item_ampm_1);
        carpool_history_item_hour_2=(TextView)v.findViewById(R.id.carpool_history_item_hour_2);
        carpool_history_item_min_2=(TextView)v.findViewById(R.id.carpool_history_item_min_2);
        carpool_history_item_ampm_2=(TextView)v.findViewById(R.id.carpool_history_item_ampm_2);
        carpool_history_item_time_is_negoable=(TextView)v.findViewById(R.id.carpool_history_item_time_is_negoable);
        carpool_history_item_vehicle_name=(TextView)v.findViewById(R.id.carpool_history_item_vehicle_name);
        carpool_history_item_totalcost=(TextView)v.findViewById(R.id.carpool_history_item_totalcost);
        carpool_history_item_seat_count=(TextView)v.findViewById(R.id.carpool_history_item_seat_count);
        carpool_history_item_passenger_name=(TextView)v.findViewById(R.id.carpool_history_item_passenger_name);
        carpool_history_item_request_timeago=(TextView)v.findViewById(R.id.carpool_history_item_request_timeago);
        carpool_history_item_rating_status=(TextView)v.findViewById(R.id.carpool_history_item_rating_status);

        carpool_history_item_fb_profile_pic=(ImageView)v.findViewById(R.id.carpool_history_item_fb_profile_pic);
        rlayout=(RelativeLayout)v.findViewById(R.id.rlayout);

        carpool_history_item_interval_2 = (LinearLayout) v.findViewById(R.id.carpool_history_item_interval_2);

    }
}