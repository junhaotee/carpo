package com.example.junhao.carpo;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class JoinCarpool extends Fragment {

    Fragment    currentFragment=this;
    GoogleMap   carpoolMap;
    ImageView   jonicarpool_owner_profile_pic;
    ProgressDialog dialog;

    Button      back_to_list_btn,
                make_reservation_btn,
                joincarpool_driver_profile_btn,
                animate_to_boundaries,
                animate_to_src,
                animate_to_dst;

    TextView    src,
                dst,
                seatLeftView,
                costPp,
                cartype,
                carpool_comment,
                carpool_datetime,
                joincarpool_driver_preferences,
                joincarpool_driver_name;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {

        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_joincarpool, null);

        //get fb name and set
        joincarpool_driver_profile_btn =(Button)root.findViewById(R.id.joincarpool_driver_profile_btn);

        src=(TextView)root.findViewById(R.id.joincarpoolSrc);
        src.setText("["+SearchResultMap.chosenItem.getItemValue("src_city_name")+" "+SearchResultMap.chosenItem.getItemValue("src_town_name")+"] "+SearchResultMap.chosenItem.getItemValue("start"));

        dst=(TextView)root.findViewById(R.id.joincarpoolDst);
        dst.setText("["+SearchResultMap.chosenItem.getItemValue("dst_city_name")+" "+SearchResultMap.chosenItem.getItemValue("dst_town_name")+"] "+SearchResultMap.chosenItem.getItemValue("destination"));

        carpool_datetime=(TextView)root.findViewById(R.id.carpool_datetime);
        String isTimeNegoable=(Integer.parseInt(SearchResultMap.chosenItem.getItemValue("isAnytime"))==1)?"(任意時間)":(Integer.parseInt(SearchResultMap.chosenItem.getItemValue("timeNego"))==1)?"(可議)":"";

        carpool_datetime.setText(formattedDatetime()+isTimeNegoable);

        seatLeftView=(TextView)root.findViewById(R.id.joincarpoolSeatleft);
        int seatLeft=Integer.parseInt(SearchResultMap.chosenItem.getItemValue("seatGiven"))-Integer.parseInt(SearchResultMap.chosenItem.getItemValue("seatTaken"));
        seatLeftView.setText(seatLeft+"");

        cartype=(TextView)root.findViewById(R.id.joincarpoolCartype);
        cartype.setText(SearchResultMap.chosenItem.getItemValue("vehicle_name"));

        costPp=(TextView)root.findViewById(R.id.joincarpoolCostpp);

        String isCostNego=(Integer.parseInt(SearchResultMap.chosenItem.getItemValue("isCostNego"))==1)?"(可議)":"";
        costPp.setText(" NTD " + SearchResultMap.chosenItem.getItemValue("costpp")+isCostNego);

        carpool_comment=(TextView)root.findViewById(R.id.carpool_comment);
        String comment=(SearchResultMap.chosenItem.getItemValue("comment").length()!=0)?"車主的話：“"+SearchResultMap.chosenItem.getItemValue("comment")+"”":"(車主沒留言)";
        carpool_comment.setText(comment);


        //call this to put database carpool markers on the map
        new LoadMapMarkers().execute();

        //make listeners for buttons
        back_to_list_btn=(Button)root.findViewById(R.id.back_to_list_btn);
        back_to_list_btn.setOnClickListener(backToListBtnListener);

        make_reservation_btn=(Button)root.findViewById(R.id.reserve_seat_btn);
        make_reservation_btn.setOnClickListener(makeReservationBtnListener);

        joincarpool_driver_profile_btn.setOnClickListener(checkDriverProfile);

        //listners for animate google map camera, must have google map ready
        animate_to_boundaries=(Button)root.findViewById(R.id.animate_to_boundaries);
        animate_to_boundaries.setOnClickListener(animate_to_boundaries_listener);

        animate_to_src=(Button)root.findViewById(R.id.animate_to_src);
        animate_to_src.setOnClickListener(animate_to_src_listener);

        animate_to_dst=(Button)root.findViewById(R.id.animate_to_dst);
        animate_to_dst.setOnClickListener(animate_to_dst_listener);

        joincarpool_driver_name=(TextView)root.findViewById(R.id.joincarpool_driver_name);
        joincarpool_driver_name.setText(SearchResultMap.chosenItem.getItemValue("fName")+SearchResultMap.chosenItem.getItemValue("lName"));

        joincarpool_driver_preferences =(TextView)root.findViewById(R.id.joincarpool_driver_preferences);
        joincarpool_driver_preferences.setText(setupPreferences()+"");

        jonicarpool_owner_profile_pic=(ImageView)root.findViewById(R.id.jonicarpool_owner_profile_pic);
        new GetProfilePic().execute(SearchResultMap.chosenItem.getItemValue("owner"), jonicarpool_owner_profile_pic);

        //TODO:remember implement double click to confirm reserveation
        return root;
    }

    String setupPreferences(){

        /*
            isFoodAllowed         "輕飲食許可":"不建議車上飲食"
            isBigLuggageAllowed   "有大件行李先詢問":"空間有限無法載送大件行李"
            isStopByAllowed       "需要時可短時間停靠":"沿途停靠先詢問車主"
            isPaymentAfterBefore  "下車後付款":"上車前付款"
            isIdentityCheckNeeded "上車前需要身份認證":""
        */

        String isFoodAllowed=(Integer.parseInt(SearchResultMap.chosenItem.getItemValue("isFoodAllowed"))==1)?"✓ 輕飲食許可":"✓ 不建議車上飲食";
        String isBigLuggageAllowed=(Integer.parseInt(SearchResultMap.chosenItem.getItemValue("isBigLuggageAllowed"))==1)?"✓ 有大件行李先詢問":"✓ 空間有限無法載送大件行李";
        String isStopByAllowed=(Integer.parseInt(SearchResultMap.chosenItem.getItemValue("isStopByAllowed"))==1)?"✓ 需要時可短時間停靠":"✓ 沿途停靠先詢問車主";
        String isPaymentAfterBefore=(Integer.parseInt(SearchResultMap.chosenItem.getItemValue("isPaymentAfterBefore"))==1)?"✓ 下車後付款":"✓ 上車前付款";
        String isIdentityCheckNeeded=(Integer.parseInt(SearchResultMap.chosenItem.getItemValue("isIdentityCheckNeeded"))==1)?"✓ 上車前需要身份認證":"";


        String driverPreferences=isFoodAllowed+"\n"+isBigLuggageAllowed+"\n"+isStopByAllowed+"\n"+isPaymentAfterBefore+"\n"+isIdentityCheckNeeded;
        return driverPreferences;

    }

    View.OnClickListener animate_to_boundaries_listener=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            //clear previous markers
            if (carpoolMap != null)
                carpoolMap.clear();

            Marker[] markerList = new Marker[2];

            //marker for dst
            Double dstlat = Double.parseDouble(SearchResultMap.chosenItem.getItemValue("dstlat"));
            Double dstlng = Double.parseDouble(SearchResultMap.chosenItem.getItemValue("dstlng"));
            LatLng dstLocation = new LatLng(dstlat, dstlng);
            markerList[0] = carpoolMap.addMarker(new MarkerOptions().position(dstLocation).draggable(true));

            //marker for src
            Double startlat = Double.parseDouble(SearchResultMap.chosenItem.getItemValue("startlat"));
            Double startlng = Double.parseDouble(SearchResultMap.chosenItem.getItemValue("startlng"));
            LatLng srcLocation = new LatLng(startlat, startlng);
            markerList[1] = carpoolMap.addMarker(new MarkerOptions().position(srcLocation).draggable(true));

            //create boundary for google map, so camera knows where to focus
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            for (Marker marker : markerList) {
                builder.include(marker.getPosition());
            }
            LatLngBounds bounds = builder.build();

            //int padding = getActivity().getResources().getDimensionPixelSize(R.dimen.home_map_padding);
            // 100 is the offset of markers from edges of the map in pixels
            //greater the number camera zoom is farther
            CameraUpdate cameraUpdateTwoMarkers = CameraUpdateFactory.newLatLngBounds(bounds, 100);
            carpoolMap.animateCamera(cameraUpdateTwoMarkers);

        }
    };

    View.OnClickListener animate_to_src_listener=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            LatLng coordinate = new LatLng(Double.parseDouble(SearchResultMap.chosenItem.getItemValue("startlat").toString()), Double.parseDouble(SearchResultMap.chosenItem.getItemValue("startlng").toString()));
            CameraUpdate startLocation = CameraUpdateFactory.newLatLngZoom(coordinate, 15);
            carpoolMap.animateCamera(startLocation);
        }
    };

    View.OnClickListener animate_to_dst_listener=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            LatLng coordinate = new LatLng(Double.parseDouble(SearchResultMap.chosenItem.getItemValue("dstlat").toString()), Double.parseDouble(SearchResultMap.chosenItem.getItemValue("dstlng").toString()));
            CameraUpdate dstLocation = CameraUpdateFactory.newLatLngZoom(coordinate, 15);
            carpoolMap.animateCamera(dstLocation);
        }
    };

    View.OnClickListener makeReservationBtnListener=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            DialogFragment confirmReservationDialog= new ConfirmReservationDialog();
            confirmReservationDialog.setTargetFragment(currentFragment,11);
            confirmReservationDialog.show(getFragmentManager(), "confirmation  dialog");
        }
    };

    View.OnClickListener checkDriverProfile=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Bundle driverProfileBundle=new Bundle();
            driverProfileBundle.putString("profileObj",SearchResultMap.chosenItem.getJSONObject().toString());

            DialogFragment driverProfileDialog= new DriverProfileDialog();
            driverProfileDialog.setTargetFragment(currentFragment,-1);
            driverProfileDialog.setArguments(driverProfileBundle);
            driverProfileDialog.show(getFragmentManager(),"profile  dialog");
        }
    };

    //onclick listener for back to list btn
    View.OnClickListener backToListBtnListener=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            getFragmentManager().beginTransaction()
                    .replace(R.id.container, new SearchResultMap())
                    .commit();
        }
    };

    public String formattedDatetime(){

        String formattedDatetime="";

        //string processing to extract ymd hms
        String[] dateTime=SearchResultMap.chosenItem.getItemValue("timeDepart").split(" ");
        String[] ymd=dateTime[0].split("-");
        String[] hms=dateTime[1].split(":");

        //find day of week
        // Note that Month value is 0-based. e.g., 0 for January.
        Calendar calendar = new GregorianCalendar(Integer.parseInt(ymd[0]),Integer.parseInt(ymd[1])-1,Integer.parseInt(ymd[2]));
        int day = calendar.get(Calendar.DAY_OF_WEEK);

        String dow="";
        switch (day) {
            case Calendar.MONDAY:{dow="一";break;}
            case Calendar.TUESDAY:{dow="二";break;}
            case Calendar.WEDNESDAY:{dow="三";break;}
            case Calendar.THURSDAY:{dow="四";break;}
            case Calendar.FRIDAY:{dow="五";break;}
            case Calendar.SATURDAY:{dow="六";break;}
            case Calendar.SUNDAY:{dow="日";break;}
        }

        //removing 0 prefix for month,day,hour,minute
        if(Integer.parseInt(ymd[1].charAt(0)+"")==0)
            ymd[1]=ymd[1].charAt(1)+"";

        if(Integer.parseInt(ymd[2].charAt(0)+"")==0)
            ymd[2]=ymd[2].charAt(1)+"";

        if(Integer.parseInt(hms[0].charAt(0) + "")==0)
            hms[0]=hms[0].charAt(1)+"";

        //detect AM or PM
        String AMPM=(Integer.parseInt(hms[0])>=12)?"PM":"AM";

        //make hour 13 to 1
        if(Integer.parseInt(hms[0])>=13){
            hms[0]=" "+(Integer.parseInt(hms[0])-12)+"";
        }

        //TODO:day difference with the carpool date

        String dateInterval2="";
        if(Integer.parseInt(SearchResultMap.chosenItem.getItemValue("hasInterval"))==1){

            String[] dateTime2=SearchResultMap.chosenItem.getItemValue("timeDepart2").split(" ");
            String[] ymd2=dateTime2[0].split("-");
            String[] hms2=dateTime2[1].split(":");

            if(Integer.parseInt(hms2[0].charAt(0) + "")==0)
                hms2[0]=hms2[0].charAt(1)+"";

            //detect AM or PM
            String AMPM2=(Integer.parseInt(hms2[0])>=12)?"PM":"AM";

            //make hour 13 to 1
            if(Integer.parseInt(hms2[0])>=13){
                hms2[0]=" "+(Integer.parseInt(hms2[0])-12)+"";
            }

            dateInterval2=" ~ "+hms2[0]+":"+hms2[1]+" "+AMPM2;

        }

        formattedDatetime=" "+ymd[1]+" 月 "+ymd[2]+" 日 ("+dow+") \n"+" "+hms[0]+":"+hms[1]+" "+AMPM+dateInterval2;

        //2月18日(六) 9:30 AM
        return formattedDatetime;
    }

    class LoadMapMarkers extends AsyncTask<Void,Void,Integer>{
        @Override
        protected Integer doInBackground(Void... params) {
            return null;
        }

        @Override
        protected void onPostExecute(Integer i){

            //get profile picture from fb.com
            //fb_profile_pic_driver_bmp= BitmapFactory.decodeStream((InputStream) new URL(fbProfile[0].getProfilePictureUri(50, 50).toString()).getContent());


            //get map view
            //carpoolMap = ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map)).getMap();
            SupportMapFragment mapFrag = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
            if(mapFrag==null)return;
            mapFrag.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {

                    carpoolMap = googleMap;
                    //trigger action after the map is properly loaded, if not you get view not complete errors
                    carpoolMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                        @Override
                        public void onMapLoaded() {

                            //clear previous markers
                            if (carpoolMap != null)
                                carpoolMap.clear();

                            Marker[] markerList = new Marker[2];

                            //marker for dst
                            Double dstlat = Double.parseDouble(SearchResultMap.chosenItem.getItemValue("dstlat"));
                            Double dstlng = Double.parseDouble(SearchResultMap.chosenItem.getItemValue("dstlng"));
                            LatLng dstLocation = new LatLng(dstlat, dstlng);
                            markerList[0] = carpoolMap.addMarker(new MarkerOptions().position(dstLocation).draggable(true));

                            //marker for src
                            Double startlat = Double.parseDouble(SearchResultMap.chosenItem.getItemValue("startlat"));
                            Double startlng = Double.parseDouble(SearchResultMap.chosenItem.getItemValue("startlng"));
                            LatLng srcLocation = new LatLng(startlat, startlng);
                            markerList[1] = carpoolMap.addMarker(new MarkerOptions().position(srcLocation).draggable(true));

                            //create boundary for google map, so camera knows where to focus
                            LatLngBounds.Builder builder = new LatLngBounds.Builder();
                            for (Marker marker : markerList) {
                                builder.include(marker.getPosition());
                            }
                            LatLngBounds bounds = builder.build();

                            //int padding = getActivity().getResources().getDimensionPixelSize(R.dimen.home_map_padding);
                            // 100 is the offset of markers from edges of the map in pixels
                            //greater the number camera zoom is farther
                            CameraUpdate cameraUpdateTwoMarkers = CameraUpdateFactory.newLatLngBounds(bounds, 100);
                            carpoolMap.animateCamera(cameraUpdateTwoMarkers);

                        }
                    });
                }
            });
        }
    }

    @Override
    public void onActivityResult(int requestCode,int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode){
            case -1:{MainActivity.dialog.hide();}break;
            case 11:{onConfirmReservationDialogReturn(data);}break;
        }
    }

    void onConfirmReservationDialogReturn(Intent data){

        dialog = ProgressDialog.show(getContext(), "發送共乘請求","發送中...", true);

        //order : carpool id, user id, seat requested
        String carpoolId=SearchResultMap.chosenItem.getItemValue("cId");
        String userId=MainActivity.fb_profile_id;
        String seatRequested=data.getExtras().getInt("joincarpoolPersonCount")+"";

        new PostRequestData().execute(carpoolId,userId,seatRequested);

    }

    class GetProfilePic extends AsyncTask<Object,Void,Bitmap>{

        ImageView fb_profile_pic;
        String fbId;

        @Override
        protected Bitmap doInBackground(Object... params) {
            Bitmap profilePic=null;
            fb_profile_pic=(ImageView)params[1];
            fbId=(String)params[0];

            try {

                URL profileURL=new URL("https://graph.facebook.com/"+fbId+"/picture?type=square");
                Log.d("pp", "https://graph.facebook.com/" + fbId + "/picture?type=square");
                URLConnection urlConn=profileURL.openConnection();
                urlConn.setReadTimeout(1500);
                urlConn.setConnectTimeout(1500);
                profilePic= BitmapFactory.decodeStream((InputStream) urlConn.getContent());

            }
            catch (MalformedURLException e) { e.printStackTrace(); }
            catch (IOException e) { e.printStackTrace(); }

            return profilePic;
        }

        @Override
        protected void onPostExecute(Bitmap profilePic){
            if(profilePic==null) return;

            fb_profile_pic.setImageBitmap(profilePic);

        }
    }

    class PostRequestData extends AsyncTask<Object,Void,Integer>{
        @Override
        protected Integer doInBackground(Object... params) {

            //order : carpool id, user id, seat requested
            int fk_carpool_id=Integer.parseInt(params[0]+"");
            String fk_user_fb=params[1].toString();
            int num_seat_requested=Integer.parseInt(params[2]+"");

            String data="";
            StringBuilder serverResponse = new StringBuilder();

            try{
                /* DATA PREPARATION SECTION: careful for the namings, might caught column name mismatch in databases */

                /* id of carpool user is joining */
                data = URLEncoder.encode("fk_carpool_id", "UTF-8") + "=" + URLEncoder.encode(fk_carpool_id+"", "UTF-8");

                /* id of the person who sent the request to join carpool */
                data +="&"+ URLEncoder.encode("fk_user_fb", "UTF-8") + "=" + URLEncoder.encode(fk_user_fb+"", "UTF-8");

                /* number of seat requested */
                data +="&"+ URLEncoder.encode("num_seat_requested", "UTF-8") + "=" + URLEncoder.encode(num_seat_requested+"", "UTF-8");

            } catch(UnsupportedEncodingException uee){ uee.printStackTrace(); }

            try{
                //Send data to server
                String queryServer="http://140.116.83.83/api/post_passenger_request.php";
                URLConnection conn = new URL(queryServer).openConnection();

                //must set timeout otherwise by default it wait for somewhile
                conn.setConnectTimeout(1000);
                conn.setReadTimeout(1000);

                conn.setDoOutput(true);

                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write( data );
                wr.flush();

                // setup buffer reader
                String bufferLine="";
                BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));

                // Read server response into serverResponse
                while((bufferLine = reader.readLine()) != null)
                    serverResponse.append(bufferLine + "\n");

                reader.close();
            } catch (MalformedURLException e) {
                e.printStackTrace();
                return 1;
            } catch (IOException e) {
                //show toast and return to CreateCarpoolInvitation
                Toast.makeText(getContext(),"發送請求出現問題，請多嘗試一下",Toast.LENGTH_SHORT).show();
                return 1;
            }

            //return 0 for OK, 1 for else responses
            int returnCode=Integer.parseInt(serverResponse.toString().trim());

            return (returnCode==0)?0:1;
        }

        @Override
        protected void onPostExecute(Integer i){

            if(i!=0){
                Toast.makeText(getContext(),"發送請求出現問題，請多嘗試一下",Toast.LENGTH_SHORT).show();
                return;
            }
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            dialog.dismiss();
            getFragmentManager().beginTransaction().replace(R.id.container, new SearchInterface()).commit();

        }
    }

}