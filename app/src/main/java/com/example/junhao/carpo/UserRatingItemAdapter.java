package com.example.junhao.carpo;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.Profile;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Objects;


public class UserRatingItemAdapter extends ArrayAdapter<UserRatingItem> {

    RelativeLayout container;

    public UserRatingItemAdapter(Context context, ArrayList listItems) {
        super(context, R.layout.user_rating_item, listItems);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){

        View row = convertView;
        UserRatingItemViewHolder holder;

        if(row==null) {
            LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(getContext().LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.user_rating_item, parent, false);
            holder = new UserRatingItemViewHolder(row);
            row.setTag(holder);
        }else {
            holder = (UserRatingItemViewHolder)row.getTag();
        }

        String ratingState="";
        int ratingDrawableId=0;

        switch(getItem(position).getItemValue("user_given_rating").toString()){
            case "p_rating_good":{
                ratingState="給予好評";
                ratingDrawableId=R.drawable.rating_good;
                break;
            }
            case "p_rating_average":{
                ratingState="給予中評";
                ratingDrawableId=R.drawable.rating_average;
                break;
            }
            case "p_rating_bad":{
                ratingState="給予差評";
                ratingDrawableId=R.drawable.rating_bad;
                break;
            }
            case "d_rating_good":{
                ratingState="給予好評";
                ratingDrawableId=R.drawable.rating_good;
                break;
            }
            case "d_rating_average":{
                ratingState="給予中評";
                ratingDrawableId=R.drawable.rating_average;
                break;
            }
            case "d_rating_bad":{
                ratingState="給予差評";
                ratingDrawableId=R.drawable.rating_bad;
                break;
            }
        }

        String rater        = getItem(position).getItemValue("fName")+getItem(position).getItemValue("lName");
        String raterComment = getItem(position).getItemValue("user_comment");
        String dateAgo      = TimeAgo.getTimeAgo(getItem(position).getItemValue("user_rating_timestamp").toString());

        holder.user_rating_item_state.setText(ratingState);
        holder.user_rating_item_rater.setText(rater);
        holder.user_rating_item_comment.setText(raterComment);
        holder.user_rating_item_dateAgo.setText(dateAgo);

        holder.user_rating_item_state_icon.setImageDrawable(ContextCompat.getDrawable(getContext(),ratingDrawableId));

        return row;
    }

    public void setupDateAndTime(String datetimeFormat,TextView dom,TextView month,TextView hour,TextView minute,TextView ampm){

        Log.d("datetimeFormat",datetimeFormat);

        //string processing to extract ymd hms
        String[] dateTime=datetimeFormat.split(" ");
        String[] ymd=dateTime[0].split("-");
        String[] hms=dateTime[1].split(":");

        //find day of week
        // Note that Month value is 0-based. e.g., 0 for January.
        Calendar calendar = new GregorianCalendar(Integer.parseInt(ymd[0]),Integer.parseInt(ymd[1])-1,Integer.parseInt(ymd[2]));
        int day = calendar.get(Calendar.DAY_OF_WEEK);

        String dow="";
        switch (day) {
            case Calendar.MONDAY:{dow="一";break;}
            case Calendar.TUESDAY:{dow="二";break;}
            case Calendar.WEDNESDAY:{dow="三";break;}
            case Calendar.THURSDAY:{dow="四";break;}
            case Calendar.FRIDAY:{dow="五";break;}
            case Calendar.SATURDAY:{dow="六";break;}
            case Calendar.SUNDAY:{dow="日";break;}
        }

        //removing 0 prefix for month,day,hour,minute
        if(Integer.parseInt(ymd[1].charAt(0)+"")==0)
            ymd[1]=ymd[1].charAt(1)+"";

        if(Integer.parseInt(ymd[2].charAt(0)+"")==0)
            ymd[2]=ymd[2].charAt(1)+"";

        if(Integer.parseInt(hms[0].charAt(0) + "")==0)
            hms[0]=hms[0].charAt(1)+"";

        //detect AM or PM
        String AMPM=(Integer.parseInt(hms[0])>=12)?"PM":"AM";

        //make hour 13 to 1
        if(Integer.parseInt(hms[0])>=13){
            hms[0]=" "+(Integer.parseInt(hms[0])-12)+"";
        }

        //set formatted date output to ui
        dom.setText("("+dow+") "+ymd[2]);
        month.setText(ymd[1]);
        hour.setText(hms[0]);
        minute.setText(hms[1]);
        ampm.setText(AMPM);

    }
}

class UserRatingItemViewHolder {

    TextView    user_rating_item_state,
                user_rating_item_rater,
                user_rating_item_comment,
                user_rating_item_dateAgo;

    ImageView   user_rating_item_state_icon;

    UserRatingItemViewHolder(View v) {

        user_rating_item_state=(TextView)v.findViewById(R.id.user_rating_item_state);
        user_rating_item_rater=(TextView)v.findViewById(R.id.user_rating_item_rater);
        user_rating_item_comment=(TextView)v.findViewById(R.id.user_rating_item_comment);
        user_rating_item_dateAgo=(TextView)v.findViewById(R.id.user_rating_item_dateAgo);

        user_rating_item_state_icon=(ImageView)v.findViewById(R.id.user_rating_item_state_icon);

    }
}


