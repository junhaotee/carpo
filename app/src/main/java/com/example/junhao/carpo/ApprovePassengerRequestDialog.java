package com.example.junhao.carpo;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;


public class ApprovePassengerRequestDialog extends DialogFragment {

    boolean                     isLoading           = false;
    boolean                     isItemMax           = false;
    int                         currentItemOffset   = 0;

    ImageView                   approve_passenger_dialog_profile_pic;
    ListView                    approve_passenger_dialog_rating_listview;
    UserRatingItemAdapter       ratingLvAdapter;
    ArrayList<UserRatingItem>   ratingCommentList;
    View                        footerView;

    Button          approve_passenger_dialog_approve_btn,
                    approve_passenger_dialog_reject_btn;

    TextView        approve_passenger_dialog_profile_verified,
                    approve_passenger_dialog_profile_passenger_rate,
                    approve_passenger_dialog_passenger_name,
                    approve_passenger_dialog_passenger_hometown,
                    approve_passenger_dialog_passenger_contact,
                    approve_passenger_dialog_reply_rate,
                    approve_passenger_dialog_reply_within_hour,
                    approve_passenger_dialog_rating_count,
                    approve_passenger_dialog_passenger_bio,
                    listview_footer_tv_stat;

    public ApprovePassengerRequestDialog() { }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_approve_passenger_request_dialog, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);

        /* Get views */
        setupViews();

        /* Retrieve data and set it to view for display */
        setupViewsData();
    }

    void setupViews(){
        approve_passenger_dialog_profile_pic        = (ImageView)getView().findViewById(R.id.approve_passenger_dialog_profile_pic);
        approve_passenger_dialog_rating_listview    = (ListView)getView().findViewById(R.id.approve_passenger_dialog_rating_listview);
        approve_passenger_dialog_profile_verified   = (TextView)getView().findViewById(R.id.approve_passenger_dialog_profile_verified);
        approve_passenger_dialog_profile_passenger_rate=(TextView)getView().findViewById(R.id.approve_passenger_dialog_profile_passenger_rate);
        approve_passenger_dialog_passenger_name     = (TextView)getView().findViewById(R.id.approve_passenger_dialog_passenger_name);
        approve_passenger_dialog_passenger_hometown = (TextView)getView().findViewById(R.id.approve_passenger_dialog_passenger_hometown);
        approve_passenger_dialog_passenger_contact  = (TextView)getView().findViewById(R.id.approve_passenger_dialog_passenger_contact);
        approve_passenger_dialog_reply_rate         = (TextView)getView().findViewById(R.id.approve_passenger_dialog_reply_rate);
        approve_passenger_dialog_reply_within_hour  = (TextView)getView().findViewById(R.id.approve_passenger_dialog_reply_within_hour);
        approve_passenger_dialog_rating_count       = (TextView)getView().findViewById(R.id.approve_passenger_dialog_rating_count);
        approve_passenger_dialog_passenger_bio      = (TextView)getView().findViewById(R.id.approve_passenger_dialog_passenger_bio);
        approve_passenger_dialog_approve_btn        = (Button)getView().findViewById(R.id.approve_passenger_dialog_approve_btn);
        approve_passenger_dialog_reject_btn         = (Button)getView().findViewById(R.id.approve_passenger_dialog_reject_btn);
        footerView                                  = ((LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.listview_footer, null, false);
        listview_footer_tv_stat                     = (TextView)footerView.findViewById(R.id.listview_footer_tv_stat);
    }

    View.OnClickListener approve_btn_listener=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            getTargetFragment().onActivityResult(getTargetRequestCode(),0, null);
            getDialog().dismiss();
        }
    };



    View.OnClickListener reject_btn_listener=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            getTargetFragment().onActivityResult(getTargetRequestCode(),1, null);
            getDialog().dismiss();
        }
    };

    void setupViewsData(){

        getDialog().setTitle("查看乘客資料");

        getDialog().setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                getTargetFragment().onActivityResult(-1, -1, null);
            }
        });

        approve_passenger_dialog_approve_btn.setOnClickListener(approve_btn_listener);
        approve_passenger_dialog_reject_btn.setOnClickListener(reject_btn_listener);

        String name             = ApprovePassengerRequestTab.chosenApprovePassengerRequestItem.getItemValue("fName")+ ApprovePassengerRequestTab.chosenApprovePassengerRequestItem.getItemValue("lName");
        String showLine         = (Integer.parseInt(ApprovePassengerRequestTab.chosenApprovePassengerRequestItem.getItemValue("showLine"))==1)?"賴:"+ ApprovePassengerRequestTab.chosenApprovePassengerRequestItem.getItemValue("line"):"";
        String verified         = (Integer.parseInt(ApprovePassengerRequestTab.chosenApprovePassengerRequestItem.getItemValue("verified"))==1)?"✓ 手機認證":"手機未認證";
        String bio              = (ApprovePassengerRequestTab.chosenApprovePassengerRequestItem.getItemValue("bio").length()!=0)? ApprovePassengerRequestTab.chosenApprovePassengerRequestItem.getItemValue("bio"):"(乘客太酷了，沒填自我簡介)";
        String passenger_hometown = ApprovePassengerRequestTab.chosenApprovePassengerRequestItem.getItemValue("hometown_name");
        int rating_count        = Integer.parseInt(ApprovePassengerRequestTab.chosenApprovePassengerRequestItem.getItemValue("p_rating_good_count"))+Integer.parseInt(ApprovePassengerRequestTab.chosenApprovePassengerRequestItem.getItemValue("p_rating_average_count"))+Integer.parseInt(ApprovePassengerRequestTab.chosenApprovePassengerRequestItem.getItemValue("p_rating_bad_count"));
        String desc_rating_count= (rating_count==0)?"乘客剛加入共乘":rating_count+"個來自司機的評價";

        approve_passenger_dialog_passenger_name.setText(name);
        approve_passenger_dialog_passenger_contact.setText(showLine);
        approve_passenger_dialog_profile_verified.setText(verified);
        approve_passenger_dialog_passenger_bio.setText(bio);
        approve_passenger_dialog_passenger_hometown.setText(passenger_hometown);
        approve_passenger_dialog_rating_count.setText(desc_rating_count);
        if((Integer.parseInt(ApprovePassengerRequestTab.chosenApprovePassengerRequestItem.getItemValue("verified"))!=1))
            approve_passenger_dialog_profile_verified.setTextColor(ContextCompat.getColor(getContext(), R.color.grey));

       /*
                TODO:need to develope a scheme for driver rating
                driver_profile_dialog_profile_driver_rate
                driver_profile_dialog_reply_rate
                driver_profile_dialog_reply_within_hour
        */

        //if rating comment is zero hide and section
        if(rating_count==0){

            LinearLayout approve_passenger_dialog_rating_listview_section=(LinearLayout)getView().findViewById(R.id.approve_passenger_dialog_rating_listview_section);
            approve_passenger_dialog_rating_listview_section.setVisibility(View.INVISIBLE);
        }else{//if there were any comments on the passenger, set it

            ratingCommentList                       = new ArrayList();
            ratingLvAdapter                         = new UserRatingItemAdapter(getContext(),ratingCommentList);
            approve_passenger_dialog_rating_listview.setAdapter(ratingLvAdapter);
            approve_passenger_dialog_rating_listview.setOnScrollListener(ratingItemLvListener);
            approve_passenger_dialog_rating_listview.addFooterView(footerView);
        }

        //isLoading=true;
        //new GetRatingResults().execute(ApprovePassengerRequestTab.chosenApprovePassengerRequestItem.getItemValue("fb"),currentItemOffset,"p");

        new GetProfilePic().execute(ApprovePassengerRequestTab.chosenApprovePassengerRequestItem.getItemValue("fb"),approve_passenger_dialog_profile_pic);
    }

    AbsListView.OnScrollListener ratingItemLvListener=new AbsListView.OnScrollListener() {
        @Override
        public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            int lastInScreen = firstVisibleItem + visibleItemCount;
            if ((lastInScreen == totalItemCount && !isLoading &&!isItemMax)) {
                isLoading = true;
                new GetRatingResults().execute(ApprovePassengerRequestTab.chosenApprovePassengerRequestItem.getItemValue("fb"),currentItemOffset,"p");
            }
        }

        @Override
        public void onScrollStateChanged(AbsListView absListView, int i) { }
    };

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onPause(){
        super.onPause();
        dismiss();
    }

    class GetProfilePic extends AsyncTask<Object,Void,Bitmap> {

        ImageView fb_profile_pic;
        String fbId;

        @Override
        protected Bitmap doInBackground(Object... params) {

            Bitmap profilePic=null;
            fb_profile_pic=(ImageView)params[1];
            fbId=(String)params[0];

            try {

                URL profileURL=new URL("https://graph.facebook.com/"+fbId+"/picture?type=square");
                Log.d("pp", "https://graph.facebook.com/" + fbId + "/picture?type=square");
                URLConnection urlConn=profileURL.openConnection();
                urlConn.setReadTimeout(1500);
                urlConn.setConnectTimeout(1500);
                profilePic= BitmapFactory.decodeStream((InputStream) urlConn.getContent());

            }
            catch (MalformedURLException e) { e.printStackTrace(); }
            catch (IOException e) { e.printStackTrace(); }

            return profilePic;
        }

        @Override
        protected void onPostExecute(Bitmap profilePic){
            if(profilePic==null) return;

            fb_profile_pic.setImageBitmap(profilePic);

        }
    }

    class GetRatingResults extends AsyncTask<Object,Void,Integer> {

        String resultItemList="";

        @Override
        protected Integer doInBackground(Object... params) {

            try {
                String  fk_user_fb_rated    = params[0]+"";
                String  role                = params[2]+"";
                int     currentItemOffset   = (int)params[1];

                String queryString      = "http://140.116.83.83/api/get_user_rating.php?fk_user_fb_rated="+fk_user_fb_rated+"&currentItemOffset="+currentItemOffset+"&role="+role;
                Log.d("queryString",queryString);
                URL url                 = new URL(queryString);
                URLConnection urlConn   = url.openConnection();

                urlConn.setReadTimeout(1000);
                urlConn.setConnectTimeout(1000);

                resultItemList=convertStreamToString(urlConn.getInputStream());

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                resultItemList=""; //if server connect/read timeout, return empty list
                e.printStackTrace();
            }
            return null;
        }

        //onPostExecute is UI thread, updat the views here
        @Override
        protected void onPostExecute(Integer result) {
            try{
                //if empty string returned from server(when server gone wrong internally)
                if(resultItemList.length()==0){
                    approve_passenger_dialog_rating_listview.setVisibility(View.GONE);
                    isItemMax=true;
                    return;
                }

                //parsed server response into json
                JSONArray userRatingArr=new JSONArray(resultItemList);

                //if no result is found
                if(userRatingArr.length()==0){
                    listview_footer_tv_stat.setText("");
                    isItemMax=true;
                }

                //if there were results, populates found result into arraylist
                for(int k=0;k<userRatingArr.length();k++){
                    JSONObject ratingObj = new JSONObject(userRatingArr.get(k).toString());
                    ratingCommentList.add(new UserRatingItem(ratingObj));
                }

                //notify listview data changed, and unblock dialog
                ratingLvAdapter.notifyDataSetChanged();
                currentItemOffset  += userRatingArr.length();
                isLoading           = false;

            }catch(JSONException jsone){ jsone.printStackTrace(); }
        }

        //method to convert input stream to string, used after reading input stream
        String convertStreamToString(java.io.InputStream is) {
            java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
            return s.hasNext() ? s.next() : "";
        }
    }
}
