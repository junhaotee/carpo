package com.example.junhao.carpo;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.login.widget.LoginButton;

public class NavigationDrawerFragment extends Fragment {

    private                 boolean         mFromSavedInstanceState;
    private                 boolean         mUserLearnedDrawer;
    private                 int             mCurrentSelectedPosition    = 0;

    private static final    String          STATE_SELECTED_POSITION     = "selected_navigation_drawer_position";
    private static final    String          PREF_USER_LEARNED_DRAWER    = "navigation_drawer_learned";
    private                 DrawerLayout    mDrawerLayout;
    private                 View            mFragmentContainerView;
    private                 NavigationDrawerCallbacks mCallbacks;
    private                 ActionBarDrawerToggle mDrawerToggle;

                            LinearLayout    drawerLinearLayout;
                            CallbackManager callbackManager;
                            ListView        navigation_drawer_passenger_listview,navigation_drawer_driver_listview;
                            ImageView       navigation_drawer_fb_profile_pic;
                            TextView        navigation_drawer_email,navigation_drawer_fb_name;
                            ArrayAdapter<String> passengerAdapter,driverAdapter;


    public NavigationDrawerFragment() { }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Read in the flag indicating whether or not the user has demonstrated awareness of the
        // drawer. See PREF_USER_LEARNED_DRAWER for details.
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getActivity());
        mUserLearnedDrawer = sp.getBoolean(PREF_USER_LEARNED_DRAWER, false);

        if (savedInstanceState != null) {
            mCurrentSelectedPosition = savedInstanceState.getInt(STATE_SELECTED_POSITION);
            mFromSavedInstanceState = true;
        }

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);//set to treu so there will be a navigation drawer when hamburger clicked

        setupViews();
        setupFbLoginButtton();
        setupDriverandPassengerListView();

        mCallbacks.passViewsToParent(navigation_drawer_fb_profile_pic, navigation_drawer_fb_name, navigation_drawer_email,navigation_drawer_passenger_listview,navigation_drawer_driver_listview);

    }

    void setupViews(){
        navigation_drawer_fb_profile_pic    = (ImageView)getView().findViewById(R.id.navigation_drawer_fb_profile_pic);
        navigation_drawer_fb_name           = (TextView)getView().findViewById(R.id.navigation_drawer_fb_name);
        navigation_drawer_email             = (TextView)getView().findViewById(R.id.navigation_drawer_email);
    }

    void setupFbLoginButtton(){
        callbackManager                     = CallbackManager.Factory.create();
        LoginButton loginButton             = (LoginButton) getView().findViewById(R.id.navigation_drawer_fb_login_btn);
        loginButton.setFragment(this);
    }

    void setupDriverandPassengerListView(){
        navigation_drawer_driver_listview   = (ListView)getView().findViewById(R.id.navgation_drawer_driver_listview);
        navigation_drawer_passenger_listview= (ListView)getView().findViewById(R.id.navgation_drawer_passenger_listview);

        driverAdapter                       = new ArrayAdapter<String>(getContext(),R.layout.custom_drawer_item);
        passengerAdapter                    = new ArrayAdapter<String>(getContext(),R.layout.custom_drawer_item);

        navigation_drawer_driver_listview.setAdapter(driverAdapter);
        navigation_drawer_passenger_listview.setAdapter(passengerAdapter);

        //driverAdapter.add("找乘客");
        //driverAdapter.add("搜索結果");
        //driverAdapter.add("乘客地圖");
        driverAdapter.add("刊登發車");
        driverAdapter.add("管理乘客請求");
        //driverAdapter.add("我的發車");

        passengerAdapter.add("找共乘");
        passengerAdapter.add("搜索結果");
        //passengerAdapter.add("今天出發地圖");
        //passengerAdapter.add("刊登找車");
        passengerAdapter.add("預訂狀況");

        driverAdapter.notifyDataSetChanged();
        passengerAdapter.notifyDataSetChanged();

        navigation_drawer_driver_listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView selectedTextView = (TextView) view;

                mCurrentSelectedPosition = position;
                if (navigation_drawer_driver_listview != null) {
                    navigation_drawer_driver_listview.setItemChecked(position, true);
                }
                if (mDrawerLayout != null) {
                    mDrawerLayout.closeDrawer(mFragmentContainerView);
                }
                if (mCallbacks != null) {
                    mCallbacks.onDriverListViewItemSelected(selectedTextView.getText() + "");
                }
            }
        });

        navigation_drawer_passenger_listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView selectedTextView=(TextView)view;

                mCurrentSelectedPosition = position;
                if (navigation_drawer_passenger_listview != null) {
                    navigation_drawer_passenger_listview.setItemChecked(position, true);
                }
                if (mDrawerLayout != null) {
                    mDrawerLayout.closeDrawer(mFragmentContainerView);
                }
                if (mCallbacks != null) {
                    mCallbacks.onPassengerListViewItemSelected(selectedTextView.getText() + "");
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        drawerLinearLayout = (LinearLayout) inflater.inflate(R.layout.fragment_navigation_drawer, container, false);
        return drawerLinearLayout;

    }



    public boolean isDrawerOpen() {
        return mDrawerLayout != null && mDrawerLayout.isDrawerOpen(mFragmentContainerView);
    }

    /**
     * Users of this fragment must call this method to set up the navigation drawer interactions.
     *
     * @param fragmentId   The android:id of this fragment in its activity's layout.
     * @param drawerLayout The DrawerLayout containing this fragment's UI.
     */
    public void setUp(int fragmentId, DrawerLayout drawerLayout) {
        mFragmentContainerView = getActivity().findViewById(fragmentId);
        mDrawerLayout = drawerLayout;

        //get actionBar, set home enabled, set home as up arrow, replace up arrow to own drawable
        ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.app_logo);
        actionBar.setSubtitle("尋找共乘"+"");

        // ActionBarDrawerToggle ties together the the proper interactions
        // between the navigation drawer and the action bar app icon.
        mDrawerToggle = new ActionBarDrawerToggle(
                getActivity(),                    /* host Activity */
                mDrawerLayout,                    /* DrawerLayout object */
                R.drawable.drawer_shadow,             /* nav drawer image to replace 'Up' caret */
                R.string.navigation_drawer_open,  /* "open drawer" description for accessibility */
                R.string.navigation_drawer_close  /* "close drawer" description for accessibility */
        ) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                if (!isAdded()) { return; }
                getActivity().supportInvalidateOptionsMenu(); // calls onPrepareOptionsMenu()
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);

                if (!isAdded()) { return; }

                if (!mUserLearnedDrawer) {
                    // The user manually opened the drawer; store this flag to prevent auto-showing
                    // the navigation drawer automatically in the future.
                    mUserLearnedDrawer = true;
                    SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getActivity());
                    sp.edit().putBoolean(PREF_USER_LEARNED_DRAWER, true).apply();
                }
                getActivity().supportInvalidateOptionsMenu(); // calls onPrepareOptionsMenu()
            }
        };

        mDrawerToggle.setDrawerIndicatorEnabled(true);

        // If the user hasn't 'learned' about the drawer, open it to introduce them to the drawer,
        // per the navigation drawer design guidelines.
        if (!mUserLearnedDrawer && !mFromSavedInstanceState) {
            mDrawerLayout.openDrawer(mFragmentContainerView);
        }

        // Defer code dependent on restoration of previous instance state.
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mCallbacks = (NavigationDrawerCallbacks) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement NavigationDrawerCallbacks.");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(STATE_SELECTED_POSITION, mCurrentSelectedPosition);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Forward the new configuration the drawer toggle component.
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // If the drawer is open, show the global app actions in the action bar. See also
        // showGlobalContextActionBar, which controls the top-left area of the action bar.
        if (mDrawerLayout != null && isDrawerOpen()) {
            inflater.inflate(R.menu.global, menu);
            showGlobalContextActionBar();
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    //the settings-btns at top right
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        if (item.getItemId() == R.id.action_example) {
            Toast.makeText(getActivity(), "Example action.", Toast.LENGTH_SHORT).show();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showGlobalContextActionBar() {
        ActionBar actionBar = getActionBar();
        actionBar.setDisplayShowTitleEnabled(true);
    }

    private ActionBar getActionBar() {
        return ((AppCompatActivity) getActivity()).getSupportActionBar();
    }

    public interface NavigationDrawerCallbacks {
        //pass the selected option in string to parent activity to make fragment change
        void onDriverListViewItemSelected(String itemName);
        void onPassengerListViewItemSelected(String itemName);

        //pass the imageview and two textview to parent
        //so that when profile tracker detected any login/out it can makes changes to pic and text
        void passViewsToParent(ImageView profilePic,TextView name,TextView email,ListView navigation_drawer_passenger_listview,ListView navigation_drawer_driver_listview);
    }
}
