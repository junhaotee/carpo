package com.example.junhao.carpo;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;


public class CancelDriverApprovedRequestTab extends Fragment {


    boolean                     isLoading           = false;
    boolean                     isItemMax           = false;
    boolean                     firstClicked        = false;
    int                         currentItemOffset   = 0;
    static JSONObject           itemRef;

    ImageView                   driver_profile_dialog_profile_pic;
    ListView                    driver_profile_dialog_rating_listview;
    UserRatingItemAdapter       ratingLvAdapter;
    ArrayList<UserRatingItem>   ratingCommentList;
    View                        footerView;
    Button                      cancel_approved_reservation_btn_cancel_request;

    TextView                    driver_profile_dialog_profile_verified,
                                driver_profile_dialog_profile_driver_rate,
                                driver_profile_dialog_name,
                                driver_profile_dialog_driver_hometown,
                                driver_profile_dialog_profile_contact,
                                driver_profile_dialog_reply_rate,
                                driver_profile_dialog_reply_within_hour,
                                driver_profile_dialog_rating_count,
                                driver_profile_dialog_bio,
                                listview_footer_tv_stat;

    public CancelDriverApprovedRequestTab() { }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_cancel_approved_request_tab, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
        setupViews();
        setupViewsData();
    }

    void setupViews(){

        driver_profile_dialog_profile_pic           = (ImageView)getView().findViewById(R.id.driver_profile_dialog_profile_pic);
        driver_profile_dialog_profile_verified      = (TextView)getView().findViewById(R.id.driver_profile_dialog_profile_verified);
        driver_profile_dialog_profile_driver_rate   = (TextView)getView().findViewById(R.id.driver_profile_dialog_profile_driver_rate);
        driver_profile_dialog_name                  = (TextView)getView().findViewById(R.id.driver_profile_dialog_name);
        driver_profile_dialog_driver_hometown       = (TextView)getView().findViewById(R.id.driver_profile_dialog_driver_hometown);
        driver_profile_dialog_profile_contact       = (TextView)getView().findViewById(R.id.driver_profile_dialog_profile_contact);
        driver_profile_dialog_reply_rate            = (TextView)getView().findViewById(R.id.driver_profile_dialog_reply_rate);
        driver_profile_dialog_reply_within_hour     = (TextView)getView().findViewById(R.id.driver_profile_dialog_reply_within_hour);
        driver_profile_dialog_rating_count          = (TextView)getView().findViewById(R.id.driver_profile_dialog_rating_count);
        driver_profile_dialog_bio                   = (TextView)getView().findViewById(R.id.driver_profile_dialog_bio);
        driver_profile_dialog_rating_listview       = (ListView)getView().findViewById(R.id.driver_profile_dialog_rating_listview);
        cancel_approved_reservation_btn_cancel_request=(Button)getView().findViewById(R.id.cancel_approved_reservation_btn_cancel_request);
        footerView                                  = ((LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.listview_footer, null, false);
        listview_footer_tv_stat                     = (TextView)footerView.findViewById(R.id.listview_footer_tv_stat);

    }

    void setupViewsData(){

        try {
            new GetProfilePic().execute(itemRef.getString("owner"), driver_profile_dialog_profile_pic);

            int rating_count            = Integer.parseInt(itemRef.getString("d_rating_good_count"))+Integer.parseInt(itemRef.getString("d_rating_average_count"))+Integer.parseInt(itemRef.getString("d_rating_bad_count"));
            String desc_rating_count    = (rating_count==0)?"車主剛加入共乘":rating_count+"個來自乘客的評價";
            String name                 = itemRef.getString("fName")+itemRef.getString("lName");
            String showLine             = (Integer.parseInt(itemRef.getString("showLine"))==1)?"賴:"+itemRef.getString("line"):"";
            String verified             = (Integer.parseInt(itemRef.getString("verified"))==1)?"✓ 手機認證":"手機未認證";
            String bio                  = (itemRef.getString("bio").length()!=0)?itemRef.getString("bio"):"(車主太酷了，沒填自我簡介)";
            String driver_hometown      = itemRef.getString("hometown_name");

            driver_profile_dialog_bio.setText(bio);
            driver_profile_dialog_name.setText(name);
            driver_profile_dialog_profile_contact.setText(showLine);
            driver_profile_dialog_profile_verified.setText(verified);
            driver_profile_dialog_driver_hometown.setText(driver_hometown);
            driver_profile_dialog_rating_count.setText(desc_rating_count);

            if((Integer.parseInt(itemRef.getString("verified"))!=1))
                driver_profile_dialog_profile_verified.setTextColor(ContextCompat.getColor(getContext(), R.color.grey));

           /*
                TODO:need to develope a scheme for driver rating
                    driver_profile_dialog_profile_driver_rate
                    driver_profile_dialog_reply_rate
                    driver_profile_dialog_reply_within_hour
            */

            //if rating comment is zero hide and section
            if(rating_count==0){

                LinearLayout driver_profile_dialog_rating_listview_section=(LinearLayout)getView().findViewById(R.id.driver_profile_dialog_rating_listview_section);
                driver_profile_dialog_rating_listview_section.setVisibility(View.INVISIBLE);

            }else{//if there were any comments on the driver, set it

                ratingCommentList                       =new ArrayList();
                ratingLvAdapter                         =new UserRatingItemAdapter(getContext(),ratingCommentList);
                driver_profile_dialog_rating_listview.setAdapter(ratingLvAdapter);
                driver_profile_dialog_rating_listview.setOnScrollListener(ratingItemLvListener);
                driver_profile_dialog_rating_listview.addFooterView(footerView);

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        cancel_approved_reservation_btn_cancel_request.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(firstClicked==false){
                    cancel_approved_reservation_btn_cancel_request.setText("再次確定");
                    firstClicked=true;
                    return;
                }
                CancelDriverApprovedRequestDialog.currentDialogFragment.getTargetFragment().onActivityResult(CancelDriverApprovedRequestDialog.currentDialogFragment.getTargetRequestCode(), CancelDriverApprovedRequestDialog.currentDialogFragment.getTargetRequestCode(), null);
                CancelDriverApprovedRequestDialog.currentDialogFragment.getDialog().dismiss();
            }
        });
    }

    AbsListView.OnScrollListener ratingItemLvListener=new AbsListView.OnScrollListener() {

        @Override
        public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            int lastInScreen = firstVisibleItem + visibleItemCount;
            if ((lastInScreen == totalItemCount && !isLoading &&!isItemMax)) {
                isLoading = true;
                try {
                    new GetRatingResults().execute(itemRef.getString("owner"),currentItemOffset,"d");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onScrollStateChanged(AbsListView absListView, int i) { }
    };

    public void setupDateAndTime(String datetimeFormat,TextView dom,TextView month,TextView hour,TextView minute,TextView ampm){

        //string processing to extract ymd hms
        String[] dateTime=datetimeFormat.split(" ");
        String[] ymd=dateTime[0].split("-");
        String[] hms=dateTime[1].split(":");

        //find day of week
        // Note that Month value is 0-based. e.g., 0 for January.
        Calendar calendar = new GregorianCalendar(Integer.parseInt(ymd[0]),Integer.parseInt(ymd[1])-1,Integer.parseInt(ymd[2]));
        int day = calendar.get(Calendar.DAY_OF_WEEK);

        String dow="";
        switch (day) {
            case Calendar.MONDAY:{dow="一";break;}
            case Calendar.TUESDAY:{dow="二";break;}
            case Calendar.WEDNESDAY:{dow="三";break;}
            case Calendar.THURSDAY:{dow="四";break;}
            case Calendar.FRIDAY:{dow="五";break;}
            case Calendar.SATURDAY:{dow="六";break;}
            case Calendar.SUNDAY:{dow="日";break;}
        }

        //removing 0 prefix for month,day,hour,minute
        if(Integer.parseInt(ymd[1].charAt(0)+"")==0)
            ymd[1]=ymd[1].charAt(1)+"";

        if(Integer.parseInt(ymd[2].charAt(0)+"")==0)
            ymd[2]=ymd[2].charAt(1)+"";

        if(Integer.parseInt(hms[0].charAt(0) + "")==0)
            hms[0]=hms[0].charAt(1)+"";

        //detect AM or PM
        String AMPM=(Integer.parseInt(hms[0])>=12)?"PM":"AM";

        //make hour 13 to 1
        if(Integer.parseInt(hms[0])>=13){
            hms[0]=" "+(Integer.parseInt(hms[0])-12)+"";
        }

        //set formatted date output to ui
        dom.setText("("+dow+") "+ymd[2]);
        month.setText(ymd[1]);
        hour.setText(hms[0]);
        minute.setText(hms[1]);
        ampm.setText(AMPM);

    }

    class GetProfilePic extends AsyncTask<Object,Void,Bitmap> {

        ImageView fb_profile_pic;
        String fbId;

        @Override
        protected Bitmap doInBackground(Object... params) {
            Bitmap profilePic=null;
            fb_profile_pic=(ImageView)params[1];
            fbId=(String)params[0];

            try {

                URL profileURL=new URL("https://graph.facebook.com/"+fbId+"/picture?type=square");
                Log.d("pp", "https://graph.facebook.com/" + fbId + "/picture?type=square");
                URLConnection urlConn=profileURL.openConnection();
                urlConn.setReadTimeout(1500);
                urlConn.setConnectTimeout(1500);
                profilePic= BitmapFactory.decodeStream((InputStream) urlConn.getContent());

            }
            catch (MalformedURLException e) { e.printStackTrace(); }
            catch (IOException e) { e.printStackTrace(); }

            return profilePic;
        }

        @Override
        protected void onPostExecute(Bitmap profilePic){
            if(profilePic==null) return;

            fb_profile_pic.setImageBitmap(profilePic);

        }
    }

    class GetRatingResults extends AsyncTask<Object,Void,Integer> {

        String resultItemList="";

        @Override
        protected Integer doInBackground(Object... params) {

            try {
                String  fk_user_fb_rated    = params[0]+"";
                String  role                = params[2]+"";
                int     currentItemOffset   = (int)params[1];

                String queryString      = "http://140.116.83.83/api/get_user_rating.php?fk_user_fb_rated="+fk_user_fb_rated+"&currentItemOffset="+currentItemOffset+"&role="+role;
                Log.d("queryString",queryString);
                URL url                 = new URL(queryString);
                URLConnection urlConn   = url.openConnection();

                urlConn.setReadTimeout(1000);
                urlConn.setConnectTimeout(1000);

                resultItemList=convertStreamToString(urlConn.getInputStream());

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                resultItemList=""; //if server connect/read timeout, return empty list
                e.printStackTrace();
            }
            return null;
        }

        //onPostExecute is UI thread, updat the views here
        @Override
        protected void onPostExecute(Integer result) {
            try{
                //if empty string returned from server(when server gone wrong internally)
                if(resultItemList.length()==0){
                    driver_profile_dialog_rating_listview.setVisibility(View.GONE);
                    isItemMax=true;
                    return;
                }

                //parsed server response into json
                JSONArray userRatingArr=new JSONArray(resultItemList);

                //if no result is found
                if(userRatingArr.length()==0){
                    listview_footer_tv_stat.setText("");
                    isItemMax=true;
                }

                //if there were results, populates found result into arraylist
                for(int k=0;k<userRatingArr.length();k++){
                    JSONObject ratingObj = new JSONObject(userRatingArr.get(k).toString());
                    ratingCommentList.add(new UserRatingItem(ratingObj));
                }

                //notify listview data changed, and unblock dialog
                ratingLvAdapter.notifyDataSetChanged();
                currentItemOffset  += userRatingArr.length();
                isLoading           = false;

            }catch(JSONException jsone){ jsone.printStackTrace(); }
        }
        //method to convert input stream to string, used after reading input stream
        String convertStreamToString(java.io.InputStream is) {
            java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
            return s.hasNext() ? s.next() : "";
        }
    }
}
