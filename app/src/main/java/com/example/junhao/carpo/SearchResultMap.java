package com.example.junhao.carpo;

import android.app.Activity;
import android.content.Context;

import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import javax.xml.transform.Result;


public class SearchResultMap extends Fragment {

    int                     lastPos=-1;
    boolean                 loading=false;
    boolean                 itemMax=false;
    int                     currentItemOffset =0;
    static ResultListItem   chosenItem;

    GoogleMap               mGoogleMapSrc;
    GoogleMap               mGoogleMapDst;
    ListView                searchResultListView;
    ArrayAdapter            d;
    ArrayList               searchResultList;
    String                  locationResults;
    Button                  search_result_map_research_btn;
    TextView                listview_footer_tv_stat;


    TextView                search_result_map_src_city_town,
                            search_result_map_dst_city_town,
                            search_result_map_result_size,
                            src_address_detail,dst_address_detail;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        return (ViewGroup) inflater.inflate(R.layout.fragment_search_result_map, null);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);

        /* the order methodcall is important here. */

        //blocking screen
        MainActivity.blockScreen("尋找相關共乘中...",getContext());

        //banner text etc..
        setupTextViews();

        //setupButtons
        setupButtons();

        //setup map view
        setupGoogleMap();

        //setup views and attach listeners
        setupResultListview();

    }

    void setupButtons(){
        search_result_map_research_btn=(Button)getView().findViewById(R.id.search_result_map_research_btn);
        search_result_map_research_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().beginTransaction().replace(R.id.container, new SearchInterface()).commit();
            }
        });
    }

    void setupTextViews(){
        search_result_map_src_city_town=(TextView)getView().findViewById(R.id.search_result_map_src_city_town);
        search_result_map_dst_city_town=(TextView)getView().findViewById(R.id.search_result_map_dst_city_town);
        search_result_map_result_size=(TextView)getView().findViewById(R.id.search_result_map_result_size);

        src_address_detail=(TextView)getView().findViewById(R.id.src_address_detail);
        dst_address_detail=(TextView)getView().findViewById(R.id.dst_address_detail);

        search_result_map_src_city_town.setText(SearchInterface.searchData.getStringExtra("src_city_town"));
        search_result_map_dst_city_town.setText(SearchInterface.searchData.getStringExtra("dst_city_town"));
    }

    public void setupGoogleMap(){
        //pass supportmapfragment's google map to mGoogleMap variable
        if(mGoogleMapSrc!=null)return;
        SupportMapFragment mapFrag1 = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.search_result_map_googlemap_src);
        mapFrag1.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                mGoogleMapSrc = googleMap;
                mGoogleMapSrc.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                    @Override
                    public void onMapLoaded() {

                    }
                });
            }
        });

        if(mGoogleMapDst!=null)return;
        SupportMapFragment mapFrag2 = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.search_result_map_googlemap_dst);
        mapFrag2.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                mGoogleMapDst = googleMap;
                mGoogleMapDst.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                    @Override
                    public void onMapLoaded() {

                    }
                });
            }
        });
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    void setupResultListview(){
        searchResultList                            = new ArrayList<ResultListItem>();
        searchResultListView                        = (ListView) getView().findViewById(R.id.search_result_map_result_listview);
        View footerView                             = ((LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.listview_footer, null, false);
        listview_footer_tv_stat                     = (TextView)footerView.findViewById(R.id.listview_footer_tv_stat);

        searchResultListView.addFooterView(footerView);
        d = new ResultListAdapter(getContext(),searchResultList);
        searchResultListView.setAdapter(d);
        d.notifyDataSetChanged();

        //set onclick action on items of listview
        searchResultListView.setOnItemClickListener(adapterItemOnClick);
        searchResultListView.setOnScrollListener(ratingItemLvListener);

        loading = true;
        new GetRelatedResults().execute(currentItemOffset);
    }

    AbsListView.OnScrollListener ratingItemLvListener=new AbsListView.OnScrollListener() {

        @Override
        public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            int lastInScreen = firstVisibleItem + visibleItemCount;
            if ((lastInScreen == totalItemCount && !loading &&!itemMax)) {
                loading = true;
                new GetRelatedResults().execute(currentItemOffset);
            }
        }

        @Override
        public void onScrollStateChanged(AbsListView absListView, int i) { }
    };


    private AdapterView.OnItemClickListener adapterItemOnClick=new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            if(position>=searchResultList.size())return;

            if(lastPos==position){
                getFragmentManager().beginTransaction().replace(R.id.container, new JoinCarpool()).commit();
                return;
            }
            lastPos=position;

            //get result item from custom array list by listview index
            //need to get carpool id of database that stored in locationJSONObject of ResultListItem

            chosenItem=(ResultListItem)(searchResultList.get(position));

            //when is 0 results(isItemNull return true), don't response to click, return immediately
            if(chosenItem.isItemNull())return;



            //set selected src/dst name to banners:with detail address
            //String srcDetail="["+chosenItem.getItemValue(getResources().getString(R.string.db_src_city_name))+" "+chosenItem.getItemValue(getResources().getString(R.string.db_src_town_name))+"]"+chosenItem.getItemValue(getResources().getString(R.string.db_carpool_src_name)) +"\n"+ chosenItem.getItemValue(getResources().getString(R.string.db_carpool_src_address));
            //String dstDetail="["+chosenItem.getItemValue(getResources().getString(R.string.db_dst_city_name))+" "+chosenItem.getItemValue(getResources().getString(R.string.db_dst_town_name))+"]"+chosenItem.getItemValue(getResources().getString(R.string.db_carpool_dst_name)) +"\n"+ chosenItem.getItemValue(getResources().getString(R.string.db_carpool_dst_address));

            //set selected src/dst name to banners:without detail address
            String srcDetail="["+chosenItem.getItemValue(getResources().getString(R.string.db_src_city_name))+" "+chosenItem.getItemValue(getResources().getString(R.string.db_src_town_name))+"]"+chosenItem.getItemValue(getResources().getString(R.string.db_carpool_src_name));
            String dstDetail="["+chosenItem.getItemValue(getResources().getString(R.string.db_dst_city_name))+" "+chosenItem.getItemValue(getResources().getString(R.string.db_dst_town_name))+"]"+chosenItem.getItemValue(getResources().getString(R.string.db_carpool_dst_name));

            src_address_detail.setText(srcDetail);
            dst_address_detail.setText(dstDetail);

            /*setup map */
            mGoogleMapSrc.clear();
            mGoogleMapDst.clear();

            setMapCoordinate(mGoogleMapSrc, Double.parseDouble(chosenItem.getItemValue(getString(R.string.db_carpool_src_lat))), Double.parseDouble(chosenItem.getItemValue(getString(R.string.db_carpool_src_lng))));
            setMapCoordinate(mGoogleMapDst,Double.parseDouble(chosenItem.getItemValue(getString(R.string.db_carpool_dst_lat))),Double.parseDouble(chosenItem.getItemValue(getString(R.string.db_carpool_dst_lng))));

            /* end of setup map*/

            //replace container with joincarpool fragment, display detail carpool information
            //

            return;
        }
    };

    void setMapCoordinate(GoogleMap mGoogleMap,Double lat,Double lng){
        LatLng defaultLocation = new LatLng(lat,lng);
        mGoogleMap.addMarker(new MarkerOptions().position(defaultLocation).draggable(true));
        CameraUpdate locationCamera = CameraUpdateFactory.newLatLngZoom(defaultLocation, 17.0f);
        mGoogleMap.animateCamera(locationCamera);
    }

    //this asynctask search for carpool results
    class GetRelatedResults extends AsyncTask<Integer,Void,Integer> {
        @Override
        protected Integer doInBackground(Integer... params) {

            try {
                //the url to query, returns json results
                Double src_lat=SearchInterface.searchData.getExtras().getDouble("src_lat");
                Double src_lng=SearchInterface.searchData.getExtras().getDouble("src_lng");
                Double dst_lat=SearchInterface.searchData.getExtras().getDouble("dst_lat");
                Double dst_lng=SearchInterface.searchData.getExtras().getDouble("dst_lng");

                String queryString="http://140.116.83.83/api/get_carpool_result.php?startLat="+src_lat+"&startLng="+src_lng+"&dstLat="+dst_lat+"&dstLng="+dst_lng+"&currentItemOffset="+currentItemOffset;
                URL url = new URL(queryString);
                URLConnection urlConn=url.openConnection();

                urlConn.setReadTimeout(1000);
                urlConn.setConnectTimeout(1000);

                locationResults=convertStreamToString(urlConn.getInputStream());

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                locationResults="";//if server connect/read timeout, return empty list
                e.printStackTrace();
            }
            return null;
        }

        //onPostExecute is UI thread, updat the views here
        @Override
        protected void onPostExecute(Integer result) {
            try{

                //if internal server exception, return empty string
                if(locationResults.length()==0){
                    /*
                    searchResultList.add(new ResultListItem());
                    d.notifyDataSetChanged();
                    */
                    searchResultListView.setVisibility(View.GONE);
                    search_result_map_result_size.setText(0 + "");
                    MainActivity.dialog.hide();
                    itemMax=true;
                    listview_footer_tv_stat.setText("");
                    return;
                }

                //parsed server response into json
                JSONArray locationArr=new JSONArray(locationResults);

                //if no result is found, add an empty item and quit
                if(locationArr.length()==0){
                    /*
                    searchResultList.add(new ResultListItem());
                    d.notifyDataSetChanged();
                    */
                    //searchResultListView.setVisibility(View.GONE);
                    //search_result_map_result_size.setText(0+"");
                    MainActivity.dialog.hide();
                    itemMax=true;
                    listview_footer_tv_stat.setText("");
                }

                //if there were results, populates found result into arraylist
                for(int k=0;k<locationArr.length();k++){
                    JSONObject locationObj = new JSONObject(locationArr.get(k).toString());
                    searchResultList.add(new ResultListItem(locationObj));
                }

                //notify listview data changed, and unblock dialog
                //search_result_map_result_size.setText(locationArr.length()+"");
                d.notifyDataSetChanged();
                MainActivity.dialog.hide();


                //click on first item after data is loaded into listview
                /*
                searchResultListView.performItemClick(searchResultListView.getAdapter().getView(0, null, null),
                        0,
                        searchResultListView.getAdapter().getItemId(0));
                */
                currentItemOffset +=locationArr.length();
                search_result_map_result_size.setText(currentItemOffset+"");
                loading=false;

            }catch(JSONException jsone){ jsone.printStackTrace(); }
        }

        //method to convert input stream to string, used after reading input stream
        String convertStreamToString(java.io.InputStream is) {
            java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
            return s.hasNext() ? s.next() : "";
        }
    }
}
