package com.example.junhao.carpo;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class CancelDriverApprovedRequestDialog extends DialogFragment {

    TabLayout               cancel_driver_approved_reservation_tabs;
    static DialogFragment   currentDialogFragment;

    public CancelDriverApprovedRequestDialog() { }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        getDialog().setTitle("聯絡車主");
        return inflater.inflate(R.layout.fragment_cancel_driver_approved_reservation_dialog, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);

        currentDialogFragment=this;
        cancel_driver_approved_reservation_tabs=(TabLayout)getView().findViewById(R.id.cancel_driver_approved_reservation_tabs);
        cancel_driver_approved_reservation_tabs.addTab(cancel_driver_approved_reservation_tabs.newTab().setText("車主"));
        cancel_driver_approved_reservation_tabs.addTab(cancel_driver_approved_reservation_tabs.newTab().setText("共乘"));
        cancel_driver_approved_reservation_tabs.setOnTabSelectedListener(tabListener);

        CancelDriverApprovedRequestTab.itemRef= CheckUpcomingCarpoolTab.chosenUpcomingCarpoolItem.getJSONObject();
        getChildFragmentManager().beginTransaction().replace(R.id.tab_frame_layout, new CancelDriverApprovedRequestTab()).commit();

    }

    private TabLayout.OnTabSelectedListener tabListener=new TabLayout.OnTabSelectedListener() {

        @Override
        public void onTabSelected(TabLayout.Tab tab) {

            Fragment userChoseFragment=null;

            switch(tab.getText().toString()){
                case "車主":{
                    CancelDriverApprovedRequestTab.itemRef= CheckUpcomingCarpoolTab.chosenUpcomingCarpoolItem.getJSONObject();
                    userChoseFragment=PassengerTabFactory.cancelApprovedRequest();

                };break;
                case "共乘":{
                    CarpoolDetailTab.carpoolItemRef= CheckUpcomingCarpoolTab.chosenUpcomingCarpoolItem.getJSONObject();
                    userChoseFragment=PassengerTabFactory.getCarpoolDetail();
                };break;
            }

            //return null if invalid fragment
            if(userChoseFragment==null)
                return;
            else{
                //clear all childviews in frame layout
                FrameLayout tabFrame=(FrameLayout)getView().findViewById(R.id.tab_frame_layout);
                tabFrame.removeAllViews();

                //add user selection framgent into framelayout
                getChildFragmentManager().beginTransaction().replace(R.id.tab_frame_layout,userChoseFragment).commit();
            }
        }

        //implement action if you wish
        @Override
        public void onTabUnselected(TabLayout.Tab tab) {}
        @Override
        public void onTabReselected(TabLayout.Tab tab) {}
    };

    @Override
    public void onPause(){
        super.onPause();
        dismiss();
    }


    class GetProfilePic extends AsyncTask<Object,Void,Bitmap> {

        ImageView fb_profile_pic;
        String fbId;

        @Override
        protected Bitmap doInBackground(Object... params) {
            Bitmap profilePic=null;
            fb_profile_pic=(ImageView)params[1];
            fbId=(String)params[0];

            try {

                URL profileURL=new URL("https://graph.facebook.com/"+fbId+"/picture?type=square");
                Log.d("pp", "https://graph.facebook.com/" + fbId + "/picture?type=square");
                URLConnection urlConn=profileURL.openConnection();
                urlConn.setReadTimeout(1500);
                urlConn.setConnectTimeout(1500);
                profilePic= BitmapFactory.decodeStream((InputStream) urlConn.getContent());

            }
            catch (MalformedURLException e) { e.printStackTrace(); }
            catch (IOException e) { e.printStackTrace(); }

            return profilePic;
        }

        @Override
        protected void onPostExecute(Bitmap profilePic){
            if(profilePic==null) return;

            fb_profile_pic.setImageBitmap(profilePic);

        }
    }



}
