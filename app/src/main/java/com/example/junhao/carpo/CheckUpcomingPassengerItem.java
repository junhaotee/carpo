
package com.example.junhao.carpo;

import org.json.JSONException;
import org.json.JSONObject;

public class CheckUpcomingPassengerItem {

    private JSONObject passengerJSONObject;

    //stores location result of one item
    public CheckUpcomingPassengerItem(JSONObject passengerResult){
        passengerJSONObject =passengerResult;
    }

    //empty constructor is called when no results from server
    public CheckUpcomingPassengerItem(){
        passengerJSONObject =null;
    }

    //a convenient method for ResultListAdapter to retrieve json values
    public String getItemValue(String JSONKey){
        try {
            return passengerJSONObject.getString(JSONKey).toString();
        }
        catch (JSONException e) { e.printStackTrace();}

        return null;
    }

    public boolean isItemNull(){
        return (passengerJSONObject==null)?true:false;
    }

    public JSONObject getJSONObject(){
        return passengerJSONObject;
    }

}
