package com.example.junhao.carpo;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by junhao on 2015/12/7.
 */
public class ResultListItem {

    private JSONObject locationJSONObject;

    //stores location result of one item
    public ResultListItem(JSONObject locationResult){
        locationJSONObject=locationResult;
    }

    //empty constructor is called when no results from server
    public ResultListItem(){
        locationJSONObject=null;
    }

    //a convenient method for ResultListAdapter to retrieve json values
    public String getItemValue(String JSONKey){

        try {
            return locationJSONObject.getString(JSONKey).toString();
        } catch (JSONException e) { e.printStackTrace();}
        return null;
    }

    public boolean isItemNull(){return (
        locationJSONObject==null)?true:false;
    }

    public JSONObject getJSONObject(){
        return locationJSONObject;
    }

}
