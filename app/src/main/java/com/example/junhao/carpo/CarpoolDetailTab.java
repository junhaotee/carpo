package com.example.junhao.carpo;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.GregorianCalendar;


public class CarpoolDetailTab extends Fragment {

    static JSONObject carpoolItemRef;

    TextView        carpool_detail_src,
                    carpool_detail_dst,
                    carpool_detail_datetime,
                    carpool_detail_seat_left,
                    carpool_detail_vehicle_name,
                    carpool_detail_costpp,
                    carpool_detail_comment,
                    carpool_detail_preferences;

    public CarpoolDetailTab() { }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_carpool_detail_tab, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
        setupViews();
        setupViewsData();
    }

    void setupViews(){
        carpool_detail_src          = (TextView)getView().findViewById(R.id.carpool_detail_src);
        carpool_detail_dst          = (TextView)getView().findViewById(R.id.carpool_detail_dst);
        carpool_detail_datetime     = (TextView)getView().findViewById(R.id.carpool_detail_datetime);
        carpool_detail_seat_left    = (TextView)getView().findViewById(R.id.carpool_detail_seat_left);
        carpool_detail_vehicle_name = (TextView)getView().findViewById(R.id.carpool_detail_vehicle_name);
        carpool_detail_costpp       = (TextView)getView().findViewById(R.id.carpool_detail_costpp);
        carpool_detail_comment      = (TextView)getView().findViewById(R.id.carpool_detail_comment);
        carpool_detail_preferences  = (TextView)getView().findViewById(R.id.carpool_detail_preferences);
    }

    void setupViewsData(){

        try {
            String srcName              = carpoolItemRef.getString("start").toString();
            String dstName              = carpoolItemRef.getString("destination").toString();
            String isTimeNegoable       = (Integer.parseInt(carpoolItemRef.getString("isAnytime"))==1)?"(任意時間)":(Integer.parseInt(carpoolItemRef.getString("timeNego"))==1)?"(可議)":"";
            String vehicle_name         = carpoolItemRef.getString("vehicle_name");
            String costPp               = carpoolItemRef.getString("costpp");
            String carpoolComment       = carpoolItemRef.getString("comment");
            String carpoolPref          = setupPreferences();
            int seatLeft                = Integer.parseInt(carpoolItemRef.getString("seatGiven"))-Integer.parseInt(carpoolItemRef.getString("seatTaken"));


            carpool_detail_src.setText(srcName);
            carpool_detail_dst.setText(dstName);
            carpool_detail_datetime.setText(formattedDatetime(carpoolItemRef.getString("timeDepart"))+isTimeNegoable);
            carpool_detail_seat_left.setText(seatLeft+"");
            carpool_detail_vehicle_name.setText(vehicle_name);
            carpool_detail_costpp.setText(costPp);
            carpool_detail_comment.setText(carpoolComment);
            carpool_detail_preferences.setText(carpoolPref);

            carpoolItemRef=null;

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public String formattedDatetime(String timeDepart){

        String formattedDatetime="";

        //string processing to extract ymd hms
        String[] dateTime=timeDepart.split(" ");
        String[] ymd=dateTime[0].split("-");
        String[] hms=dateTime[1].split(":");

        //find day of week
        // Note that Month value is 0-based. e.g., 0 for January.
        Calendar calendar = new GregorianCalendar(Integer.parseInt(ymd[0]),Integer.parseInt(ymd[1])-1,Integer.parseInt(ymd[2]));
        int day = calendar.get(Calendar.DAY_OF_WEEK);

        String dow="";
        switch (day) {
            case Calendar.MONDAY:{dow="一";break;}
            case Calendar.TUESDAY:{dow="二";break;}
            case Calendar.WEDNESDAY:{dow="三";break;}
            case Calendar.THURSDAY:{dow="四";break;}
            case Calendar.FRIDAY:{dow="五";break;}
            case Calendar.SATURDAY:{dow="六";break;}
            case Calendar.SUNDAY:{dow="日";break;}
        }

        //removing 0 prefix for month,day,hour,minute
        if(Integer.parseInt(ymd[1].charAt(0)+"")==0)
            ymd[1]=ymd[1].charAt(1)+"";

        if(Integer.parseInt(ymd[2].charAt(0)+"")==0)
            ymd[2]=ymd[2].charAt(1)+"";

        if(Integer.parseInt(hms[0].charAt(0) + "")==0)
            hms[0]=hms[0].charAt(1)+"";

        //detect AM or PM
        String AMPM=(Integer.parseInt(hms[0])>=12)?"PM":"AM";

        //make hour 13 to 1
        if(Integer.parseInt(hms[0])>=13){
            hms[0]=" "+(Integer.parseInt(hms[0])-12)+"";
        }

        //TODO:day difference with the carpool date

        String dateInterval2="";
        try {
            if(Integer.parseInt(carpoolItemRef.getString("hasInterval"))==1){

                String[] dateTime2=carpoolItemRef.getString("timeDepart2").split(" ");
                String[] ymd2=dateTime2[0].split("-");
                String[] hms2=dateTime2[1].split(":");

                if(Integer.parseInt(hms2[0].charAt(0) + "")==0)
                    hms2[0]=hms2[0].charAt(1)+"";

                //detect AM or PM
                String AMPM2=(Integer.parseInt(hms2[0])>=12)?"PM":"AM";

                //make hour 13 to 1
                if(Integer.parseInt(hms2[0])>=13){
                    hms2[0]=" "+(Integer.parseInt(hms2[0])-12)+"";
                }

                dateInterval2=" ~ "+hms2[0]+":"+hms2[1]+" "+AMPM2;

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        formattedDatetime=" "+ymd[1]+" 月 "+ymd[2]+" 日 ("+dow+") \n"+" "+hms[0]+":"+hms[1]+" "+AMPM+dateInterval2;

        //2月18日(六) 9:30 AM
        return formattedDatetime;
    }

    String setupPreferences(){

        /*
            isFoodAllowed         "輕飲食許可":"不建議車上飲食"
            isBigLuggageAllowed   "有大件行李先詢問":"空間有限無法載送大件行李"
            isStopByAllowed       "需要時可短時間停靠":"沿途停靠先詢問車主"
            isPaymentAfterBefore  "下車後付款":"上車前付款"
            isIdentityCheckNeeded "上車前需要身份認證":""
        */

        try {
            String isFoodAllowed        = (Integer.parseInt(carpoolItemRef.getString("isFoodAllowed"))==1)?"✓ 輕飲食許可":"✓ 不建議車上飲食";
            String isBigLuggageAllowed  = (Integer.parseInt(carpoolItemRef.getString("isBigLuggageAllowed"))==1)?"✓ 有大件行李先詢問":"✓ 空間有限無法載送大件行李";
            String isStopByAllowed      = (Integer.parseInt(carpoolItemRef.getString("isStopByAllowed"))==1)?"✓ 需要時可短時間停靠":"✓ 沿途停靠先詢問車主";
            String isPaymentAfterBefore = (Integer.parseInt(carpoolItemRef.getString("isPaymentAfterBefore"))==1)?"✓ 下車後付款":"✓ 上車前付款";
            String isIdentityCheckNeeded= (Integer.parseInt(carpoolItemRef.getString("isIdentityCheckNeeded"))==1)?"✓ 上車前需要身份認證":"";

            String driverPreferences    = isFoodAllowed+"\n"+isBigLuggageAllowed+"\n"+isStopByAllowed+"\n"+isPaymentAfterBefore+"\n"+isIdentityCheckNeeded;
            return driverPreferences;

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return "";
    }
}
