package com.example.junhao.carpo;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.text.format.Time;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.DatePicker;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;


public class DatePickerDialog extends DialogFragment {

    Button      datepicker_dialog_savedate_btn;
    DatePicker  datepicker_dialog_datepicker;

    public DatePickerDialog(){};

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_date_picker_dialog, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);

        getDialog().setTitle("選擇出發日期");

        datepicker_dialog_savedate_btn=(Button)getView().findViewById(R.id.datepicker_dialog_savedate_btn);
        datepicker_dialog_savedate_btn.setOnClickListener(saveDateBtnListener);

        datepicker_dialog_datepicker=(DatePicker)getView().findViewById(R.id.datepicker_dialog_datepicker);
                getDialog().setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                getTargetFragment().onActivityResult(-1, -1, null);
            }
        });
    }

    View.OnClickListener saveDateBtnListener=new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            Calendar userPickedDate = Calendar.getInstance();
            Calendar now            = Calendar.getInstance();

            int d,m,y;
            d=datepicker_dialog_datepicker.getDayOfMonth();
            m=datepicker_dialog_datepicker.getMonth();
            y=datepicker_dialog_datepicker.getYear();

            userPickedDate.set(y,m,d);
            if(userPickedDate.before(now)){
                Toast.makeText(getContext(),"對不起，APP無法回到過去",Toast.LENGTH_SHORT).show();
                return;
            }

            Intent dmyData=new Intent();
            dmyData.putExtra("d",d);
            dmyData.putExtra("m",m);
            dmyData.putExtra("y",y);

            getTargetFragment().onActivityResult(getTargetRequestCode(),getTargetRequestCode(),dmyData);
            getDialog().dismiss();
        }
    };


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
    }

    @Override
    public void onPause(){
        super.onPause();
        dismiss();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
