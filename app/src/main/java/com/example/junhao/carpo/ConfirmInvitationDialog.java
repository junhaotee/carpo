package com.example.junhao.carpo;



import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

public class ConfirmInvitationDialog extends DialogFragment {

    boolean     firstClicked=false;

    Bundle      previewData;
    Button      confirm_invitation_dialog_savebtn;
    ProgressDialog dialog;

    TextView    confirm_invitation_dialog_src,
                confirm_invitation_dialog_dst,
                confirm_invitation_dialog_date,
                confirm_invitation_dialog_time,
                confirm_invitation_dialog_vehicle_type,
                confirm_invitation_dialog_seat_num,
                confirm_invitation_dialog_costpp,
                confirm_invitation_dialog_comment;

    public ConfirmInvitationDialog() { }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        dialog=new ProgressDialog(getContext());
        return inflater.inflate(R.layout.fragment_confirm_invitation_dialog, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
        getDialog().setTitle("確定刊登共乘");

        getDialog().setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                getTargetFragment().onActivityResult(-1, -1, null);
            }
        });

        setupViews();
        setupPreviewData();

        confirm_invitation_dialog_savebtn=(Button)getView().findViewById(R.id.confirm_invitation_dialog_savebtn);
        confirm_invitation_dialog_savebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //if user id is not valid, terminate method call
                if (MainActivity.fb_profile_id == null || MainActivity.fb_profile_id.length() == 0) {
                    Toast.makeText(getContext(), "請先登入", Toast.LENGTH_SHORT);
                    return;
                }

                //when clicked for 1st time, change text and return
                if (!firstClicked) {
                    confirm_invitation_dialog_savebtn.setText("確定刊登(點擊刊登)");
                    firstClicked = true;
                    return;
                }

                //SendServerCarpoolData sendPostData = new SendServerCarpoolData();
                //sendPostData.execute(CreateCarpoolInvitation.carpoolData);
                firstClicked = false;
                getTargetFragment().onActivityResult(getTargetRequestCode(),getTargetRequestCode(), null);
                getDialog().dismiss();
            }
        });
    }

    void setupViews(){
        confirm_invitation_dialog_src       = (TextView)getView().findViewById(R.id.confirm_invitation_dialog_src);
        confirm_invitation_dialog_dst       = (TextView)getView().findViewById(R.id.confirm_invitation_dialog_dst);
        confirm_invitation_dialog_date      = (TextView)getView().findViewById(R.id.confirm_invitation_dialog_date);
        confirm_invitation_dialog_time      = (TextView)getView().findViewById(R.id.confirm_invitation_dialog_time);
        confirm_invitation_dialog_vehicle_type= (TextView)getView().findViewById(R.id.confirm_invitation_dialog_vehicle_type);
        confirm_invitation_dialog_seat_num  = (TextView)getView().findViewById(R.id.confirm_invitation_dialog_seat_num);
        confirm_invitation_dialog_costpp    = (TextView)getView().findViewById(R.id.confirm_invitation_dialog_costpp);
        confirm_invitation_dialog_comment   = (TextView)getView().findViewById(R.id.confirm_invitation_dialog_comment);
    }

    void setupPreviewData(){
        previewData=CreateCarpoolInvitation.carpoolData.getExtras();
        confirm_invitation_dialog_src.setText(getArguments().getString("previewSrc"));
        confirm_invitation_dialog_dst.setText(getArguments().getString("previewDst"));
        confirm_invitation_dialog_date.setText(getArguments().getString("previewDate"));
        confirm_invitation_dialog_time.setText(getArguments().getString("previewTime"));
        confirm_invitation_dialog_vehicle_type.setText(getArguments().getString("previewVehicle"));
        confirm_invitation_dialog_seat_num.setText(getArguments().getString("previewSeat"));
        confirm_invitation_dialog_costpp.setText(getArguments().getString("previewCostpp"));
        confirm_invitation_dialog_comment.setText(getArguments().getString("previewComment"));
    }

    @Override
    public void onStart(){
        super.onStart();
    }

    @Override
    public void onPause(){
        super.onPause();
        dismiss();
    }
}
