package com.example.junhao.carpo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by junhao on 2016/8/25.
 */
public class ResponseReceiver extends BroadcastReceiver{
    MainActivity ma;
    public ResponseReceiver(MainActivity maContext){
        ma=maContext;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        /* Determine kinds of notifications */
        try {
            JSONArray userNotificationArr=new JSONArray(intent.getExtras().getString(GetNotification.Constants.EXTENDED_DATA_STATUS));

            for(int i=0;i<userNotificationArr.length();i++){

                JSONObject notificationObj=userNotificationArr.getJSONObject(i);
                String uId=MainActivity.fb_profile_id;
                String notificationMsg="";
                //Role of user is driver
                if(notificationObj.getString("owner").equals(uId) && notificationObj.getString("request_read_state").equals("d_hasnt_read") && notificationObj.getString("request_status").equals("passenger_sent")){
                    notificationMsg+="收到共乘請求\n";
                }
                //Role of user is passenger
                if(notificationObj.getString("fk_user_fb").equals(uId) && notificationObj.getString("request_read_state").equals("p_hasnt_read") && notificationObj.getString("request_status").equals("driver_approved")){
                    notificationMsg+="收到車主回复\n";
                }

                //TODO:more notification types to be added

                ma.makeNotification(notificationMsg);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        //ma.haha(intent.getExtras().getString(GetNotification.Constants.EXTENDED_DATA_STATUS));
    }
}
