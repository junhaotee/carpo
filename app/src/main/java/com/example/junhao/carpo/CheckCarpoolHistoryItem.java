package com.example.junhao.carpo;

/**
 * Created by junhao on 2016/8/23.
 */

import org.json.JSONException;
import org.json.JSONObject;

public class CheckCarpoolHistoryItem {

    private JSONObject carpoolHistoryJSONObject;

    //stores location result of one item
    public CheckCarpoolHistoryItem(JSONObject carpoolHistoryResult){
        carpoolHistoryJSONObject =carpoolHistoryResult;
    }

    //empty constructor is called when no results from server
    public CheckCarpoolHistoryItem(){
        carpoolHistoryJSONObject =null;
    }

    //a convenient method for ResultListAdapter to retrieve json values
    public String getItemValue(String JSONKey){
        try {
            return carpoolHistoryJSONObject.getString(JSONKey).toString();
        }
        catch (JSONException e) { e.printStackTrace();}

        return null;
    }

    public boolean isItemNull(){
        if(carpoolHistoryJSONObject ==null)
            return true;
        else
            return false;
    }

    public JSONObject getJSONObject(){
        return carpoolHistoryJSONObject;
    }
}
