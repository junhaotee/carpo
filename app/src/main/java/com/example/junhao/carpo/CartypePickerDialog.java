package com.example.junhao.carpo;


import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class CartypePickerDialog extends DialogFragment {

    Spinner cartype_picker_spinner;
    Button cartype_picker_saveBtn;
    ArrayAdapter spinnerAdapter;

    public CartypePickerDialog() { }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);

        getDialog().setTitle("選擇交通工具類型");

        cartype_picker_spinner      = (Spinner)getView( ).findViewById(R.id.cartype_picker_spinner);
        cartype_picker_saveBtn      = (Button)getView().findViewById(R.id.cartype_picker_saveBtn);
        spinnerAdapter              = new ArrayAdapter(getContext(),android.R.layout.simple_list_item_1);

        cartype_picker_spinner.setAdapter(spinnerAdapter);
        spinnerAdapter.notifyDataSetChanged();

        new GetVehicles().execute();

        cartype_picker_saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent seatData = new Intent();
                seatData.putExtra("vehicle_type", cartype_picker_spinner.getSelectedItem().toString());
                seatData.putExtra("vehicle_pos", cartype_picker_spinner.getSelectedItemPosition()+1);
                getTargetFragment().onActivityResult(getTargetRequestCode(), getTargetRequestCode(), seatData);
                getDialog().dismiss();
            }
        });

        getDialog().setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                getTargetFragment().onActivityResult(-1, -1, null);
            }
        });
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onPause(){
        super.onPause();
        dismiss();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_cartype_picker_dialog, container, false);
    }

    class GetVehicles extends AsyncTask<Void,Void,Integer>{

        String serverResponse;
        JSONArray vehicleType;

        @Override
        protected Integer doInBackground(Void... params) {

            try {
                //send query and read response into string
                String vehicleQuery="http://140.116.83.83/api/get_vehicle.php";
                URL url = new URL(vehicleQuery);
                URLConnection urlConn=url.openConnection();

                urlConn.setConnectTimeout(1000);
                urlConn.setReadTimeout(1000);
                
                InputStream is=urlConn.getInputStream();
                serverResponse=convertStreamToString(is);

                vehicleType=new JSONArray(serverResponse.toString());

            }
            catch (MalformedURLException e) { e.printStackTrace(); }
            catch (IOException e) { e.printStackTrace(); }
            catch (JSONException e) { e.printStackTrace(); }
            return null;
        }

        @Override
        protected void onPostExecute(Integer i){
            try {
                for(int k=0;k<vehicleType.length();k++){
                    JSONObject vehicle=(JSONObject)vehicleType.get(k);
                    spinnerAdapter.add(vehicle.get("vehicle_name").toString());
                }
            } catch (JSONException e) { e.printStackTrace(); }
            spinnerAdapter.notifyDataSetChanged();
        }

        String convertStreamToString(java.io.InputStream is) {
            java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
            return s.hasNext() ? s.next() : "";
        }
    }
}
