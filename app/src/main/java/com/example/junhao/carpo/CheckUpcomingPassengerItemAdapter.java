package com.example.junhao.carpo;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;


public class CheckUpcomingPassengerItemAdapter extends ArrayAdapter<CheckUpcomingPassengerItem> {

    RelativeLayout container;

    public CheckUpcomingPassengerItemAdapter(Context context, ArrayList listItems) {
        super(context, R.layout.check_upcoming_passenger_item, listItems);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){

        View row = convertView;
        CheckUpcomingPassengerItemViewHolder holder;

        if(row==null) {
            LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(getContext().LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.check_upcoming_passenger_item, parent, false);
            holder = new CheckUpcomingPassengerItemViewHolder(row);
            row.setTag(holder);
        }else {
            holder = (CheckUpcomingPassengerItemViewHolder)row.getTag();
        }

        //if no result if found
        if(getItem(position).isItemNull()){
            return row;
        }

        container=holder.rlayout;

        /* Set data to views */

        holder.check_passenger_item_src_name.setText(getItem(position).getItemValue("start"));
        holder.check_passenger_item_dst_name.setText(getItem(position).getItemValue("destination"));

        setupDateAndTime(getItem(position).getItemValue("timeDepart"), holder.check_passenger_item_dom_1, holder.check_passenger_item_month_1, holder.check_passenger_item_hour_1, holder.check_passenger_item_min_1, holder.check_passenger_item_ampm_1);

        int hasInterval=Integer.parseInt(getItem(position).getItemValue("hasInterval"));

        if(hasInterval==1){ //has an interval then set text, else set visibility to gone
            Log.d("hasInterval","executed");
            setupDateAndTime(getItem(position).getItemValue("timeDepart2").toString(), holder.check_passenger_item_dom_1, holder.check_passenger_item_month_1, holder.check_passenger_item_hour_2, holder.check_passenger_item_min_2, holder.check_passenger_item_ampm_2);
            holder.check_passenger_item_interval_2.setVisibility(View.VISIBLE);
        }else{
            holder.check_passenger_item_interval_2.setVisibility(View.GONE);
        }

        String isTimeNegoable=(Integer.parseInt(getItem(position).getItemValue("isAnytime"))==1)?"任意時間":(Integer.parseInt(getItem(position).getItemValue("timeNego"))==1)?"可議":"";
        holder.check_passenger_item_time_is_negoable.setText(isTimeNegoable);

//        holder.check_passenger_item_vehicle_name.setText(getItem(position).getItemValue("vehicle_name"));

        int totalCost=Integer.parseInt(getItem(position).getItemValue("costpp"))*Integer.parseInt(getItem(position).getItemValue("num_seat_requested"));
        holder.check_passenger_item_totalcost.setText(totalCost+"");

        holder.check_passenger_item_seat_count.setText(getItem(position).getItemValue("num_seat_requested"));

        holder.check_passenger_item_passenger_name.setText(getItem(position).getItemValue("fName")+getItem(position).getItemValue("lName"));

        holder.check_passenger_item_status.setText(setupDaysBefore(getItem(position).getItemValue("timeDepart")));

        holder.check_passenger_item_request_timeago.setText(TimeAgo.getTimeAgo(getItem(position).getItemValue("date_passenger_request")));

        //pass user facebook id and imageview of the profile picture
        new GetProfilePic().execute(getItem(position).getItemValue("fk_user_fb"), holder.check_passenger_item_fb_profile_pic);

        return row;
    }

    public String setupDaysBefore(String timeDepart){
        String dateStart = new Timestamp(System.currentTimeMillis()).toString();
        String dateStop = timeDepart;

        //HH converts hour in 24 hours format (0-23), day calculation
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Date d1 = null;
        Date d2 = null;

        try {
            d1 = format.parse(dateStart);
            d2 = format.parse(dateStop);
        }catch (ParseException e) { e.printStackTrace(); }

        //in milliseconds
        long diff = d2.getTime() - d1.getTime();

        long diffDays = diff / (24 * 60 * 60 * 1000);

        String durationBeforeMsg="";
        if(diffDays >= 1 && diffDays < 7 ){
            durationBeforeMsg=diffDays+"天後";
        }else if(diffDays >= 7 && diffDays <30){
            int weeks=(int)diffDays/7;
            durationBeforeMsg=weeks+"個星期後";
        }else if(diffDays >= 30){
            int months=(int)diffDays/30;
            durationBeforeMsg=months+"個月後";
        }

        return durationBeforeMsg;
    }

    public void setupDateAndTime(String datetimeFormat,TextView dom,TextView month,TextView hour,TextView minute,TextView ampm){

        Log.d("datetimeFormat",datetimeFormat);

        //string processing to extract ymd hms
        String[] dateTime=datetimeFormat.split(" ");
        String[] ymd=dateTime[0].split("-");
        String[] hms=dateTime[1].split(":");

        //find day of week
        // Note that Month value is 0-based. e.g., 0 for January.
        Calendar calendar = new GregorianCalendar(Integer.parseInt(ymd[0]),Integer.parseInt(ymd[1])-1,Integer.parseInt(ymd[2]));
        int day = calendar.get(Calendar.DAY_OF_WEEK);

        String dow="";
        switch (day) {
            case Calendar.MONDAY:{dow="一";break;}
            case Calendar.TUESDAY:{dow="二";break;}
            case Calendar.WEDNESDAY:{dow="三";break;}
            case Calendar.THURSDAY:{dow="四";break;}
            case Calendar.FRIDAY:{dow="五";break;}
            case Calendar.SATURDAY:{dow="六";break;}
            case Calendar.SUNDAY:{dow="日";break;}
        }

        //removing 0 prefix for month,day,hour,minute
        if(Integer.parseInt(ymd[1].charAt(0)+"")==0)
            ymd[1]=ymd[1].charAt(1)+"";

        if(Integer.parseInt(ymd[2].charAt(0)+"")==0)
            ymd[2]=ymd[2].charAt(1)+"";

        if(Integer.parseInt(hms[0].charAt(0) + "")==0)
            hms[0]=hms[0].charAt(1)+"";

        //detect AM or PM
        String AMPM=(Integer.parseInt(hms[0])>=12)?"PM":"AM";

        //make hour 13 to 1
        if(Integer.parseInt(hms[0])>=13){
            hms[0]=" "+(Integer.parseInt(hms[0])-12)+"";
        }

        //set formatted date output to ui
        dom.setText("("+dow+") "+ymd[2]);
        month.setText(ymd[1]);
        hour.setText(hms[0]);
        minute.setText(hms[1]);
        ampm.setText(AMPM);

    }

    class GetProfilePic extends AsyncTask<Object,Void,Bitmap> {

        ImageView fb_profile_pic;
        String fbId;

        @Override
        protected Bitmap doInBackground(Object... params) {
            Bitmap profilePic=null;
            fb_profile_pic=(ImageView)params[1];
            fbId=(String)params[0];

            try {

                URL profileURL=new URL("https://graph.facebook.com/"+fbId+"/picture?type=square");
                Log.d("pp","https://graph.facebook.com/"+fbId+"/picture?type=square");
                URLConnection urlConn=profileURL.openConnection();
                urlConn.setReadTimeout(1500);
                urlConn.setConnectTimeout(1500);
                profilePic= BitmapFactory.decodeStream((InputStream) urlConn.getContent());

            }
            catch (MalformedURLException e) { e.printStackTrace(); }
            catch (IOException e) { e.printStackTrace(); }

            return profilePic;
        }

        @Override
        protected void onPostExecute(Bitmap profilePic){
            if(profilePic==null) return;
            fb_profile_pic.setImageBitmap(profilePic);
        }
    }
}

class CheckUpcomingPassengerItemViewHolder {

        TextView    check_passenger_item_src_name,
                    check_passenger_item_dst_name,
                    check_passenger_item_dom_1,
                    check_passenger_item_month_1,
                    check_passenger_item_hour_1,
                    check_passenger_item_min_1,
                    check_passenger_item_ampm_1,
                    check_passenger_item_hour_2,
                    check_passenger_item_min_2,
                    check_passenger_item_ampm_2,
                    check_passenger_item_time_is_negoable,
                    check_passenger_item_vehicle_name,
                    check_passenger_item_totalcost,
                    check_passenger_item_seat_count,
                    check_passenger_item_request_timeago,
                    check_passenger_item_passenger_name,
                    check_passenger_item_status;

    ImageView       check_passenger_item_fb_profile_pic;

    RelativeLayout  rlayout;

    LinearLayout    check_passenger_item_interval_2;

    CheckUpcomingPassengerItemViewHolder(View v) {

        check_passenger_item_src_name               = (TextView)v.findViewById(R.id.check_passenger_item_src_name);
        check_passenger_item_dst_name               = (TextView)v.findViewById(R.id.check_passenger_item_dst_name);
        check_passenger_item_dom_1                  = (TextView)v.findViewById(R.id.check_passenger_item_dom_1);
        check_passenger_item_month_1                = (TextView)v.findViewById(R.id.check_passenger_item_month_1);
        check_passenger_item_hour_1                 = (TextView)v.findViewById(R.id.check_passenger_item_hour_1);
        check_passenger_item_min_1                  = (TextView)v.findViewById(R.id.check_passenger_item_min_1);
        check_passenger_item_ampm_1                 = (TextView)v.findViewById(R.id.check_passenger_item_ampm_1);
        check_passenger_item_hour_2                 = (TextView)v.findViewById(R.id.check_passenger_item_hour_2);
        check_passenger_item_min_2                  = (TextView)v.findViewById(R.id.check_passenger_item_min_2);
        check_passenger_item_ampm_2                 = (TextView)v.findViewById(R.id.check_passenger_item_ampm_2);
        check_passenger_item_time_is_negoable       = (TextView)v.findViewById(R.id.check_passenger_item_time_is_negoable);
        check_passenger_item_vehicle_name           = (TextView)v.findViewById(R.id.check_passenger_item_vehicle_name);
        check_passenger_item_totalcost              = (TextView)v.findViewById(R.id.check_passenger_item_totalcost);
        check_passenger_item_seat_count             = (TextView)v.findViewById(R.id.check_passenger_item_seat_count);
        check_passenger_item_passenger_name         = (TextView)v.findViewById(R.id.check_passenger_item_passenger_name);
        check_passenger_item_request_timeago        = (TextView)v.findViewById(R.id.check_passenger_item_request_timeago);
        check_passenger_item_status                 = (TextView)v.findViewById(R.id.check_passenger_item_status);

        check_passenger_item_fb_profile_pic         = (ImageView)v.findViewById(R.id.check_passenger_item_fb_profile_pic);
        rlayout                                     = (RelativeLayout)v.findViewById(R.id.rlayout);

        check_passenger_item_interval_2             = (LinearLayout) v.findViewById(R.id.check_passenger_item_interval_2);

    }
}


