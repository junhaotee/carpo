package com.example.junhao.carpo;

/**
 * Created by junhao on 2016/9/23.
 */

public class PassengerTabFactory {

    static ManageCarpool getTabContainer(){
        return new ManageCarpool();
    }

    static CheckCarpoolHistoryTab getCarpoolHistory(){
        return new CheckCarpoolHistoryTab();
    }

    static CheckUpcomingCarpoolTab getUpcomingCarpool(){
        return new CheckUpcomingCarpoolTab();
    }

    static CheckPassengerRequestStatusTab getSentRequest(){
        return new CheckPassengerRequestStatusTab();
    }

    static CarpoolDetailTab getCarpoolDetail(){
        return new CarpoolDetailTab();
    }

    static CancelDriverUnapprovedRequestTab cancelUnapprovedRequest(){
        return new CancelDriverUnapprovedRequestTab();
    }

    static CancelDriverApprovedRequestTab cancelApprovedRequest(){
        return new CancelDriverApprovedRequestTab();
    }
}
