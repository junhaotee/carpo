package com.example.junhao.carpo;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;


public class ApprovePassengerRequestTab extends Fragment {

    boolean                                 loading=false;
    boolean                                 itemMax=false;
    int                                     currentItemOffset =0;

    View                                    footerView;
    Fragment                                currentFragment=this;
    ListView                                approve_passenger_listview;
    TextView                                approve_passenger_request_count,listview_footer_tv_stat;

    ArrayAdapter                            passengerRequestItemAdapter;
    ArrayList<ApprovePassengerRequestItem>  passengerRequestArrList;
    String                                  passengerRequestResults;
    static ApprovePassengerRequestItem      chosenApprovePassengerRequestItem;

    public ApprovePassengerRequestTab() { }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_approve_passenger_request_tab, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);

        setupViews();
        setupViewsData();
    }

    void setupViews(){
        footerView                              = ((LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.listview_footer, null, false);
        listview_footer_tv_stat                 = (TextView)footerView.findViewById(R.id.listview_footer_tv_stat);
        approve_passenger_request_count         = (TextView)getView().findViewById(R.id.approve_passenger_request_count);
        approve_passenger_listview              = (ListView)getView().findViewById(R.id.approve_passenger_listview);
        passengerRequestArrList                 = new ArrayList();
        passengerRequestItemAdapter             = new ApprovePassengerRequestItemAdapter(getContext(),passengerRequestArrList);
    }

    void setupViewsData(){
        approve_passenger_listview.setAdapter(passengerRequestItemAdapter);
        approve_passenger_listview.addFooterView(footerView);
        approve_passenger_listview.setOnScrollListener(reservationItemLvListener);
        approve_passenger_listview.setOnItemClickListener(adapterItemOnClick);
    }

    private AdapterView.OnItemClickListener adapterItemOnClick=new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            if(position>=passengerRequestArrList.size())return;

            chosenApprovePassengerRequestItem=passengerRequestArrList.get(position);

            ApprovePassengerRequestDialog approvePassengerRequestDialog =new ApprovePassengerRequestDialog();
            approvePassengerRequestDialog.setTargetFragment(currentFragment, 11);
            approvePassengerRequestDialog.show(getChildFragmentManager(), "");
        }
    };

    AbsListView.OnScrollListener reservationItemLvListener=new AbsListView.OnScrollListener() {

        @Override
        public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            int lastInScreen = firstVisibleItem + visibleItemCount;
            if ((lastInScreen == totalItemCount && !loading &&!itemMax)) {
                loading = true;
                new GetPassengerResults().execute();
            }
        }

        @Override
        public void onScrollStateChanged(AbsListView absListView, int i) { }
    };

    @Override
    public void onActivityResult(int requestCode,int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode){
            case -1:{MainActivity.dialog.hide();}break;
            case 11:{onApprovePassengerDialogReturn(data,resultCode);}break;
        }
    }

    void onApprovePassengerDialogReturn(Intent data,int resultCode){
        //0= request accepted, 1=request rejected
        new SetPassengerRequest().execute(resultCode);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    class GetPassengerResults extends AsyncTask<Void,Void,Integer> {
        @Override
        protected Integer doInBackground(Void... params) {

            try {
                //defines query url, get current driver's carpool
                String queryString="http://140.116.83.83/api/get_passenger_request.php?fb="+MainActivity.fb_profile_id+"&currentItemOffset="+currentItemOffset;
                URL url = new URL(queryString);
                URLConnection urlConn=url.openConnection();

                urlConn.setReadTimeout(1000);
                urlConn.setConnectTimeout(1000);

                passengerRequestResults=convertStreamToString(urlConn.getInputStream());

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                passengerRequestResults="";//if server connect/read timeout, return empty list
                e.printStackTrace();
            }
            return null;
        }

        //onPostExecute is UI thread, update the views here
        @Override
        protected void onPostExecute(Integer result) {
            try{
                //if no result is found, add an empty item and quit
                if(passengerRequestResults.length()==0){
                    itemMax=true;
                    listview_footer_tv_stat.setText("");
                }

                //parsed server response into json
                JSONArray passengerRequestArr=new JSONArray(passengerRequestResults);

                //if no result is found, add an empty item and quit
                if(passengerRequestArr.length()==0){
                    approve_passenger_request_count.setText(0+"");
                    itemMax=true;
                    listview_footer_tv_stat.setText("");
                }

                //if there were results, populates found result into arraylist
                for(int k=0;k<passengerRequestArr.length();k++){
                    JSONObject passengerRequestObj = new JSONObject(passengerRequestArr.get(k).toString());
                    passengerRequestArrList.add(new ApprovePassengerRequestItem(passengerRequestObj));
                }

                //notify listview data changed, and unblock dialog
                passengerRequestItemAdapter.notifyDataSetChanged();

                currentItemOffset +=passengerRequestArr.length();
                approve_passenger_request_count.setText(currentItemOffset+"");
                loading=false;

            }catch(JSONException jsone){ jsone.printStackTrace(); }
        }

        //method to convert input stream to string, used after reading input stream
        String convertStreamToString(java.io.InputStream is) {
            java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
            return s.hasNext() ? s.next() : "";
        }
    }

    class SetPassengerRequest extends AsyncTask<Integer,Void,Integer> {

        String passengerRequestJSON;

        @Override
        protected Integer doInBackground(Integer... params) {

            try {
                String passenger_request_id = ApprovePassengerRequestTab.chosenApprovePassengerRequestItem.getItemValue("passenger_request_id");
                String fk_carpool_id        = ApprovePassengerRequestTab.chosenApprovePassengerRequestItem.getItemValue("fk_carpool_id");
                String num_seat_requested   = ApprovePassengerRequestTab.chosenApprovePassengerRequestItem.getItemValue("num_seat_requested");
                int request_status          = params[0];
                String fb                   = MainActivity.fb_profile_id;

                String queryString="http://140.116.83.83/api/set_passenger_request.php?passenger_request_id="+passenger_request_id+"&fk_carpool_id="+fk_carpool_id+"&num_seat_requested="+num_seat_requested+"&request_status="+request_status+"&fb="+fb;
                Log.d("queryString",queryString);
                URL url = new URL(queryString);
                URLConnection urlConn=url.openConnection();

                urlConn.setReadTimeout(1000);
                urlConn.setConnectTimeout(1000);

                passengerRequestJSON=convertStreamToString(urlConn.getInputStream());

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                passengerRequestJSON="";//if server connect/read timeout, return empty list
                e.printStackTrace();
            }
            return null;
        }

        //onPostExecute is UI thread, updat the views here
        @Override
        protected void onPostExecute(Integer result) {
            try{
                //clear previous results
                passengerRequestArrList.clear();

                //if no result is found, add an empty item and quit
                if(passengerRequestJSON.length()==0){
                    /*
                    passengerRequestArrList.add(new ApprovePassengerRequestItem());
                    passengerRequestItemAdapter.notifyDataSetChanged();
                    */
                    approve_passenger_listview.setVisibility(View.GONE);
                    approve_passenger_request_count.setText(0 + "");
                    return;
                }

                //parsed server response into json
                JSONArray requestArr=new JSONArray(passengerRequestJSON);

                //if no result is found, add an empty item and quit
                if(requestArr.length()==0){
                    approve_passenger_request_count.setText(0+"");
                    /*
                    passengerRequestArrList.add(new ApprovePassengerRequestItem());
                    passengerRequestItemAdapter.notifyDataSetChanged();
                    */
                    return;
                }

                //if there were results, populates found result into arraylist
                for(int k=0;k<requestArr.length();k++){
                    JSONObject requestObj = new JSONObject(requestArr.get(k).toString());
                    passengerRequestArrList.add(new ApprovePassengerRequestItem(requestObj));
                }

                //notify listview data changed, and unblock dialog
                approve_passenger_request_count.setText(requestArr.length() + "");
                passengerRequestItemAdapter.notifyDataSetChanged();

            }catch(JSONException jsone){ jsone.printStackTrace(); }
        }

        //method to convert input stream to string, used after reading input stream
        String convertStreamToString(java.io.InputStream is) {
            java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
            return s.hasNext() ? s.next() : "";
        }
    }
}