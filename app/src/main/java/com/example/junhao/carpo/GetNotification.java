package com.example.junhao.carpo;

import android.app.IntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by junhao on 2016/8/25.
 */
public class GetNotification extends IntentService {

    public static boolean hasReadPreviousNotification=true;

    public GetNotification(){
        super("GetNotification");
    }

    public final class Constants {
        public static final String BROADCAST_ACTION ="com.example.junhao.carpo.Broadcast";
        public static final String EXTENDED_DATA_STATUS ="com.example.junhao.carpo.CarpoolNotification";
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        Intent localIntent;
        while(true){

            try {
                Thread.sleep(5000);
                if(!hasReadPreviousNotification) return;

                String serverResponse   = pullServerNotification();
                localIntent             = new Intent(Constants.BROADCAST_ACTION);

                if(serverResponse.length()==0)continue;

                localIntent.putExtra(Constants.EXTENDED_DATA_STATUS,serverResponse);
                LocalBroadcastManager.getInstance(this).sendBroadcast(localIntent);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        return START_STICKY;
    }


    protected String pullServerNotification(){
        String serverResponse="";
        try {
            //defines query url, get current driver's carpool
            //String queryString="http://140.116.83.83/api/get_notification.php?fb="+MainActivity.fb_profile_id;
            String queryString="http://140.116.83.83/api/get_notification.php?fk_user_fb="+MainActivity.fb_profile_id;
            URL url = new URL(queryString);
            URLConnection urlConn=url.openConnection();

            urlConn.setReadTimeout(1000);
            urlConn.setConnectTimeout(1000);

            serverResponse=convertStreamToString(urlConn.getInputStream());

        } catch (MalformedURLException e) {
            serverResponse="";
            e.printStackTrace();
        } catch (IOException e) {
            serverResponse="";//if server connect/read timeout, return empty list
            e.printStackTrace();
        }

        return (serverResponse.length()==0)?"":serverResponse;

    }

    String convertStreamToString(java.io.InputStream is) {
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }



}
