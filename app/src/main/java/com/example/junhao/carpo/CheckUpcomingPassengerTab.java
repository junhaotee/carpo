package com.example.junhao.carpo;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;


public class CheckUpcomingPassengerTab extends Fragment {

    boolean                                 loading=false;
    boolean                                 itemMax=false;
    int                                     currentItemOffset =0;
    static CheckUpcomingPassengerItem       chosenCheckPassengerItem;

    View                                    footerView;
    Fragment                                currentFragment=this;
    ListView                                check_passenger_listview;
    CheckUpcomingPassengerItemAdapter       checkPassengerItemAdapter;
    ArrayList<CheckUpcomingPassengerItem>   checkPassengerArrList;
    String                                  checkPassengerResults;

    TextView                                check_passenger_count,
                                            listview_footer_tv_stat;


    public CheckUpcomingPassengerTab() { }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_check_upcoming_passenger_tab, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);

        setupViews();
        setupViewsData();
    }

    void setupViews(){
        footerView                              = ((LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.listview_footer, null, false);
        listview_footer_tv_stat                 = (TextView)footerView.findViewById(R.id.listview_footer_tv_stat);
        check_passenger_count                   = (TextView)getView().findViewById(R.id.check_passenger_count);
        check_passenger_listview                = (ListView)getView().findViewById(R.id.check_passenger_listview);
        checkPassengerArrList                   = new ArrayList();
        checkPassengerItemAdapter               = new CheckUpcomingPassengerItemAdapter(getContext(),checkPassengerArrList);
    }

    void setupViewsData(){
        check_passenger_listview.setAdapter(checkPassengerItemAdapter);
        check_passenger_listview.addFooterView(footerView);
        check_passenger_listview.setOnScrollListener(checkPassengerItemListener);
        check_passenger_listview.setOnItemClickListener(adapterItemOnClick);

    }

    private AdapterView.OnItemClickListener adapterItemOnClick=new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            if(position>=checkPassengerArrList.size())return;

            chosenCheckPassengerItem=(checkPassengerArrList.get(position));

            CheckUpcomingPassengerDialog checkPassengerDialog=new CheckUpcomingPassengerDialog();
            checkPassengerDialog.setTargetFragment(currentFragment, 12);
            checkPassengerDialog.show(getChildFragmentManager(), "");

            return;
        }
    };

    AbsListView.OnScrollListener checkPassengerItemListener=new AbsListView.OnScrollListener() {

        @Override
        public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            int lastInScreen = firstVisibleItem + visibleItemCount;
            if ((lastInScreen == totalItemCount && !loading &&!itemMax)) {
                loading = true;
                new GetUpcomingPassengerResults().execute();
            }
        }

        @Override
        public void onScrollStateChanged(AbsListView absListView, int i) { }
    };

    @Override
    public void onActivityResult(int requestCode,int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode){
            case -1:{MainActivity.dialog.hide();}break;
            case 12:{onCheckPassengerDialogReturn(data, resultCode);}break;
        }
    }

    void onCheckPassengerDialogReturn(Intent data,int resultCode){
        new CancelPassengerRequest().execute(resultCode);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    class GetUpcomingPassengerResults extends AsyncTask<Void,Void,Integer> {
        @Override
        protected Integer doInBackground(Void... params) {

            try {
                //defines query url, get current driver's carpool
                String queryString="http://140.116.83.83/api/get_upcoming_passenger.php?fb="+MainActivity.fb_profile_id+"&currentItemOffset="+currentItemOffset;
                URL url = new URL(queryString);
                Log.d("queryString",queryString);
                URLConnection urlConn=url.openConnection();

                urlConn.setReadTimeout(1000);
                urlConn.setConnectTimeout(1000);

                checkPassengerResults=convertStreamToString(urlConn.getInputStream());

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                checkPassengerResults="";//if server connect/read timeout, return empty list
                e.printStackTrace();
            }
            return null;
        }

        //onPostExecute is UI thread, update the views here
        @Override
        protected void onPostExecute(Integer result) {
            try{

                //if no result is found, add an empty item and quit
                if(checkPassengerResults.length()==0){
                    itemMax=true;
                    listview_footer_tv_stat.setText("");
                }

                //parsed server response into json
                JSONArray passengerRequestArr=new JSONArray(checkPassengerResults);

                //if no result is found, add an empty item and quit
                if(passengerRequestArr.length()==0){
                    itemMax=true;
                    listview_footer_tv_stat.setText("");
                    check_passenger_count.setText(0+"");
                }

                //if there were results, populates found result into arraylist
                for(int k=0;k<passengerRequestArr.length();k++){
                    JSONObject passengerRequestObj = new JSONObject(passengerRequestArr.get(k).toString());
                    checkPassengerArrList.add(new CheckUpcomingPassengerItem(passengerRequestObj));
                }

                checkPassengerItemAdapter.notifyDataSetChanged();

                currentItemOffset +=passengerRequestArr.length();
                check_passenger_count.setText(currentItemOffset+"");
                loading=false;

            }catch(JSONException jsone){ jsone.printStackTrace(); }
        }

        //Method to convert input stream to string, used after reading input stream
        String convertStreamToString(java.io.InputStream is) {
            java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
            return s.hasNext() ? s.next() : "";
        }
    }

    class CancelPassengerRequest extends AsyncTask<Integer,Void,Integer> {

        String passengerRequestJSON;

        @Override
        protected Integer doInBackground(Integer... params) {

            try {
                String owner                = chosenCheckPassengerItem.getItemValue("owner");
                String passenger_request_id = chosenCheckPassengerItem.getItemValue("passenger_request_id");
                String num_seat_requested   = chosenCheckPassengerItem.getItemValue("num_seat_requested");
                String cId                  = chosenCheckPassengerItem.getItemValue("cId");

                String queryString="http://140.116.83.83/api/cancel_passenger_request.php?owner="+owner+"&passenger_request_id="+passenger_request_id+"&num_seat_requested="+num_seat_requested+"&cId="+cId+"&fb="+MainActivity.fb_profile_id;
                Log.d("queryString",queryString);
                URL url = new URL(queryString);
                URLConnection urlConn=url.openConnection();

                urlConn.setReadTimeout(1000);
                urlConn.setConnectTimeout(1000);

                passengerRequestJSON=convertStreamToString(urlConn.getInputStream());

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                passengerRequestJSON="";//if server connect/read timeout, return empty list
                e.printStackTrace();
            }
            return null;
        }

        //onPostExecute is UI thread, updat the views here
        @Override
        protected void onPostExecute(Integer result) {
            try{
                //clear previous results
                checkPassengerArrList.clear();

                //if no result is found, add an empty item and quit
                if(passengerRequestJSON.length()==0){
                    check_passenger_listview.setVisibility(View.GONE);
                    check_passenger_count.setText(0 + "");
                    return;
                }

                //parsed server response into json
                JSONArray requestArr=new JSONArray(passengerRequestJSON);

                //if no result is found, add an empty item and quit
                if(requestArr.length()==0){
                    check_passenger_count.setText(0+"");
                    /*
                    passengerRequestArrList.add(new ApprovePassengerRequestItem());
                    passengerRequestItemAdapter.notifyDataSetChanged();
                    */
                    return;
                }

                //if there were results, populates found result into arraylist
                for(int k=0;k<requestArr.length();k++){
                    JSONObject requestObj = new JSONObject(requestArr.get(k).toString());
                    checkPassengerArrList.add(new CheckUpcomingPassengerItem(requestObj));
                }

                //notify listview data changed, and unblock dialog
                check_passenger_count.setText(requestArr.length() + "");
                checkPassengerItemAdapter.notifyDataSetChanged();

            }catch(JSONException jsone){ jsone.printStackTrace(); }
        }

        //method to convert input stream to string, used after reading input stream
        String convertStreamToString(java.io.InputStream is) {
            java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
            return s.hasNext() ? s.next() : "";
        }
    }
}