package com.example.junhao.carpo;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by junhao on 2015/12/7.
 */
public class CheckPassengerRequestStatusItem {

    private JSONObject reservationStatusItem;

    //stores location result of one item
    public CheckPassengerRequestStatusItem(JSONObject locationResult){
        reservationStatusItem=locationResult;
    }

    //empty constructor is called when no results from server
    public CheckPassengerRequestStatusItem(){
        reservationStatusItem=null;
    }

    //a convenient method for ResultListAdapter to retrieve json values
    public String getItemValue(String JSONKey){

        try {
            return reservationStatusItem.getString(JSONKey).toString();
        } catch (JSONException e) { e.printStackTrace();}
        return null;
    }

    public boolean isItemNull(){
        return (reservationStatusItem==null)?true:false;
    }

    public JSONObject getJSONObject(){
        return reservationStatusItem;
    }

}