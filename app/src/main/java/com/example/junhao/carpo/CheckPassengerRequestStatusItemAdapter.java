package com.example.junhao.carpo;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;


public class CheckPassengerRequestStatusItemAdapter extends ArrayAdapter<CheckPassengerRequestStatusItem> {

    RelativeLayout container;

    public CheckPassengerRequestStatusItemAdapter(Context context, ArrayList listItems) {
        super(context, R.layout.check_passenger_request_status_item, listItems);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){

        View row = convertView;
        CheckPassengerRequestStatusItemViewHolder holder;

        if(row==null) {
            LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(getContext().LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.check_passenger_request_status_item, parent, false);
            holder = new CheckPassengerRequestStatusItemViewHolder(row);
            row.setTag(holder);
        }else {
            holder = (CheckPassengerRequestStatusItemViewHolder)row.getTag();
        }

        new GetProfilePic().execute(getItem(position).getItemValue("fb").toString(),holder.passenger_reservation_status_item_iv_profile_pic);

        String driverName           = getItem(position).getItemValue("fName")+getItem(position).getItemValue("lName");
        String numSeatRequested     = getItem(position).getItemValue("num_seat_requested");
        String srcName              = getItem(position).getItemValue("start");
        String dstName              = getItem(position).getItemValue("destination");
        String requestedTimeAgo     = TimeAgo.getTimeAgo(getItem(position).getItemValue("date_passenger_request").toString());


        holder.passenger_reservation_status_item_tv_driver_name.setText(driverName);
        holder.passenger_reservation_status_item_seat_requested.setText(numSeatRequested);
        holder.passenger_reservation_status_item_src_name.setText(srcName);
        holder.passenger_reservation_status_item_dst_name.setText(dstName);
        holder.passenger_reservation_status_request_timestamp.setText(requestedTimeAgo);
        setupDateAndTime(getItem(position).getItemValue("timeDepart"), holder.passenger_reservation_status_item_dom_1, holder.passenger_reservation_status_item_month_1, holder.passenger_reservation_status_item_hour_1, holder.passenger_reservation_status_item_min_1, holder.passenger_reservation_status_item_ampm_1);

        int hasInterval=Integer.parseInt(getItem(position).getItemValue("hasInterval"));

        if(hasInterval==1){ //has an interval then set text, else set visibility to gone
            Log.d("hasInterval","executed");
            setupDateAndTime(getItem(position).getItemValue("timeDepart2").toString(), holder.passenger_reservation_status_item_dom_1, holder.passenger_reservation_status_item_month_1, holder.passenger_reservation_status_item_hour_2, holder.passenger_reservation_status_item_min_2, holder.passenger_reservation_status_item_ampm_2);
            holder.passenger_reservation_status_item_interval_2.setVisibility(View.VISIBLE);
        }else{
            holder.passenger_reservation_status_item_interval_2.setVisibility(View.GONE);
        }

        return row;
    }


    public void setupDateAndTime(String datetimeFormat,TextView dom,TextView month,TextView hour,TextView minute,TextView ampm){

        Log.d("datetimeFormat",datetimeFormat);

        //string processing to extract ymd hms
        String[] dateTime=datetimeFormat.split(" ");
        String[] ymd=dateTime[0].split("-");
        String[] hms=dateTime[1].split(":");

        //find day of week
        // Note that Month value is 0-based. e.g., 0 for January.
        Calendar calendar = new GregorianCalendar(Integer.parseInt(ymd[0]),Integer.parseInt(ymd[1])-1,Integer.parseInt(ymd[2]));
        int day = calendar.get(Calendar.DAY_OF_WEEK);

        String dow="";
        switch (day) {
            case Calendar.MONDAY:{dow="一";break;}
            case Calendar.TUESDAY:{dow="二";break;}
            case Calendar.WEDNESDAY:{dow="三";break;}
            case Calendar.THURSDAY:{dow="四";break;}
            case Calendar.FRIDAY:{dow="五";break;}
            case Calendar.SATURDAY:{dow="六";break;}
            case Calendar.SUNDAY:{dow="日";break;}
        }

        //removing 0 prefix for month,day,hour,minute
        if(Integer.parseInt(ymd[1].charAt(0)+"")==0)
            ymd[1]=ymd[1].charAt(1)+"";

        if(Integer.parseInt(ymd[2].charAt(0)+"")==0)
            ymd[2]=ymd[2].charAt(1)+"";

        if(Integer.parseInt(hms[0].charAt(0) + "")==0)
            hms[0]=hms[0].charAt(1)+"";

        //detect AM or PM
        String AMPM=(Integer.parseInt(hms[0])>=12)?"PM":"AM";

        //make hour 13 to 1
        if(Integer.parseInt(hms[0])>=13){
            hms[0]=" "+(Integer.parseInt(hms[0])-12)+"";
        }

        //set formatted date output to ui
        dom.setText("("+dow+") "+ymd[2]);
        month.setText(ymd[1]);
        hour.setText(hms[0]);
        minute.setText(hms[1]);
        ampm.setText(AMPM);

    }

    class GetProfilePic extends AsyncTask<Object,Void,Bitmap> {

        ImageView fb_profile_pic;
        String fbId;

        @Override
        protected Bitmap doInBackground(Object... params) {
            Bitmap profilePic=null;
            fb_profile_pic=(ImageView)params[1];
            fbId=(String)params[0];

            try {

                URL profileURL=new URL("https://graph.facebook.com/"+fbId+"/picture?type=square");
                Log.d("pp","https://graph.facebook.com/"+fbId+"/picture?type=square");
                URLConnection urlConn=profileURL.openConnection();
                urlConn.setReadTimeout(1500);
                urlConn.setConnectTimeout(1500);
                profilePic= BitmapFactory.decodeStream((InputStream) urlConn.getContent());

            }
            catch (MalformedURLException e) { e.printStackTrace(); }
            catch (IOException e) { e.printStackTrace(); }

            return profilePic;
        }

        @Override
        protected void onPostExecute(Bitmap profilePic){
            if(profilePic==null) return;
            fb_profile_pic.setImageBitmap(profilePic);
        }
    }
}

class CheckPassengerRequestStatusItemViewHolder {

    TextView        passenger_reservation_status_item_tv_driver_name,
                    passenger_reservation_status_item_seat_requested,
                    passenger_reservation_status_item_src_name,
                    passenger_reservation_status_item_dst_name,
                    passenger_reservation_status_item_dom_1,
                    passenger_reservation_status_item_month_1,
                    passenger_reservation_status_item_hour_1,
                    passenger_reservation_status_item_min_1,
                    passenger_reservation_status_item_hour_2,
                    passenger_reservation_status_item_min_2,
                    passenger_reservation_status_item_ampm_1,
                    passenger_reservation_status_item_ampm_2,
                    passenger_reservation_status_request_timestamp;

    ImageView       passenger_reservation_status_item_iv_profile_pic;
    LinearLayout    passenger_reservation_status_item_interval_2;

    CheckPassengerRequestStatusItemViewHolder(View v) {

        passenger_reservation_status_item_tv_driver_name    = (TextView)v.findViewById(R.id.passenger_reservation_status_item_tv_driver_name);
        passenger_reservation_status_item_seat_requested    = (TextView)v.findViewById(R.id.passenger_reservation_status_item_seat_requested);
        passenger_reservation_status_item_src_name          = (TextView)v.findViewById(R.id.passenger_reservation_status_item_src_name);
        passenger_reservation_status_item_dst_name          = (TextView)v.findViewById(R.id.passenger_reservation_status_item_dst_name);
        passenger_reservation_status_item_dom_1             = (TextView)v.findViewById(R.id.passenger_reservation_status_item_dom_1);
        passenger_reservation_status_item_month_1           = (TextView)v.findViewById(R.id.passenger_reservation_status_item_month_1);
        passenger_reservation_status_item_hour_1            = (TextView)v.findViewById(R.id.passenger_reservation_status_item_hour_1);
        passenger_reservation_status_item_min_1             = (TextView)v.findViewById(R.id.passenger_reservation_status_item_min_1);
        passenger_reservation_status_item_hour_2            = (TextView)v.findViewById(R.id.passenger_reservation_status_item_hour_2);
        passenger_reservation_status_item_min_2             = (TextView)v.findViewById(R.id.passenger_reservation_status_item_min_2);
        passenger_reservation_status_item_ampm_1            = (TextView)v.findViewById(R.id.passenger_reservation_status_item_ampm_1);
        passenger_reservation_status_item_ampm_2            = (TextView)v.findViewById(R.id.passenger_reservation_status_item_ampm_2);
        passenger_reservation_status_request_timestamp      = (TextView)v.findViewById(R.id.passenger_reservation_status_request_timestamp);

        passenger_reservation_status_item_iv_profile_pic    = (ImageView)v.findViewById(R.id.passenger_reservation_status_item_iv_profile_pic);

        passenger_reservation_status_item_interval_2        = (LinearLayout)v.findViewById(R.id.passenger_reservation_status_item_interval_2);

    }
}


