package com.example.junhao.carpo;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.DialogFragment;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.LatLng;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;


public class ConfirmReservationDialog extends DialogFragment {

    TextView        confirm_reservation_dialog_total_price,
                    confirm_reservation_dialog_src,
                    confirm_reservation_dialog_dst,
                    confirm_reservation_dialog_date,
                    confirm_reservation_dialog_time,
                    confirm_reservation_dialog_driver_comment;

    Spinner         seatNumSpinner;

    Button          double_confirm_reservation_btn;

    boolean         firstClicked=false;

    public ConfirmReservationDialog() { }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_confirm_reservation_dialog, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstance){

        super.onActivityCreated(savedInstance);

        setupViews();

        setupViewsData();
    }

    void setupViews(){
        confirm_reservation_dialog_total_price=(TextView)getView().findViewById(R.id.confirm_reservation_dialog_total_price);
        confirm_reservation_dialog_src=(TextView)getView().findViewById(R.id.confirm_reservation_dialog_src);
        confirm_reservation_dialog_dst=(TextView)getView().findViewById(R.id.confirm_reservation_dialog_dst);
        confirm_reservation_dialog_date=(TextView)getView().findViewById(R.id.confirm_reservation_dialog_date);
        confirm_reservation_dialog_time=(TextView)getView().findViewById(R.id.confirm_reservation_dialog_time);
        confirm_reservation_dialog_driver_comment=(TextView)getView().findViewById(R.id.confirm_reservation_dialog_driver_comment);
        double_confirm_reservation_btn=(Button)getView().findViewById(R.id.double_confirm_reservation_btn);
    }

    void setupViewsData(){
        getDialog().setTitle("確定人數 預訂座位");
        confirm_reservation_dialog_total_price.setText(SearchResultMap.chosenItem.getItemValue("costpp"));
        confirm_reservation_dialog_src.setText(SearchResultMap.chosenItem.getItemValue("start"));
        confirm_reservation_dialog_dst.setText(SearchResultMap.chosenItem.getItemValue("destination"));
        setupDateAndTime();
        setupNumSpinner();
        String comment=(SearchResultMap.chosenItem.getItemValue("comment").length()!=0)?"車主的話：“"+SearchResultMap.chosenItem.getItemValue("comment")+"”":"(車主沒留言)";
        confirm_reservation_dialog_driver_comment.setText(comment);
        confirm_reservation_dialog_driver_comment.setTextColor(ContextCompat.getColor(getContext(), R.color.grey));
        double_confirm_reservation_btn.setOnClickListener(reservation_btn_listener);


        getDialog().setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                getTargetFragment().onActivityResult(-1,-1, null);
            }
        });
    }

    View.OnClickListener reservation_btn_listener=new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            if(!firstClicked){
                firstClicked=true;
                double_confirm_reservation_btn.setText("再次點擊確定");
                return;
            }

            Intent joincarpoolData = new Intent();
            joincarpoolData.putExtra("joincarpoolPersonCount", Integer.parseInt(seatNumSpinner.getSelectedItem().toString()));

            getTargetFragment().onActivityResult(getTargetRequestCode(),getTargetRequestCode(),joincarpoolData);
            getDialog().dismiss();

        }
    };

    public void setupDateAndTime(){

        //string processing to extract ymd hms
        String[] dateTime=SearchResultMap.chosenItem.getItemValue("timeDepart").split(" ");
        String[] ymd=dateTime[0].split("-");
        String[] hms=dateTime[1].split(":");

        //find day of week
        // Note that Month value is 0-based. e.g., 0 for January.
        Calendar calendar = new GregorianCalendar(Integer.parseInt(ymd[0]),Integer.parseInt(ymd[1])-1,Integer.parseInt(ymd[2]));
        int day = calendar.get(Calendar.DAY_OF_WEEK);

        String dow="";
        switch (day) {
            case Calendar.MONDAY:{dow="一";break;}
            case Calendar.TUESDAY:{dow="二";break;}
            case Calendar.WEDNESDAY:{dow="三";break;}
            case Calendar.THURSDAY:{dow="四";break;}
            case Calendar.FRIDAY:{dow="五";break;}
            case Calendar.SATURDAY:{dow="六";break;}
            case Calendar.SUNDAY:{dow="日";break;}
        }

        //removing 0 prefix for month,day,hour,minute
        if(Integer.parseInt(ymd[1].charAt(0)+"")==0)
            ymd[1]=ymd[1].charAt(1)+"";

        if(Integer.parseInt(ymd[2].charAt(0)+"")==0)
            ymd[2]=ymd[2].charAt(1)+"";

        if(Integer.parseInt(hms[0].charAt(0) + "")==0)
            hms[0]=hms[0].charAt(1)+"";

        //detect AM or PM
        String AMPM=(Integer.parseInt(hms[0])>=12)?"PM":"AM";

        //make hour 13 to 1
        if(Integer.parseInt(hms[0])>=13){
            hms[0]=" "+(Integer.parseInt(hms[0])-12)+"";
        }

        //set formatted date output to ui
        confirm_reservation_dialog_date.setText(ymd[1] + "月" + ymd[2] + "日(" + dow + ") ");
        confirm_reservation_dialog_time.setText(formattedDatetime());

    }

    public String formattedDatetime(){

        String formattedDatetime="";

        //string processing to extract ymd hms
        String[] dateTime=SearchResultMap.chosenItem.getItemValue("timeDepart").split(" ");
        String[] ymd=dateTime[0].split("-");
        String[] hms=dateTime[1].split(":");

        //find day of week
        // Note that Month value is 0-based. e.g., 0 for January.
        Calendar calendar = new GregorianCalendar(Integer.parseInt(ymd[0]),Integer.parseInt(ymd[1])-1,Integer.parseInt(ymd[2]));
        int day = calendar.get(Calendar.DAY_OF_WEEK);

        String dow="";
        switch (day) {
            case Calendar.MONDAY:{dow="一";break;}
            case Calendar.TUESDAY:{dow="二";break;}
            case Calendar.WEDNESDAY:{dow="三";break;}
            case Calendar.THURSDAY:{dow="四";break;}
            case Calendar.FRIDAY:{dow="五";break;}
            case Calendar.SATURDAY:{dow="六";break;}
            case Calendar.SUNDAY:{dow="日";break;}
        }

        //removing 0 prefix for month,day,hour,minute
        if(Integer.parseInt(ymd[1].charAt(0)+"")==0)
            ymd[1]=ymd[1].charAt(1)+"";

        if(Integer.parseInt(ymd[2].charAt(0)+"")==0)
            ymd[2]=ymd[2].charAt(1)+"";

        if(Integer.parseInt(hms[0].charAt(0) + "")==0)
            hms[0]=hms[0].charAt(1)+"";

        //detect AM or PM
        String AMPM=(Integer.parseInt(hms[0])>=12)?"PM":"AM";

        //make hour 13 to 1
        if(Integer.parseInt(hms[0])>=13){
            hms[0]=" "+(Integer.parseInt(hms[0])-12)+"";
        }

        //TODO:day difference with the carpool date

        String dateInterval2="";
        if(Integer.parseInt(SearchResultMap.chosenItem.getItemValue("hasInterval"))==1){

            String[] dateTime2=SearchResultMap.chosenItem.getItemValue("timeDepart2").split(" ");
            String[] ymd2=dateTime2[0].split("-");
            String[] hms2=dateTime2[1].split(":");

            if(Integer.parseInt(hms2[0].charAt(0) + "")==0)
                hms2[0]=hms2[0].charAt(1)+"";

            //detect AM or PM
            String AMPM2=(Integer.parseInt(hms2[0])>=12)?"PM":"AM";

            //make hour 13 to 1
            if(Integer.parseInt(hms2[0])>=13){
                hms2[0]=" "+(Integer.parseInt(hms2[0])-12)+"";
            }

            dateInterval2=" ~ "+hms2[0]+":"+hms2[1]+" "+AMPM2;

        }

        String isTimeNegoable=(Integer.parseInt(SearchResultMap.chosenItem.getItemValue("isAnytime"))==1)?"(任意時間)":(Integer.parseInt(SearchResultMap.chosenItem.getItemValue("timeNego"))==1)?"(可議)":"";
        formattedDatetime=" "+hms[0]+":"+hms[1]+" "+AMPM+dateInterval2+" "+isTimeNegoable;

        //2月18日(六) 9:30 AM
        return formattedDatetime;
    }

    public void setupNumSpinner() {

        seatNumSpinner = (Spinner) getView().findViewById(R.id.seat_number_spinner);
        List<String> list = new ArrayList<String>();
        //here get the number of available seats

        //get user chosen carpool item, = available seats=seat given - seat taken
        int availableSeats=Integer.parseInt(SearchResultMap.chosenItem.getItemValue("seatGiven").toString())-Integer.parseInt(SearchResultMap.chosenItem.getItemValue("seatTaken").toString());

        for(int k=1;k<=availableSeats;k++){
            list.add(k+"");
        }

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        seatNumSpinner.setAdapter(dataAdapter);

        seatNumSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int costPerPassenger=Integer.parseInt(SearchResultMap.chosenItem.getItemValue("costpp"));
                int passengerNum=Integer.parseInt(seatNumSpinner.getSelectedItem().toString());
                int priceChange=costPerPassenger*passengerNum;
                confirm_reservation_dialog_total_price.setText(priceChange+"");
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {  }
        });
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onPause(){
        super.onPause();
        dismiss();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
