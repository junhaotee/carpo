package com.example.junhao.carpo;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

public class CheckUpcomingPassengerDialog extends DialogFragment {

    boolean                     firstClicked        = false;
    boolean                     isLoading           = false;
    boolean                     isItemMax           = false;
    int                         currentItemOffset   = 0;

    ImageView                   check_passenger_dialog_profile_pic;
    ListView                    check_passenger_dialog_rating_listview;
    UserRatingItemAdapter       ratingLvAdapter;
    ArrayList<UserRatingItem>   ratingCommentList;
    View                        footerView;
    Button                      check_passenger_dialog_cancel_reservation;

    TextView                    check_passenger_dialog_profile_verified,
                                check_passenger_dialog_profile_driver_rate,
                                check_passenger_dialog_name,
                                check_passenger_dialog_driver_hometown,
                                check_passenger_dialog_profile_contact,
                                check_passenger_dialog_reply_rate,
                                check_passenger_dialog_reply_within_hour,
                                check_passenger_dialog_rating_count,
                                check_passenger_dialog_bio,
                                listview_footer_tv_stat;

    public CheckUpcomingPassengerDialog() { }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_check_upcoming_passenger_dialog, container, false);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);

        getDialog().setTitle("聯絡乘客");

        setupViews();

        setupViewsData();

        getDialog().setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                getTargetFragment().onActivityResult(-1, -1, null);
            }
        });

    }

    void setupViews(){

        check_passenger_dialog_profile_pic          =(ImageView)getView().findViewById(R.id.check_passenger_dialog_profile_pic);
        check_passenger_dialog_profile_verified     =(TextView)getView().findViewById(R.id.check_passenger_dialog_profile_verified);
        check_passenger_dialog_profile_driver_rate  =(TextView)getView().findViewById(R.id.check_passenger_dialog_profile_passenger_rate);
        check_passenger_dialog_name                 =(TextView)getView().findViewById(R.id.check_passenger_dialog_passenger_name);
        check_passenger_dialog_driver_hometown      =(TextView)getView().findViewById(R.id.check_passenger_dialog_passenger_hometown);
        check_passenger_dialog_profile_contact      =(TextView)getView().findViewById(R.id.check_passenger_dialog_passenger_contact);
        check_passenger_dialog_reply_rate           =(TextView)getView().findViewById(R.id.check_passenger_dialog_reply_rate);
        check_passenger_dialog_reply_within_hour    =(TextView)getView().findViewById(R.id.check_passenger_dialog_reply_within_hour);
        check_passenger_dialog_rating_count         =(TextView)getView().findViewById(R.id.check_passenger_dialog_rating_count);
        check_passenger_dialog_bio                  =(TextView)getView().findViewById(R.id.check_passenger_dialog_passenger_bio);
        check_passenger_dialog_rating_listview      =(ListView)getView().findViewById(R.id.check_passenger_dialog_rating_listview);
        footerView                                  = ((LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.listview_footer, null, false);
        listview_footer_tv_stat                     = (TextView)footerView.findViewById(R.id.listview_footer_tv_stat);

        check_passenger_dialog_cancel_reservation   =(Button)getView().findViewById(R.id.check_passenger_dialog_cancel_reservation);

    }

    void setupViewsData(){

        new GetProfilePic().execute(CheckUpcomingPassengerTab.chosenCheckPassengerItem.getItemValue("fk_user_fb"), check_passenger_dialog_profile_pic);

        int rating_count=Integer.parseInt(CheckUpcomingPassengerTab.chosenCheckPassengerItem.getItemValue("p_rating_good_count"))+Integer.parseInt(CheckUpcomingPassengerTab.chosenCheckPassengerItem.getItemValue("p_rating_average_count"))+Integer.parseInt(CheckUpcomingPassengerTab.chosenCheckPassengerItem.getItemValue("p_rating_bad_count"));
        String name= CheckUpcomingPassengerTab.chosenCheckPassengerItem.getItemValue("fName")+ CheckUpcomingPassengerTab.chosenCheckPassengerItem.getItemValue("lName");
        String showLine=(Integer.parseInt(CheckUpcomingPassengerTab.chosenCheckPassengerItem.getItemValue("showLine"))==1)?"賴:"+ CheckUpcomingPassengerTab.chosenCheckPassengerItem.getItemValue("line"):"";
        String verified=(Integer.parseInt(CheckUpcomingPassengerTab.chosenCheckPassengerItem.getItemValue("verified"))==1)?"✓ 手機認證":"手機未認證";
        String bio=(CheckUpcomingPassengerTab.chosenCheckPassengerItem.getItemValue("bio").length()!=0)? CheckUpcomingPassengerTab.chosenCheckPassengerItem.getItemValue("bio"):"(乘客太酷了，沒填自我簡介)";
        String driver_hometown= CheckUpcomingPassengerTab.chosenCheckPassengerItem.getItemValue("hometown_name");
        String desc_rating_count=(rating_count==0)?"乘客剛加入共乘":rating_count+"個來自乘客的評價";

        check_passenger_dialog_name.setText(name);
        check_passenger_dialog_profile_contact.setText(showLine);
        check_passenger_dialog_bio.setText(bio);
        check_passenger_dialog_driver_hometown.setText(driver_hometown);
        check_passenger_dialog_rating_count.setText(desc_rating_count);
        check_passenger_dialog_profile_verified.setText(verified);
        if((Integer.parseInt(CheckUpcomingPassengerTab.chosenCheckPassengerItem.getItemValue("verified"))!=1))
            check_passenger_dialog_profile_verified.setTextColor(ContextCompat.getColor(getContext(), R.color.grey));

       /*
            TODO:need to develope a scheme for driver rating
                check_passenger_dialog_profile_driver_rate
                check_passenger_dialog_reply_rate
                check_passenger_dialog_reply_within_hour
        */

        //if rating comment is zero hide and section
        if(rating_count==0){

            LinearLayout check_passenger_dialog_rating_listview_section=(LinearLayout)getView().findViewById(R.id.check_passenger_dialog_rating_listview_section);
            check_passenger_dialog_rating_listview_section.setVisibility(View.INVISIBLE);

        }else{//if there were any comments on the driver, set it

            ratingCommentList                       =new ArrayList();
            ratingLvAdapter                         =new UserRatingItemAdapter(getContext(),ratingCommentList);
            check_passenger_dialog_rating_listview.setAdapter(ratingLvAdapter);
            check_passenger_dialog_rating_listview.setOnScrollListener(ratingItemLvListener);
            check_passenger_dialog_rating_listview.addFooterView(footerView);
        }

        check_passenger_dialog_cancel_reservation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!firstClicked){
                    firstClicked=true;
                    check_passenger_dialog_cancel_reservation.setText("再次確定");
                    return;
                }

                getTargetFragment().onActivityResult(getTargetRequestCode(),getTargetRequestCode(),null);
                getDialog().dismiss();
            }
        });
    }

    AbsListView.OnScrollListener ratingItemLvListener=new AbsListView.OnScrollListener() {

        @Override
        public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

            int lastInScreen = firstVisibleItem + visibleItemCount;
            if ((lastInScreen == totalItemCount && !isLoading &&!isItemMax)) {
                isLoading = true;
                new GetRatingResults().execute(CheckUpcomingPassengerTab.chosenCheckPassengerItem.getItemValue("fk_user_fb"),currentItemOffset,"p");
            }
        }

        @Override
        public void onScrollStateChanged(AbsListView absListView, int i) { }
    };


    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onPause(){
        super.onPause();
        dismiss();
    }

    class GetProfilePic extends AsyncTask<Object,Void,Bitmap> {

        ImageView fb_profile_pic;
        String fbId;

        @Override
        protected Bitmap doInBackground(Object... params) {
            Bitmap profilePic=null;
            fb_profile_pic=(ImageView)params[1];
            fbId=(String)params[0];

            try {

                URL profileURL=new URL("https://graph.facebook.com/"+fbId+"/picture?type=square");
                Log.d("pp", "https://graph.facebook.com/" + fbId + "/picture?type=square");
                URLConnection urlConn=profileURL.openConnection();
                urlConn.setReadTimeout(1500);
                urlConn.setConnectTimeout(1500);
                profilePic= BitmapFactory.decodeStream((InputStream) urlConn.getContent());

            }
            catch (MalformedURLException e) { e.printStackTrace(); }
            catch (IOException e) { e.printStackTrace(); }

            return profilePic;
        }

        @Override
        protected void onPostExecute(Bitmap profilePic){
            if(profilePic==null) return;

            fb_profile_pic.setImageBitmap(profilePic);

        }
    }

    class GetRatingResults extends AsyncTask<Object,Void,Integer> {

        String resultItemList="";

        @Override
        protected Integer doInBackground(Object... params) {

            try {
                String  fk_user_fb_rated    = params[0]+"";
                String  role                = params[2]+"";
                int     currentItemOffset   = (int)params[1];

                String queryString      = "http://140.116.83.83/api/get_user_rating.php?fk_user_fb_rated="+fk_user_fb_rated+"&currentItemOffset="+currentItemOffset+"&role="+role;
                Log.d("queryString",queryString);
                URL url                 = new URL(queryString);
                URLConnection urlConn   = url.openConnection();

                urlConn.setReadTimeout(1000);
                urlConn.setConnectTimeout(1000);

                resultItemList=convertStreamToString(urlConn.getInputStream());

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                resultItemList=""; //if server connect/read timeout, return empty list
                e.printStackTrace();
            }
            return null;
        }

        //onPostExecute is UI thread, updat the views here
        @Override
        protected void onPostExecute(Integer result) {
            try{
                //if empty string returned from server(when server gone wrong internally)
                if(resultItemList.length()==0){
                    check_passenger_dialog_rating_listview.setVisibility(View.GONE);
                    isItemMax=true;
                    return;
                }

                //parsed server response into json
                JSONArray userRatingArr=new JSONArray(resultItemList);

                //if no result is found
                if(userRatingArr.length()==0){
                    listview_footer_tv_stat.setText("");
                    isItemMax=true;
                }

                //if there were results, populates found result into arraylist
                for(int k=0;k<userRatingArr.length();k++){
                    JSONObject ratingObj = new JSONObject(userRatingArr.get(k).toString());
                    ratingCommentList.add(new UserRatingItem(ratingObj));
                }

                //notify listview data changed, and unblock dialog
                ratingLvAdapter.notifyDataSetChanged();
                currentItemOffset  += userRatingArr.length();
                isLoading           = false;

            }catch(JSONException jsone){ jsone.printStackTrace(); }
        }

        //method to convert input stream to string, used after reading input stream
        String convertStreamToString(java.io.InputStream is) {
            java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
            return s.hasNext() ? s.next() : "";
        }
    }


}
