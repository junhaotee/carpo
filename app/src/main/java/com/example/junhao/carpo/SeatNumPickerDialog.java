package com.example.junhao.carpo;


import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.NumberPicker;


public class SeatNumPickerDialog extends DialogFragment {

    NumberPicker seat_num_picker_dialog_picker;
    Button timepicker_dialog_saveseatnum_btn;

    public SeatNumPickerDialog() { }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onPause(){
        super.onPause();
        dismiss();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_seat_num_picker_dialog, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);

        getDialog().setTitle("提供座位給乘客");

        seat_num_picker_dialog_picker=(NumberPicker)getView().findViewById(R.id.seat_num_picker_dialog_picker);
        seat_num_picker_dialog_picker.setMinValue(1);
        seat_num_picker_dialog_picker.setMaxValue(24);

        timepicker_dialog_saveseatnum_btn=(Button)getView().findViewById(R.id.timepicker_dialog_saveseatnum_btn);

        timepicker_dialog_saveseatnum_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent seatData=new Intent();
                seatData.putExtra("seat_num",seat_num_picker_dialog_picker.getValue()+"");
                getTargetFragment().onActivityResult(getTargetRequestCode(),getTargetRequestCode(),seatData);
                getDialog().dismiss();
            }
        });

        getDialog().setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                getTargetFragment().onActivityResult(-1, -1, null);
            }
        });

    }


}
