package com.example.junhao.carpo;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

public class CheckCarpoolHistoryTab extends Fragment {

    boolean                                 loading=false;
    boolean                                 itemMax=false;
    int                                     currentItemOffset =0;
    static CheckCarpoolHistoryItem          chosenCarpoolHistoryItem;

    View                                    footerView;
    Fragment                                currentFragment=this;
    ListView                                check_carpool_history_listview;
    CheckCarpoolHistoryItemAdapter          carpoolHistoryItemAdapter;
    ArrayList<CheckCarpoolHistoryItem>      carpoolHistoryArrList;
    String                                  carpoolHistoryResults;

    TextView                                check_carpool_history_count,
            listview_footer_tv_stat;

    public CheckCarpoolHistoryTab() { }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);

        setupViews();
        setupViewsData();
    }

    void setupViews(){
        footerView                      = ((LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.listview_footer, null, false);
        listview_footer_tv_stat         = (TextView)footerView.findViewById(R.id.listview_footer_tv_stat);
        carpoolHistoryArrList           = new ArrayList();
        check_carpool_history_count    = (TextView)getView().findViewById(R.id.check_carpool_history_count);
        check_carpool_history_listview = (ListView)getView().findViewById(R.id.check_carpool_history_listview);
        carpoolHistoryItemAdapter       = new CheckCarpoolHistoryItemAdapter(getContext(), carpoolHistoryArrList);
    }

    void setupViewsData(){
        check_carpool_history_listview.addFooterView(footerView);
        check_carpool_history_listview.setAdapter(carpoolHistoryItemAdapter);
        check_carpool_history_listview.setOnScrollListener(carpoolHistoryLvListener);
        check_carpool_history_listview.setOnItemClickListener(adapterItemOnClick);
    }

    private AdapterView.OnItemClickListener adapterItemOnClick=new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            if(position>=carpoolHistoryArrList.size())return;
            chosenCarpoolHistoryItem=(carpoolHistoryArrList.get(position));

            /* Order of if-else-if is important, careful when reimplement */
            if(chosenCarpoolHistoryItem.getItemValue("fk_user_rating_id_on_driver").equals("0") && chosenCarpoolHistoryItem.getItemValue("request_status").equals("driver_approved")){
                //pop up dialog for rating
                RateDriverDialog rateDriverDialog=new RateDriverDialog();
                rateDriverDialog.setTargetFragment(currentFragment, 13);
                rateDriverDialog.show(getChildFragmentManager(), "");


            }else if(chosenCarpoolHistoryItem.getItemValue("request_status").equals("driver_canceled") || chosenCarpoolHistoryItem.getItemValue("request_status").equals("driver_approved") || chosenCarpoolHistoryItem.getItemValue("request_status").equals("driver_declined") || chosenCarpoolHistoryItem.getItemValue("request_status").equals("passenger_canceled") || chosenCarpoolHistoryItem.getItemValue("request_status").equals("passenger_canceled_after_approved")){

                //Setup bundle
                Bundle historyItemBundle=new Bundle();
                historyItemBundle.putString("profileObj", chosenCarpoolHistoryItem.getJSONObject().toString());

                //pop up passenger profile dialog
                DriverProfileDialog driverProfileDialog=new DriverProfileDialog();
                driverProfileDialog.setTargetFragment(currentFragment, 12);
                driverProfileDialog.setArguments(historyItemBundle);
                driverProfileDialog.show(getChildFragmentManager(), "");
            }

            return;

        }
    };

    AbsListView.OnScrollListener carpoolHistoryLvListener =new AbsListView.OnScrollListener() {

        @Override
        public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            int lastInScreen = firstVisibleItem + visibleItemCount;
            if ((lastInScreen == totalItemCount && !loading &&!itemMax)) {
                loading = true;
                new GetCarpoolHistory().execute();
            }
        }

        @Override
        public void onScrollStateChanged(AbsListView absListView, int i) { }
    };

    @Override
    public void onActivityResult(int requestCode,int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode){
            case -1:{MainActivity.dialog.hide();}break;
            case 12:{onDriverProfileDialogReturn(data, resultCode);}break;
            case 13:{onRatePassengerDialogReturn(data, resultCode);}break;

        }
    }

    void onDriverProfileDialogReturn(Intent data,int resultCode){ }

    void onRatePassengerDialogReturn(Intent data,int resultCode){
        //Driver either viewed passenger dialog, or already gave rating
        new RateDriver().execute(data);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_check_carpool_history_tab, container, false);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    class GetCarpoolHistory extends AsyncTask<Void,Void,Integer> {
        @Override
        protected Integer doInBackground(Void... params) {

            try {
                //defines query url, get current driver's carpool
                String queryString="http://140.116.83.83/api/get_carpool_history.php?fb="+MainActivity.fb_profile_id+"&currentItemOffset="+currentItemOffset;
                Log.d("queryString",queryString);
                URL url = new URL(queryString);
                URLConnection urlConn=url.openConnection();

                urlConn.setReadTimeout(1000);
                urlConn.setConnectTimeout(1000);

                carpoolHistoryResults =convertStreamToString(urlConn.getInputStream());

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                carpoolHistoryResults =""; //if server connect/read timeout, return empty list
                e.printStackTrace();
            }
            return null;
        }

        //onPostExecute is UI thread, update the views here
        @Override
        protected void onPostExecute(Integer result) {
            try{

                //if no result is found, add an empty item and quit
                if(carpoolHistoryResults.length()==0){
                    itemMax=true;
                    listview_footer_tv_stat.setText("");
                }

                //parsed server response into json
                JSONArray carpoolHistoryArr=new JSONArray(carpoolHistoryResults);

                //if no result is found, add an empty item and quit
                if(carpoolHistoryArr.length()==0){
                    itemMax=true;
                    listview_footer_tv_stat.setText("");
                    check_carpool_history_count.setText(0 + "");
                }

                //if there were results, populates found result into arraylist
                for(int k=0;k<carpoolHistoryArr.length();k++){
                    JSONObject carpoolHistoryObj = new JSONObject(carpoolHistoryArr.get(k).toString());
                    carpoolHistoryArrList.add(new CheckCarpoolHistoryItem(carpoolHistoryObj));
                }

                carpoolHistoryItemAdapter.notifyDataSetChanged();

                currentItemOffset +=carpoolHistoryArr.length();
                check_carpool_history_count.setText(currentItemOffset+"");
                loading=false;

            }catch(JSONException jsone){ jsone.printStackTrace(); }
        }

        //method to convert input stream to string, used after reading input stream
        String convertStreamToString(java.io.InputStream is) {
            java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
            return s.hasNext() ? s.next() : "";
        }
    }

    class RateDriver extends AsyncTask<Intent,Void,Integer> {

        String carpoolHistoryJSON;

        @Override
        protected Integer doInBackground(Intent... params) {

            try {
                //carpoolData.getExtras().getString("vehicle_type")
                String user_comment         = params[0].getExtras().getString("user_comment");
                String user_given_rating    = params[0].getExtras().getString("user_given_rating");
                String fk_user_fb_rates     = params[0].getExtras().getString("fk_user_fb_rates");
                String fk_user_fb_rated     = params[0].getExtras().getString("fk_user_fb_rated");
                String fb                   = params[0].getExtras().getString("fb");
                String fk_carpool_id        = params[0].getExtras().getString("fk_carpool_id");
                String passenger_request_id = params[0].getExtras().getString("passenger_request_id");

                String queryString="http://140.116.83.83/api/set_driver_rating.php?user_comment="+user_comment+"&user_given_rating="+user_given_rating+"&fk_user_fb_rates="+fk_user_fb_rates+"&fk_user_fb_rated="+fk_user_fb_rated+"&fb="+fb+"&fk_carpool_id="+fk_carpool_id+"&passenger_request_id="+passenger_request_id;
                Log.d("queryString", queryString);
                URL url = new URL(queryString);
                URLConnection urlConn=url.openConnection();

                urlConn.setReadTimeout(1000);
                urlConn.setConnectTimeout(1000);

                carpoolHistoryJSON =convertStreamToString(urlConn.getInputStream());

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                carpoolHistoryJSON ="";//if server connect/read timeout, return empty list
                e.printStackTrace();
            }
            return null;
        }

        //onPostExecute is UI thread, updat the views here
        @Override
        protected void onPostExecute(Integer result) {
            try{
                //clear previous results
                carpoolHistoryArrList.clear();
                carpoolHistoryItemAdapter.notifyDataSetChanged();

                //if no result is found, add an empty item and quit
                if(carpoolHistoryJSON.length()==0){
                    check_carpool_history_listview.setVisibility(View.GONE);
                    check_carpool_history_count.setText(0 + "");
                    return;
                }

                //parsed server response into json
                JSONArray historyArr=new JSONArray(carpoolHistoryJSON);

                //if no result is found, add an empty item and quit
                if(historyArr.length()==0){
                    check_carpool_history_count.setText(0+"");
                    return;
                }

                //if there were results, populates found result into arraylist
                for(int k=0;k<historyArr.length();k++){
                    JSONObject historyObj = new JSONObject(historyArr.get(k).toString());
                    carpoolHistoryArrList.add(new CheckCarpoolHistoryItem(historyObj));
                }

                //notify listview data changed, and unblock dialog
                check_carpool_history_count.setText(historyArr.length() + "");
                currentItemOffset=historyArr.length();
                carpoolHistoryItemAdapter.notifyDataSetChanged();



            }catch(JSONException jsone){ jsone.printStackTrace(); }
        }

        //method to convert input stream to string, used after reading input stream
        String convertStreamToString(java.io.InputStream is) {
            java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
            return s.hasNext() ? s.next() : "";
        }
    }
}