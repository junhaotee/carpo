package com.example.junhao.carpo;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by junhao on 2015/12/7.
 */
public class UserRatingItem {

    private JSONObject userRatingObject;

    //stores location result of one item
    public UserRatingItem(JSONObject locationResult){
        userRatingObject=locationResult;
    }

    //a convenient method for ResultListAdapter to retrieve json values
    public String getItemValue(String JSONKey){

        try {
            return userRatingObject.getString(JSONKey).toString();
        } catch (JSONException e) { e.printStackTrace();}
        return null;
    }

    public boolean isItemNull(){
        return (userRatingObject==null)?true:false;
    }

    public JSONObject getJSONObject(){
        return userRatingObject;
    }

}
