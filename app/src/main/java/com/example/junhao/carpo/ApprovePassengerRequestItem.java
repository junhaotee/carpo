
package com.example.junhao.carpo;

import org.json.JSONException;
import org.json.JSONObject;

public class ApprovePassengerRequestItem {

    private JSONObject passengerJSONObject;

    //stores location result of one item
    public ApprovePassengerRequestItem(JSONObject passengerResult){
        passengerJSONObject =passengerResult;
    }

    //a convenient method for ResultListAdapter to retrieve json values
    public String getItemValue(String JSONKey){
        try {
            return passengerJSONObject.getString(JSONKey).toString();
        }
        catch (JSONException e) { e.printStackTrace();}

        return null;
    }

    public boolean isItemNull(){
        return (passengerJSONObject ==null)?true:false;
    }

    public JSONObject getJSONObject(){
        return passengerJSONObject;
    }

}
