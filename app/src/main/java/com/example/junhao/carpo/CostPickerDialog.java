package com.example.junhao.carpo;


import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.Switch;


public class CostPickerDialog extends DialogFragment{

    NumberPicker costpicker_dialog_digit[]=new NumberPicker[4];
    Button timepicker_dialog_savecost_btn;
    Switch cost_picker_dialog_price_negoable;

    public CostPickerDialog() { }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onPause(){
        super.onPause();
        dismiss();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_cost_picker_dialog, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);

        getDialog().setTitle("設定每個座位的車資");

        cost_picker_dialog_price_negoable=(Switch)getView().findViewById(R.id.cost_picker_dialog_price_negoable);

        costpicker_dialog_digit[0]=(NumberPicker)getView().findViewById(R.id.costpicker_dialog_digit_0);
        costpicker_dialog_digit[1]=(NumberPicker)getView().findViewById(R.id.costpicker_dialog_digit_1);
        costpicker_dialog_digit[2]=(NumberPicker)getView().findViewById(R.id.costpicker_dialog_digit_2);
        costpicker_dialog_digit[3]=(NumberPicker)getView().findViewById(R.id.costpicker_dialog_digit_3);

        for(int k=0;k<4;k++){
            costpicker_dialog_digit[k].setMinValue(0);
            costpicker_dialog_digit[k].setMaxValue(9);
        }

        timepicker_dialog_savecost_btn=(Button)getView().findViewById(R.id.timepicker_dialog_savecost_btn);
        timepicker_dialog_savecost_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent seatData=new Intent();

                String d1=costpicker_dialog_digit[0].getValue()+"";
                String d2=costpicker_dialog_digit[1].getValue()+"";
                String d3=costpicker_dialog_digit[2].getValue()+"";
                String d4=costpicker_dialog_digit[3].getValue()+"";
                String priceString=d1+d2+d3+d4;
                seatData.putExtra("costpp",priceString);
                seatData.putExtra("isCostNego",cost_picker_dialog_price_negoable.isChecked());
                getTargetFragment().onActivityResult(getTargetRequestCode(),getTargetRequestCode(),seatData);


                getDialog().dismiss();
            }
        });

        getDialog().setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                getTargetFragment().onActivityResult(-1, -1, null);
            }
        });


    }


}
