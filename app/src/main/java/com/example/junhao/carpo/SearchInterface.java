package com.example.junhao.carpo;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceLikelihoodBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.R.attr.id;
import static com.example.junhao.carpo.R.id.search_interface_iv_dst;

public class SearchInterface extends Fragment {

    static Intent searchData=new Intent();

    ViewGroup       root                = null;
    Fragment        currentFragment     = this;
    GoogleMap       mGoogleMap;
    GoogleApiClient mGoogleApiClient;
    ProgressDialog  progressDialog;

    Button          searchBtn,
                    src_btn,
                    dst_btn,
                    src_dst_btn;

    EditText        searchSrc,
                    searchDst,
                    searchDate;

    ImageView       search_interface_iv_src,
                    search_interface_iv_dst,
                    search_interface_iv_datetime;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {

        root = (ViewGroup) inflater.inflate(R.layout.activity_search_interface, null);
/*
        //datepicker related initialization
        datePickerInit(root);

        //find view and attach listener
        searchBtnInit(root);

        //set onclick listener for both src/dst edittext
        searchSrcDstInitBtn(root);
*/

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                getAllViews(root);
                setupAllListeners();
                getCurrentPlaceInfo(root);
            }
        });

        //google map setup
        setupGoogleMap();

        //GPS service initialization, if coordinates not set, get the default one where user is at
        //getCurrentPlaceInfo(root);

        //zoom in zoom out btn setup
        //setupZoomBtn(root);

        return root;

    }



    void setupZoomBtn(ViewGroup root){



    }

    //listener definition
    View.OnClickListener src_dst_btn_listener=new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            if(!(searchData.hasExtra("src_lat") || searchData.hasExtra("dst_lat"))) {
                Toast.makeText(getContext(),"請先定位迄點",Toast.LENGTH_SHORT).show();
                return;
            }

            if(!searchData.hasExtra("dst_lat")) {
                Toast.makeText(getContext(),"請先定位迄點",Toast.LENGTH_SHORT).show();
                return;
            }

            //if have both lat and lng
            if(searchData.hasExtra("src_lat") && searchData.hasExtra("dst_lat")){

                //clear google map previous markers
                mGoogleMap.clear();

                //create src marker and dst marker set them on map
                Marker[] markerList=new Marker[2];

                //setups for latlng variables
                double srcLat=searchData.getExtras().getDouble("src_lat");
                double srcLng=searchData.getExtras().getDouble("src_lng");
                double dstLat=searchData.getExtras().getDouble("dst_lat");
                double dstLng=searchData.getExtras().getDouble("dst_lng");

                //marker for dst
                LatLng dstLocation = new LatLng(dstLat,dstLng);
                markerList[0]=mGoogleMap.addMarker(new MarkerOptions().position(dstLocation).draggable(true));

                //marker for src
                LatLng srcLocation = new LatLng(srcLat,srcLng);
                markerList[1]=mGoogleMap.addMarker(new MarkerOptions().position(srcLocation).draggable(true));

                //create boundary for google map, so camera knows where to focus
                LatLngBounds.Builder builder = new LatLngBounds.Builder();
                for (Marker marker : markerList) {
                    builder.include(marker.getPosition());
                }
                LatLngBounds bounds = builder.build();

                //int padding = getActivity().getResources().getDimensionPixelSize(R.dimen.home_map_padding);
                // 100 is the offset of markers from edges of the map in pixels
                //greater the number camera zoom is farther
                CameraUpdate cameraUpdateTwoMarkers = CameraUpdateFactory.newLatLngBounds(bounds, 100);
                mGoogleMap.animateCamera(cameraUpdateTwoMarkers);

                return;

            }
        }
    };

    //listeners definition
    View.OnClickListener src_btn_listener=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(searchData.hasExtra("src_lat")){
                double dstLat=searchData.getExtras().getDouble("src_lat");
                double dstLng=searchData.getExtras().getDouble("src_lng");
                LatLng defaultLocation = new LatLng(dstLat,dstLng);
                mGoogleMap.addMarker(new MarkerOptions().position(defaultLocation).draggable(true));
                CameraUpdate locationCamera = CameraUpdateFactory.newLatLngZoom(defaultLocation, 17.0f);
                mGoogleMap.animateCamera(locationCamera);
            }
        }
    };

    //listeners definition
    View.OnClickListener dst_btn_listener=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(searchData.hasExtra("dst_lat")){
                double dstLat=searchData.getExtras().getDouble("dst_lat");
                double dstLng=searchData.getExtras().getDouble("dst_lng");
                LatLng defaultLocation = new LatLng(dstLat,dstLng);
                mGoogleMap.addMarker(new MarkerOptions().position(defaultLocation).draggable(true));
                CameraUpdate locationCamera = CameraUpdateFactory.newLatLngZoom(defaultLocation, 17.0f);
                mGoogleMap.animateCamera(locationCamera);
            }else{
                Toast.makeText(getContext(),"請先定位迄點",Toast.LENGTH_SHORT).show();
                return;
            }
        }
    };

    @Override// set listeners and action on views in onActivityCreated, after all views inflated in onCreateViews
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onResume(){
        super.onResume();
    }


    public void setupGoogleMap(){
        //pass supportmapfragment's google map to mGoogleMap variable
        if(mGoogleMap!=null)return;
        SupportMapFragment mapFrag = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        if(mapFrag==null)return;
        mapFrag.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                //Log.d("","\n\n\nsuccessfully implemented");
                mGoogleMap = googleMap;
                mGoogleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                    @Override
                    public void onMapLoaded() {
                        setupCoordinatesIfAvailable();
                    }
                });
            }
        });
    }

    //called everytime user enters the interface, adjust the map zoom for markers and format the src/dst name accordingly
    public void setupCoordinatesIfAvailable(){

        //clear previous markers
        if(mGoogleMap!=null)
            mGoogleMap.clear();

        //if searchData doesn't contain src and dst latlng at the same time, return immediately do nothing on the map
        if(!(searchData.hasExtra("src_lat") || searchData.hasExtra("dst_lat"))) return;

        //if have both src and dst stored in intent before, put two markers and adjust zoom for them
        if(searchData.hasExtra("src_lat") && searchData.hasExtra("dst_lat")){

            //clear google map previous markers
            mGoogleMap.clear();

            //create src marker and dst marker set them on map
            Marker[] markerList=new Marker[2];

            //setups for latlng variables
            double srcLat=searchData.getExtras().getDouble("src_lat");
            double srcLng=searchData.getExtras().getDouble("src_lng");
            double dstLat=searchData.getExtras().getDouble("dst_lat");
            double dstLng=searchData.getExtras().getDouble("dst_lng");

            //marker for dst
            LatLng dstLocation = new LatLng(dstLat,dstLng);
            markerList[0]=mGoogleMap.addMarker(new MarkerOptions().position(dstLocation).draggable(true));

            //marker for src
            LatLng srcLocation = new LatLng(srcLat,srcLng);
            markerList[1]=mGoogleMap.addMarker(new MarkerOptions().position(srcLocation).draggable(true));

            //create boundary for google map, so camera knows where to focus
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            for (Marker marker : markerList) {
                builder.include(marker.getPosition());
            }
            LatLngBounds bounds = builder.build();

            //int padding = getActivity().getResources().getDimensionPixelSize(R.dimen.home_map_padding);
            //100 is the offset of markers from edges of the map in pixels
            //greater the number camera zoom is farther
            //create a camera update for boundry and pass it to animatecamera to adjust zoom
            CameraUpdate cameraUpdateTwoMarkers = CameraUpdateFactory.newLatLngBounds(bounds, 100);
            mGoogleMap.moveCamera(cameraUpdateTwoMarkers);

            //set src and dstt edittext for location name, and call FormatLocationName afterwards to put postcode area as prefix
            searchSrc.setText(searchData.getExtras().getString("src_city_town")+searchData.getStringExtra("src_name") + "\n" + searchData.getStringExtra("src_address"));
            //new FormatLocationName().execute(true);

            searchDst.setText(searchData.getExtras().getString("dst_city_town")+searchData.getStringExtra("dst_name")+"\n"+searchData.getStringExtra("dst_address"));
            //new FormatLocationName().execute(false);

            return;

        }

        //move camera if only src lat is defined in searcData intent
        if(searchData.hasExtra("src_lat")){
            double srcLat=searchData.getExtras().getDouble("src_lat");
            double srcLng=searchData.getExtras().getDouble("src_lng");
            LatLng defaultLocation = new LatLng(srcLat,srcLng);
            mGoogleMap.addMarker(new MarkerOptions().position(defaultLocation).draggable(true));
            CameraUpdate locationCamera = CameraUpdateFactory.newLatLngZoom(defaultLocation, 17.0f);
            mGoogleMap.moveCamera(locationCamera);


            //String prefix=(searchData.hasExtra("src_city_town"))?searchData.getStringExtra("src_city_town"):"";
            //searchSrc.setText(prefix+searchData.getStringExtra("src_name"));
        }

        //if only dst lat is defined in searcData intent
        if(searchData.hasExtra("dst_lat")){
            double dstLat=Double.parseDouble(searchData.getExtras().getString("dst_lat"));
            double dstLng=Double.parseDouble(searchData.getExtras().getString("dst_lng"));
            LatLng defaultLocation = new LatLng(dstLat, dstLng);
            mGoogleMap.addMarker(new MarkerOptions().position(defaultLocation).draggable(true));
            CameraUpdate locationCamera = CameraUpdateFactory.newLatLngZoom(defaultLocation, 17.0f);
            mGoogleMap.moveCamera(locationCamera);

            //String prefix=(searchData.hasExtra("dst_city_town"))?searchData.getStringExtra("dst_city_town"):"";
            //searchDst.setText(prefix + searchData.getStringExtra("dst_name"));
        }
    }

    public void datePickerInit(ViewGroup root){



    }

    public void searchBtnInit(ViewGroup root){



    }

    public void searchSrcDstInitBtn(ViewGroup root){


    }

    public void showDialog(String msg){
        MainActivity.dialog.setMessage(msg);
        MainActivity.dialog.setCancelable(false);
        MainActivity.dialog.setInverseBackgroundForced(false);
        MainActivity.dialog.show();
    }

    //retrieve current location
    public void getCurrentPlaceInfo(ViewGroup root){

        //setup google api client, only used in this method
        mGoogleApiClient=new GoogleApiClient.Builder(getContext())
                .enableAutoManage(getActivity(), new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) { }
                })
                .addApi(Places.PLACE_DETECTION_API)
                .build();

        //do a permission check, if permission not granted, prompt user to select by his own
        if ( ContextCompat.checkSelfPermission( getContext(), Manifest.permission.ACCESS_COARSE_LOCATION ) != PackageManager.PERMISSION_GRANTED ) {
            //if permission not granted
            Toast.makeText(getContext(),"獲取當地資訊權限不足",Toast.LENGTH_SHORT).show();
        }

        PendingResult<PlaceLikelihoodBuffer> result= Places.PlaceDetectionApi.getCurrentPlace(mGoogleApiClient, null);

        result.setResultCallback(new ResultCallback<PlaceLikelihoodBuffer>() {
            @Override
            public void onResult(PlaceLikelihoodBuffer likelyPlaces) {

                //the first of results is the most possible place where user is at
                Place currentPlace = likelyPlaces.get(0).getPlace();

                searchData.putExtra("src_lat", currentPlace.getLatLng().latitude);
                searchData.putExtra("src_lng", currentPlace.getLatLng().longitude);
                searchData.putExtra("src_name", currentPlace.getName());
                searchData.putExtra("src_address", currentPlace.getAddress());
                searchSrc.setText(currentPlace.getName() + "\n" + currentPlace.getAddress());

                //get postcode
                Matcher m = Pattern.compile(".*([1-9][0-9][0-9]).*").matcher(currentPlace.getAddress());
                int postCode = (m.matches()) ? Integer.parseInt(m.group(1)) : 0;
                searchData.putExtra("src_postcode", postCode);

                if (postCode != 0) new FormatLocationName().execute(true);
                //release resources to prevent mem leak
                likelyPlaces.release();
                setupCoordinatesIfAvailable();
            }
        });
    }

    void getAllViews(ViewGroup root){

        searchDate=(EditText) root.findViewById(R.id.searchDataEdittext);
        //get view and set action, change fragment
        searchBtn=(Button)root.findViewById(R.id.searchBtn);

        searchSrc=(EditText)root.findViewById(R.id.searchSrc);
        searchDst=(EditText)root.findViewById(R.id.searchDst);

        //get views
        src_btn=(Button)root.findViewById(R.id.src_btn);
        dst_btn=(Button)root.findViewById(R.id.dst_btn);
        src_dst_btn=(Button)root.findViewById(R.id.src_dst_btn);

        search_interface_iv_src         = (ImageView)root.findViewById(R.id.search_interface_iv_src);
        search_interface_iv_dst         = (ImageView)root.findViewById(R.id.search_interface_iv_dst);
        search_interface_iv_datetime    = (ImageView)root.findViewById(R.id.search_interface_iv_datetime);

        progressDialog=new ProgressDialog(getContext());

    }

    void setupAllListeners(){
        View.OnClickListener searchDateListener=new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog datePickerDialog=new DatePickerDialog();
                datePickerDialog.setTargetFragment(currentFragment,0);
                datePickerDialog.show(getChildFragmentManager(), "");
            }
        };

        searchDate.setOnClickListener(searchDateListener);

        //define listener for saveBtn
        Button.OnClickListener searchBtnListener=new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().beginTransaction().replace(R.id.container, new SearchResultMap()).commit();
            }
        };

        //attach listener
        searchBtn.setOnClickListener(searchBtnListener);


        searchSrc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int PLACE_AUTOCOMPLETE_REQUEST_CODE = 5;
                try {
                    //Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY).build(getActivity());
                    Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY).build(getActivity());
                    startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);

                }
                catch (GooglePlayServicesRepairableException e) { }
                catch (GooglePlayServicesNotAvailableException e) { }
            }
        });

        search_interface_iv_src.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int PLACE_AUTOCOMPLETE_REQUEST_CODE = 5;
                try {
                    //Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY).build(getActivity());
                    Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY).build(getActivity());
                    startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);

                }
                catch (GooglePlayServicesRepairableException e) { }
                catch (GooglePlayServicesNotAvailableException e) { }
            }
        });

        searchDst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog.setMessage("正在打開Google定位器");
                progressDialog.show();
                int PLACE_AUTOCOMPLETE_REQUEST_CODE = 6;
                try {
                    Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY).build(getActivity());
                    startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
                    progressDialog.hide();
                }
                catch (GooglePlayServicesRepairableException e) { }
                catch (GooglePlayServicesNotAvailableException e) { }
            }
        });

        search_interface_iv_dst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog.setMessage("正在打開Google定位器");
                progressDialog.show();
                int PLACE_AUTOCOMPLETE_REQUEST_CODE = 6;
                try {
                    Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY).build(getActivity());
                    startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
                    progressDialog.hide();
                }
                catch (GooglePlayServicesRepairableException e) { }
                catch (GooglePlayServicesNotAvailableException e) { }
            }
        });

        search_interface_iv_datetime.setOnClickListener(searchDateListener);

        //attach listeners
        src_btn.setOnClickListener(src_btn_listener);
        dst_btn.setOnClickListener(dst_btn_listener);
        src_dst_btn.setOnClickListener(src_dst_btn_listener);
    }

    //handles the interaction with child fragment
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode){
            case -1:{MainActivity.dialog.hide();}break;
            case 0:{onDatePickerReturned(data);}break;
            case 5:{onSrcLocationReturn(data,resultCode);}break;
            case 6:{onDstLocationReturn(data,resultCode);}break;
        }
    }

    public void onDatePickerReturned(Intent data){
        //mainly formatting the date view text output
        searchData.putExtras(data);

        Calendar calendar = new GregorianCalendar(data.getExtras().getInt("y"),data.getExtras().getInt("m"),data.getExtras().getInt("d"));
        int day = calendar.get(Calendar.DAY_OF_WEEK);

        String dow="";
        switch (day) {
            case Calendar.MONDAY:{dow="一";break;}
            case Calendar.TUESDAY:{dow="二";break;}
            case Calendar.WEDNESDAY:{dow="三";break;}
            case Calendar.THURSDAY:{dow="四";break;}
            case Calendar.FRIDAY:{dow="五";break;}
            case Calendar.SATURDAY:{dow="六";break;}
            case Calendar.SUNDAY:{dow="日";break;}
        }

        String formattedDate=data.getExtras().getInt("y")+"年 "+(data.getExtras().getInt("m")+1)+"月 "+data.getExtras().getInt("d")+"日 （星期"+dow+"）";

        searchDate.setText(formattedDate);
    }

    //not using locatinpicker anymore
    public void onSrcLocationReturn(Intent data,int resultCode){

        //do nothing if no results were found
        if(resultCode==0)return;

        Place place = PlaceAutocomplete.getPlace(getContext(), data);

        searchData.putExtra("src_lat", place.getLatLng().latitude);
        searchData.putExtra("src_lng", place.getLatLng().longitude);
        searchData.putExtra("src_name", place.getName());
        searchData.putExtra("src_address", place.getAddress());

        //get postcode
        Matcher m = Pattern.compile(".*?([1-9][0-9][0-9])?.*?").matcher(place.getAddress());
        int postCode=(m.matches())?Integer.parseInt(m.group(1)) : 0;
        searchData.putExtra("src_postcode",postCode);

        if(postCode!=0)new FormatLocationName().execute(true);

        setupCoordinatesIfAvailable();
    }

    //not using locatinpicker anymore
    public void onDstLocationReturn(Intent data,int resultCode){

        //do nothing if no results were found
        if(resultCode==0)return;

        Place place = PlaceAutocomplete.getPlace(getContext(), data);

        searchData.putExtra("dst_lat", place.getLatLng().latitude);
        searchData.putExtra("dst_lng",place.getLatLng().longitude);
        searchData.putExtra("dst_name", place.getName());
        searchData.putExtra("dst_address", place.getAddress());

        int postCode=0;
        try{
            Matcher m = Pattern.compile(".*?([1-9][0-9][0-9])?.*?").matcher(place.getAddress());
            postCode=(m.matches())?Integer.parseInt(m.group(1)):0;
        }catch(Exception e){
            Toast.makeText(getContext(),"位置不夠精準",Toast.LENGTH_LONG).show();
            searchData.removeExtra("dst_postcode");
            searchData.removeExtra("dst_lat");
            setupCoordinatesIfAvailable();
            searchDst.setText("");
            return; }

        if(postCode==0)return;

        searchData.putExtra("dst_postcode",postCode);
        searchDst.setText(place.getName() + "\n" + place.getAddress());
        if (postCode!=0)new FormatLocationName().execute(false);

        setupCoordinatesIfAvailable();
    }

    @Override
    public void onDestroyView() {
        mGoogleApiClient.stopAutoManage(getActivity());
        mGoogleApiClient.disconnect();
        super.onDestroyView();
    }


    class FormatLocationName extends AsyncTask<Boolean,Void,String>{

        boolean isSource;

        @Override
        protected String doInBackground(Boolean... isSource) {
            this.isSource=isSource[0];

            String postCode=(this.isSource)?searchData.getExtras().getInt("src_postcode")+"":searchData.getExtras().getInt("dst_postcode")+"";
            String locationName="";
            InputStream inputStream=null;

            try {
                //need to add 'http://' otherwise malform exception is thrown
                String queryString="http://140.116.83.83/api/get_location.php?postcode=" + URLEncoder.encode(postCode, "utf8");
                Log.d("queryString", queryString);

                //open server connection
                URL url = new URL(queryString);
                URLConnection urlConn=url.openConnection();

                //to detect server failure, must do timeout settings, otherwise thsi thread will keep waiting until timeout
                urlConn.setConnectTimeout(1000);
                urlConn.setReadTimeout(1000);

                //read server response
                inputStream=urlConn.getInputStream();
                String tempJSONOArrString=convertStreamToString(inputStream);
                inputStream.close();

                //parse response into json array, if response is 0 length or invalid jsonarr, return '[]' directly
                JSONArray locationNameJSONArr=new JSONArray(tempJSONOArrString);
                if(locationNameJSONArr==null || locationNameJSONArr.length()==0)return "";
                locationName="["+locationNameJSONArr.getJSONObject(0).getString(getResources().getString(R.string.db_city_name))+" "+locationNameJSONArr.getJSONObject(0).getString(getResources().getString(R.string.db_town_name))+"]";
            }
            catch(MalformedURLException mue) {
                mue.printStackTrace();
                return "";
            }
            catch(IOException ioe) {
                ioe.printStackTrace();
                try{
                    inputStream.close();
                }
                catch(NullPointerException npe){ npe.printStackTrace(); }
                catch (IOException e) { e.printStackTrace(); }
                return "";
            }
            catch(JSONException jsone) {
                jsone.printStackTrace();
                return "";
            }
            return locationName;
        }

        @Override
        protected void onPostExecute(String formattedPrefix){
            if (isSource){
                searchData.putExtra("src_city_town",formattedPrefix);
                searchSrc.setText(formattedPrefix + " " + searchData.getExtras().getString("src_name")+"\n"+searchData.getExtras().getString("src_address"));
            }else{
                searchData.putExtra("dst_city_town",formattedPrefix);
                searchDst.setText(formattedPrefix + " " + searchData.getExtras().getString("dst_name")+"\n"+searchData.getExtras().getString("dst_address"));
            }
        }

        String convertStreamToString(java.io.InputStream is) {
            java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
            return s.hasNext() ? s.next() : "";
        }
    }
}