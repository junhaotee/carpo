
package com.example.junhao.carpo;

import org.json.JSONException;
import org.json.JSONObject;

public class CheckPassengerHistoryItem {

    private JSONObject passengerHistoryJSONObject;

    //stores location result of one item
    public CheckPassengerHistoryItem(JSONObject passengerHistoryResult){
        passengerHistoryJSONObject =passengerHistoryResult;
    }

    //empty constructor is called when no results from server
    public CheckPassengerHistoryItem(){
        passengerHistoryJSONObject =null;
    }

    //a convenient method for ResultListAdapter to retrieve json values
    public String getItemValue(String JSONKey){
        try {
            return passengerHistoryJSONObject.getString(JSONKey).toString();
        }
        catch (JSONException e) { e.printStackTrace();}

        return null;
    }

    public boolean isItemNull(){
        if(passengerHistoryJSONObject ==null)
            return true;
        else
            return false;
    }

    public JSONObject getJSONObject(){
        return passengerHistoryJSONObject;
    }
}
