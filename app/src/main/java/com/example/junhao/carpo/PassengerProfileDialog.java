package com.example.junhao.carpo;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;


public class PassengerProfileDialog extends DialogFragment {

    boolean             loading=false;
    boolean             itemMax=false;
    int                 currentItemOffset =0;

    JSONObject          profileObj=null;
    ListView            passenger_profile_dialog_rating_listview;
    ArrayList           userRatingItemList;
    UserRatingItemAdapter userRatingItemAdapter;
    ImageView           passenger_profile_dialog_profile_pic;

    TextView            passenger_profile_dialog_profile_verified,
                        passenger_profile_dialog_profile_driver_rate,
                        passenger_profile_dialog_name,
                        passenger_profile_dialog_driver_hometown,
                        passenger_profile_dialog_profile_contact,
                        passenger_profile_dialog_reply_rate,
                        passenger_profile_dialog_reply_within_hour,
                        passenger_profile_dialog_rating_count,
                        listview_footer_tv_stat,
                        passenger_profile_dialog_bio;

    public PassengerProfileDialog() { }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_passenger_profile_dialog, container, false);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);

        getDialog().setTitle("聯絡乘客");

        setupViews();

        setupViewsData();

        getDialog().setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                getTargetFragment().onActivityResult(-1, -1, null);
            }
        });

    }

    void setupViews(){

        passenger_profile_dialog_profile_pic        = (ImageView)getView().findViewById(R.id.passenger_profile_dialog_profile_pic);
        passenger_profile_dialog_profile_verified   = (TextView)getView().findViewById(R.id.passenger_profile_dialog_profile_verified);
        passenger_profile_dialog_profile_driver_rate= (TextView)getView().findViewById(R.id.passenger_profile_dialog_profile_driver_rate);
        passenger_profile_dialog_name               = (TextView)getView().findViewById(R.id.passenger_profile_dialog_name);
        passenger_profile_dialog_driver_hometown    = (TextView)getView().findViewById(R.id.passenger_profile_dialog_driver_hometown);
        passenger_profile_dialog_profile_contact    = (TextView)getView().findViewById(R.id.passenger_profile_dialog_profile_contact);
        passenger_profile_dialog_reply_rate         = (TextView)getView().findViewById(R.id.passenger_profile_dialog_reply_rate);
        passenger_profile_dialog_reply_within_hour  = (TextView)getView().findViewById(R.id.passenger_profile_dialog_reply_within_hour);
        passenger_profile_dialog_rating_count       = (TextView)getView().findViewById(R.id.passenger_profile_dialog_rating_count);
        passenger_profile_dialog_bio                = (TextView)getView().findViewById(R.id.passenger_profile_dialog_bio);
        passenger_profile_dialog_rating_listview    = (ListView)getView().findViewById(R.id.passenger_profile_dialog_rating_listview);
        View footerView                             = ((LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.listview_footer, null, false);
        listview_footer_tv_stat                     = (TextView)footerView.findViewById(R.id.listview_footer_tv_stat);
        passenger_profile_dialog_rating_listview.addFooterView(footerView);

    }

    void setupViewsData(){

        try {

            profileObj=new JSONObject(this.getArguments().getString("profileObj"));

            new GetProfilePic().execute(profileObj.getString("fk_user_fb"), passenger_profile_dialog_profile_pic);

            String name = profileObj.getString("fName")+ profileObj.getString("lName");

            String showLine         =(Integer.parseInt(profileObj.getString("showLine"))==1)?"賴:"+ profileObj.getString("line"):"";
            String verified         =(Integer.parseInt(profileObj.getString("verified"))==1)?"✓ 手機認證":"手機未認證";
            String bio              =(profileObj.getString("bio").length()!=0)? profileObj.getString("bio"):"(乘客太酷了，沒填自我簡介)";
            String driver_hometown  = profileObj.getString("hometown_name");
            int rating_count        =Integer.parseInt(profileObj.getString("p_rating_good_count"))+Integer.parseInt(profileObj.getString("p_rating_average_count"))+Integer.parseInt(profileObj.getString("p_rating_bad_count"));
            String desc_rating_count=(rating_count==0)?"乘客剛加入共乘":rating_count+"個來自車主的評價";

            passenger_profile_dialog_name.setText(name);
            passenger_profile_dialog_profile_contact.setText(showLine);
            passenger_profile_dialog_profile_verified.setText(verified);
            passenger_profile_dialog_bio.setText(bio);
            passenger_profile_dialog_driver_hometown.setText(driver_hometown);
            passenger_profile_dialog_rating_count.setText(desc_rating_count);
            if((Integer.parseInt(profileObj.getString("verified"))!=1))
                passenger_profile_dialog_profile_verified.setTextColor(ContextCompat.getColor(getContext(), R.color.grey));

           /*
                TODO:need to develope a scheme for driver rating
                    passenger_profile_dialog_profile_driver_rate
                    passenger_profile_dialog_reply_rate
                    passenger_profile_dialog_reply_within_hour
            */

            //if rating comment is zero hide and section
            if(rating_count==0){

                LinearLayout passenger_profile_dialog_rating_listview_section=(LinearLayout)getView().findViewById(R.id.passenger_profile_dialog_rating_listview_section);
                passenger_profile_dialog_rating_listview_section.setVisibility(View.INVISIBLE);

            }else{//if there were any comments on the passenger, set it

                userRatingItemList=new ArrayList();
                userRatingItemAdapter=new UserRatingItemAdapter(getContext(),userRatingItemList);
                passenger_profile_dialog_rating_listview.setAdapter(userRatingItemAdapter);

                loading = true;
                new GetRatingResults().execute(profileObj.getString("fk_user_fb"), currentItemOffset , "p");

                passenger_profile_dialog_rating_listview.setOnScrollListener(ratingItemLvListener);
        }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    AbsListView.OnScrollListener ratingItemLvListener=new AbsListView.OnScrollListener() {

        @Override
        public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            int lastInScreen = firstVisibleItem + visibleItemCount;
            if ((lastInScreen == totalItemCount && !loading &&!itemMax)) {
                loading = true;
                try {
                    new GetRatingResults().execute(profileObj.getString("fk_user_fb"), currentItemOffset,"p");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onScrollStateChanged(AbsListView absListView, int i) { }
    };


    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onPause(){
        super.onPause();
        dismiss();
    }

    class GetProfilePic extends AsyncTask<Object,Void,Bitmap> {

        ImageView fb_profile_pic;
        String fbId;

        @Override
        protected Bitmap doInBackground(Object... params) {
            Bitmap profilePic=null;
            fb_profile_pic=(ImageView)params[1];
            fbId=(String)params[0];

            try {

                URL profileURL=new URL("https://graph.facebook.com/"+fbId+"/picture?type=square");
                Log.d("pp", "https://graph.facebook.com/" + fbId + "/picture?type=square");
                URLConnection urlConn=profileURL.openConnection();
                urlConn.setReadTimeout(1500);
                urlConn.setConnectTimeout(1500);
                profilePic= BitmapFactory.decodeStream((InputStream) urlConn.getContent());

            }
            catch (MalformedURLException e) { e.printStackTrace(); }
            catch (IOException e) { e.printStackTrace(); }

            return profilePic;
        }

        @Override
        protected void onPostExecute(Bitmap profilePic){
            if(profilePic==null) return;

            fb_profile_pic.setImageBitmap(profilePic);

        }
    }

    class GetRatingResults extends AsyncTask<Object,Void,Integer> {

        String resultItemList="";

        @Override
        protected Integer doInBackground(Object... params) {

            try {
                String fk_user_fb_rated =params[0]+"";
                int currentItemOffset   =(int)params[1];
                String role             =params[2]+"";

                String queryString      = "http://140.116.83.83/api/get_user_rating.php?fk_user_fb_rated="+fk_user_fb_rated+"&currentItemOffset="+currentItemOffset+"&role="+role;
                URL url                 = new URL(queryString);
                URLConnection urlConn   = url.openConnection();

                urlConn.setReadTimeout(1000);
                urlConn.setConnectTimeout(1000);

                resultItemList=convertStreamToString(urlConn.getInputStream());

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                resultItemList=""; //if server connect/read timeout, return empty list
                e.printStackTrace();
            }
            return null;
        }

        //onPostExecute is UI thread, updat the views here
        @Override
        protected void onPostExecute(Integer result) {
            try{
                //if empty string returned from server(when server gone wrong internally)
                if(resultItemList.length()==0){
                    passenger_profile_dialog_rating_listview.setVisibility(View.GONE);
                    return;
                }

                //parsed server response into json
                JSONArray userRatingArr=new JSONArray(resultItemList);

                //if no result is found
                if(userRatingArr.length()==0){
                    listview_footer_tv_stat.setText("");
                    itemMax=true;
                }

                //if there were results, populates found result into arraylist
                for(int k=0;k<userRatingArr.length();k++){
                    JSONObject ratingObj = new JSONObject(userRatingArr.get(k).toString());
                    userRatingItemList.add(new UserRatingItem(ratingObj));
                }

                //notify listview data changed, and unblock dialog
                userRatingItemAdapter.notifyDataSetChanged();
                currentItemOffset +=5;
                loading=false;

            }catch(JSONException jsone){ jsone.printStackTrace(); }
        }

        //method to convert input stream to string, used after reading input stream
        String convertStreamToString(java.io.InputStream is) {
            java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
            return s.hasNext() ? s.next() : "";
        }
    }
}
