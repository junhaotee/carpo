package com.example.junhao.carpo;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.ProfileTracker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class MainActivity extends AppCompatActivity implements NavigationDrawerFragment.NavigationDrawerCallbacks{

    boolean isVisibleToUser     = true;
    boolean toastShownToUser    = false;

    public static Bitmap        fb_profile_pic_bmp;
    public static String        fb_profile_name_str;
    public static String        fb_profile_id;
    public static ProgressDialog dialog;

    NavigationDrawerFragment    mNavigationDrawerFragment;
    ActionBar                   actionBar;
    AccessTokenTracker          mAccessTokenTracker;
    ProfileTracker              mProfileTracker;
    ImageView                   navigation_drawer_fb_profile_pic;
    TextView                    navigation_drawer_email,navigation_drawer_fb_name;
    ListView                    navigation_drawer_driver_listview,navigation_drawer_passenger_listview;
    Intent                      mServiceIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //setup progress dialog to be used in application wide
        dialog=new ProgressDialog(this);

        //put this before any facebook api calls
        FacebookSdk.sdkInitialize(getApplicationContext());

        setContentView(R.layout.activity_main);
        setTitle(getString(R.string.app_name));

        // find drawer fragment and setup, drawer must be a fragment
        mNavigationDrawerFragment = (NavigationDrawerFragment)getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mNavigationDrawerFragment.setUp(R.id.navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout));

        //setup trackers to track user login/logout
        setupFbApi();
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#2b918f")));

    }

    static void blockScreen(String blockMsg,Context context){
        dialog=new ProgressDialog(context);
        dialog.setMessage(blockMsg);
        dialog.setCancelable(false);
        dialog.setInverseBackgroundForced(false);
        try{
            dialog.show();
        }
        catch(Exception e){ }

    }

    static void unblockDialog(){
        dialog.hide();
    }

    /* READ THIS, IMPLEMENT CAREFULLY
    to detect if user logged in or out for first time
    if profile is not null -> user is logged in
    if profile is null -> user is not logged in
    take action acoordingly
    (this methods only call after all views has been established including drawer components, otherwise get npe)
     */
    void checkUserLoginState(){
        if(Profile.getCurrentProfile()!=null){// if already logged in
            new GetFBInfo().execute(Profile.getCurrentProfile());
        }else{// if logged out
            doWhenLoggedOut();
        }
    }

    /* Defines what happens when an item on the listview is click, mostly are fragment switching */
    @Override
    public void onDriverListViewItemSelected(String itemName){

        if(fb_profile_id==null || fb_profile_id.length()==0) return;


        if(itemName.equals("• 找乘客")){

        }else if(itemName.equals("搜索結果")){

        }
        else if(itemName.equals("乘客地圖")){

        }
        else if(itemName.equals("刊登發車")){
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, new CreateCarpoolInvitation())
                    .commit();
            setActionBarSubtitle("刊登發車");

        }else if(itemName.equals("管理乘客請求")){
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, new ManagePassenger())
                    .commit();
            setActionBarSubtitle("管理乘客請求");

        }
    }

    /* Defines what happens when an item on the listview is click, mostly are fragment switching */
    @Override
    public void onPassengerListViewItemSelected(String itemName){

        if(fb_profile_id==null || fb_profile_id.length()==0) return;

        //by using string to match it's more concrete than number, because order might change, but text won't
        if(itemName.equals("找共乘")){
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, new SearchInterface())
                    .commit();
            setActionBarSubtitle("找共乘");

        }else if(itemName.equals("搜索結果")){
            if(!(SearchInterface.searchData.hasExtra("src_lat") && SearchInterface.searchData.hasExtra("dst_lat"))){
                Toast.makeText(getApplicationContext(),"找不到最後一次的搜尋記錄",Toast.LENGTH_LONG).show();
                return;
            }
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, new SearchResultMap())
                    .commit();
            setActionBarSubtitle("搜索結果");
            setActionBarSubtitle("搜索結果");

        }else if(itemName.equals("今天出發地圖")){

        }else if(itemName.equals("刊登找車")){

        }else if(itemName.equals("預訂狀況")){
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, new ManageCarpool())
                    .commit();
            setActionBarSubtitle("預訂狀況");

        }
    }

    @Override
    public void passViewsToParent(ImageView profilePic,TextView name,TextView email,ListView passengerListView,ListView driverListView){
        //the views reference was passed from drawer and assign to local reference
        navigation_drawer_fb_profile_pic        = profilePic;
        navigation_drawer_fb_name               = name;
        navigation_drawer_email                 = email;
        navigation_drawer_passenger_listview    = passengerListView;
        navigation_drawer_driver_listview       = driverListView;

        //only check user login state after all views are passed from navigatino drawer fragment
        //otherwise setting profile picture will trigger null pointer exception
        checkUserLoginState();
    }

    public void restoreActionBar() {
        actionBar = getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDestroy(){
        try{//stop tracking when the application dies
            mAccessTokenTracker.stopTracking();
            mProfileTracker.stopTracking();
        }catch(Exception e){ }
        super.onDestroy();
    }

    public void setActionBarSubtitle(String subtitle){
        getSupportActionBar().setSubtitle(subtitle);
    }

    /*
    called by GetFBInfo, and now the object Profile is available
    when logged in, do :
    1.)set profile image and name for the views
    2.)set opacity of listview to let user know he/she is logged in
    */
    void doWhenLoggedIn(){
        //set color of listview
        navigation_drawer_fb_profile_pic.setImageBitmap(fb_profile_pic_bmp);
        navigation_drawer_fb_name.setText(fb_profile_name_str);
        navigation_drawer_email.setText("");

        //set opacity here
        navigation_drawer_passenger_listview.setAlpha((float) 1);
        navigation_drawer_driver_listview.setAlpha((float)1);

        //switch fragment, the fragment when user logs in
        getSupportFragmentManager().beginTransaction().replace(R.id.container, new SearchInterface()).commit();
    }

    /*
    DO WHEN LOGGED OUT :
    1.)rest all the views values
    2.)reset all user variable, profile pic, name, email
    3.)set opacity of listview to let user know he/she is not logged in
    */
    void doWhenLoggedOut(){
        // need to catch exception because when logging in for the 1st time and when user is logged out, system can't get the view objects
        navigation_drawer_fb_profile_pic.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.person, null));
        navigation_drawer_fb_name.setText("(請先登入)");
        navigation_drawer_email.setText("");

        fb_profile_pic_bmp  = null;
        fb_profile_name_str = "";
        fb_profile_id       = "";

        //grey out all listviews, and switch to today depart UI
        navigation_drawer_passenger_listview.setAlpha((float)0.3);
        navigation_drawer_driver_listview.setAlpha((float)0.3);

        //switch fragment, show the fragment when user is logged out
        getSupportFragmentManager().beginTransaction().replace(R.id.container, new LoginInterface()).commit();
    }

    void setupFbApi(){
        /*
        Set profile tracker and access token tracker to detect login and logout
        In our case only the profile tracker is in use
        newProfile==null->user logged out, newProfile!=null->user logged in
        */
        mProfileTracker=new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile newProfile) {

                if(newProfile==null){
                    /* Triggered when user log out is in action */
                    doWhenLoggedOut();
                    return;
                }else{
                    /* Triggered when user log in is in action */
                    new GetFBInfo().execute(newProfile);
                }
            }
        };

        //doesn't have much use in here.
        mAccessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken,AccessToken currentAccessToken) { }
        };
    }

    @Override
    public void onStart(){
        super.onStart();
        mServiceIntent = new Intent(this, GetNotification.class);
        this.startService(mServiceIntent);

        IntentFilter mStatusIntentFilter    = new IntentFilter(GetNotification.Constants.BROADCAST_ACTION);
        ResponseReceiver responseReceiver   = new ResponseReceiver(this);
        LocalBroadcastManager.getInstance(this).registerReceiver(responseReceiver, mStatusIntentFilter);
    }

    @Override
    public void onResume(){
        isVisibleToUser=true;
        GetNotification.hasReadPreviousNotification=true;
        super.onResume();
    }

    @Override
    public void onPause(){
        isVisibleToUser=false;
        super.onPause();
    }

    public void makeNotification(String notificationMsg){

        if(toastShownToUser) return;

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this);
        mBuilder.setSmallIcon(R.drawable.green_exclamation);
        mBuilder.setContentTitle("共乘新通知");
        mBuilder.setContentText(notificationMsg);
        mBuilder.setDefaults(Notification.DEFAULT_SOUND);
        mBuilder.setAutoCancel(true);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, new Intent(this, MainActivity.class), 0);

        mBuilder.setContentIntent(contentIntent);
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(1, mBuilder.build());

        ActionBar actionBar=getSupportActionBar();
        actionBar.setHomeAsUpIndicator(R.drawable.green_exclamation);

        if(isVisibleToUser){
            GetNotification.hasReadPreviousNotification = true;
            toastShownToUser                            = true;
            Toast.makeText(this, notificationMsg, Toast.LENGTH_LONG).show();
        }else{
            GetNotification.hasReadPreviousNotification = false;
            toastShownToUser                            = false;
        }
    }

    //this asynctask download a picture from given URL, then set facebook name and facebook picture to app accordingly
    public class GetFBInfo extends AsyncTask<Profile,Void,Integer>{

        @Override
        protected Integer doInBackground(Profile... fbProfile) {

            //get name from profile object
            fb_profile_name_str = fbProfile[0].getName();
            fb_profile_id       = fbProfile[0].getId();

            //Open a connection to server check if user has registered, if not insert a record.

            URL url = null;
            try {
                String queryString="http://140.116.83.83/api/check_user_exists.php?fb="+fbProfile[0].getId()+"&fName="+fbProfile[0].getFirstName()+"&lName="+fbProfile[0].getLastName();
                Log.d("queryString",queryString);
                url = new URL(queryString);
                URLConnection urlConn=url.openConnection();
                urlConn.setReadTimeout(1000);
                urlConn.setConnectTimeout(1000);
                String result=convertStreamToString(urlConn.getInputStream());

            }
            catch (MalformedURLException e) {
                e.printStackTrace();
            }
            catch (IOException e) {
                e.printStackTrace();
            }


            //create url connection, get bitmap file
            //use bitmap factory to decode downloaded binary to Bitmap
            try{
                fb_profile_pic_bmp=BitmapFactory.decodeStream((InputStream) new URL(fbProfile[0].getProfilePictureUri(80,80).toString()).getContent());
            }
            catch( MalformedURLException malE){
                malE.printStackTrace(); }
            catch (IOException e){
                e.printStackTrace(); }
            return 0;
        }

        @Override
        protected void onPostExecute(Integer i){
            try{
                doWhenLoggedIn();
            }catch(NullPointerException npe){ npe.printStackTrace();}
        }

        String convertStreamToString(java.io.InputStream is) {
            java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
            return s.hasNext() ? s.next() : "";
        }
    }
}
