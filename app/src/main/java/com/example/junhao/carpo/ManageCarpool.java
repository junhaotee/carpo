package com.example.junhao.carpo;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

public class ManageCarpool extends Fragment {

    TabLayout tabLayout;

    public ManageCarpool() { }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_manage_carpool, container, false);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onActivityCreated(Bundle savedInstancedState){
        super.onActivityCreated(savedInstancedState);
        tabLayout=(TabLayout)getView().findViewById(R.id.manage_carpool_tabs);
        tabLayout.setBackground(new ColorDrawable(Color.parseColor("#2b918f")));
        tabLayout.setTabTextColors(Color.parseColor("#ffffff"),Color.parseColor("#ffffff"));
        tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#ffffff"));
        tabLayout.addTab(tabLayout.newTab().setText("預訂狀態"));
        tabLayout.addTab(tabLayout.newTab().setText("即將出發"));
        tabLayout.addTab(tabLayout.newTab().setText("歷史記錄"));
        tabLayout.setOnTabSelectedListener(tabListener);


        getFragmentManager().beginTransaction().replace(R.id.tab_frame_layout,PassengerTabFactory.getSentRequest()).commit();

    }

    private TabLayout.OnTabSelectedListener tabListener=new TabLayout.OnTabSelectedListener() {

        @Override
        public void onTabSelected(TabLayout.Tab tab) {

            Fragment userChoseFragment=null;

            switch(tab.getText().toString()){
                case "預訂狀態":{
                    userChoseFragment=PassengerTabFactory.getSentRequest();
                };break;
                case "即將出發":{
                    userChoseFragment=PassengerTabFactory.getUpcomingCarpool();
                };break;
                case "歷史記錄":{
                    userChoseFragment=PassengerTabFactory.getCarpoolHistory();
                };break;
            }

            //return null if invalid fragment
            if(userChoseFragment==null)
                return;
            else{
                //clear all childviews in frame layout
                FrameLayout tabFrame=(FrameLayout)getView().findViewById(R.id.tab_frame_layout);
                tabFrame.removeAllViews();

                //add user selection framgent into framelayout
                getFragmentManager().beginTransaction().replace(R.id.tab_frame_layout,userChoseFragment).commit();
            }
        }

        //implement action if you wish
        @Override
        public void onTabUnselected(TabLayout.Tab tab) {}
        @Override
        public void onTabReselected(TabLayout.Tab tab) {}
    };


}
