package com.example.junhao.carpo;


import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;


public class CheckPassengerRequestStatusTab extends Fragment {

    boolean                                 loading=false;
    boolean                                 itemMax=false;
    int                                     currentItemOffset =0;

    Fragment                                currentFragment=this;
    View                                    footerView;
    ListView                                passenger_reservation_status_listview;
    CheckPassengerRequestStatusItemAdapter reservationItemAdapter;
    ArrayList<CheckPassengerRequestStatusItem> reservationItemList;
    static CheckPassengerRequestStatusItem chosenPassengerReservationItem;


    TextView                                listview_footer_tv_stat,
                                            passenger_reservation_status_count;

    public CheckPassengerRequestStatusTab() { }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_check_passenger_request_status, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
        setupViews();
        setupViewsData();

    }

    void setupViews(){
        passenger_reservation_status_listview   = (ListView)getView().findViewById(R.id.passenger_reservation_status_listview);
        reservationItemList                     = new ArrayList();
        reservationItemAdapter                  = new CheckPassengerRequestStatusItemAdapter(getContext(),reservationItemList);
        footerView                              = ((LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.listview_footer, null, false);
        listview_footer_tv_stat                 = (TextView)footerView.findViewById(R.id.listview_footer_tv_stat);
        passenger_reservation_status_count      = (TextView)getView().findViewById(R.id.passenger_reservation_status_count);

    }

    void setupViewsData(){
        passenger_reservation_status_listview.setAdapter(reservationItemAdapter);
        passenger_reservation_status_listview.addFooterView(footerView);
        passenger_reservation_status_listview.setOnScrollListener(reservationItemLvListener);
        passenger_reservation_status_listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(position>=reservationItemList.size())return;

                chosenPassengerReservationItem=reservationItemList.get(position);

                CancelDriverUnapprovedRequestDialog cancelDriverUnapprovedRequestDialog =new CancelDriverUnapprovedRequestDialog();
                cancelDriverUnapprovedRequestDialog.setTargetFragment(currentFragment, 10);
                cancelDriverUnapprovedRequestDialog.show(getChildFragmentManager(), "");
            }
        });

    }

    public void onActivityResult(int requestCode,int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode){
            case -1:{MainActivity.dialog.hide();}break;
            case 10:{onCancelDriverUnapproveDialogReturn(data);}break;
        }
    }

    public void onCancelDriverUnapproveDialogReturn(Intent data){
        new PassengerCancelRequest().execute();
    }

    AbsListView.OnScrollListener reservationItemLvListener=new AbsListView.OnScrollListener() {

        @Override
        public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            int lastInScreen = firstVisibleItem + visibleItemCount;
            if ((lastInScreen == totalItemCount && !loading &&!itemMax)) {
                loading = true;
                new GetPassengerReservation().execute();
            }
        }

        @Override
        public void onScrollStateChanged(AbsListView absListView, int i) { }
    };

    class GetPassengerReservation extends AsyncTask<Void,Void,Void> {
        String reservationResults;
        @Override
        protected Void doInBackground(Void... params) {

            try {
                String fk_user_fb=MainActivity.fb_profile_id;

                String queryString="http://140.116.83.83/api/get_passenger_reservation_status.php?currentItemOffset="+currentItemOffset+"&fk_user_fb="+fk_user_fb;
                Log.d("queryString",queryString);
                URL url = new URL(queryString);
                URLConnection urlConn=url.openConnection();

                urlConn.setReadTimeout(1000);
                urlConn.setConnectTimeout(1000);

                reservationResults=convertStreamToString(urlConn.getInputStream());

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                reservationResults="";//if server connect/read timeout, return empty list
                e.printStackTrace();
            }
            return null;
        }

        //onPostExecute is UI thread, updat the views here
        @Override
        protected void onPostExecute(Void result) {
            try{

                //if internal server exception, return empty string
                if(reservationResults.length()==0){
                    itemMax=true;
                    listview_footer_tv_stat.setText("");
                    return;
                }

                //parsed server response into json
                JSONArray reservationArr=new JSONArray(reservationResults);

                //if no result is found, add an empty item and quit
                if(reservationArr.length()==0){
                    itemMax=true;
                    listview_footer_tv_stat.setText("");
                }

                //if there were results, populates found result into arraylist
                for(int k=0;k<reservationArr.length();k++){
                    JSONObject reservationObj = new JSONObject(reservationArr.get(k).toString());
                    reservationItemList.add(new CheckPassengerRequestStatusItem(reservationObj));
                }

                //notify listview data changed, and unblock dialog
                //search_result_map_result_size.setText(locationArr.length()+"");
                reservationItemAdapter.notifyDataSetChanged();

                currentItemOffset +=reservationArr.length();
                passenger_reservation_status_count.setText(currentItemOffset+"");
                loading=false;

            }catch(JSONException jsone){ jsone.printStackTrace(); }
        }

        //method to convert input stream to string, used after reading input stream
        String convertStreamToString(java.io.InputStream is) {
            java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
            return s.hasNext() ? s.next() : "";
        }
    }

    class PassengerCancelRequest extends AsyncTask<Void,Void,Void> {

        String passengerRequestJSON;

        @Override
        protected Void doInBackground(Void... params) {

            try {
                String passenger_request_id		= CheckPassengerRequestStatusTab.chosenPassengerReservationItem.getItemValue("passenger_request_id");
                String fk_carpool_id			= CheckPassengerRequestStatusTab.chosenPassengerReservationItem.getItemValue("fk_carpool_id");
                String num_seat_requested		= CheckPassengerRequestStatusTab.chosenPassengerReservationItem.getItemValue("num_seat_requested");
                String fb						= MainActivity.fb_profile_id;

                String queryString="http://140.116.83.83/api/cancel_driver_unapproved_request.php?passenger_request_id="+passenger_request_id+"&fk_carpool_id="+fk_carpool_id+"&num_seat_requested="+num_seat_requested+"&fb="+fb;
                Log.d("queryString",queryString);
                URL url = new URL(queryString);
                URLConnection urlConn=url.openConnection();

                urlConn.setReadTimeout(1000);
                urlConn.setConnectTimeout(1000);

                passengerRequestJSON=convertStreamToString(urlConn.getInputStream());

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                passengerRequestJSON="";//if server connect/read timeout, return empty list
                e.printStackTrace();
            }
            return null;
        }

        //onPostExecute is UI thread, updat the views here
        @Override
        protected void onPostExecute(Void result) {

            if(passengerRequestJSON.length()>0 && passengerRequestJSON.equals("1")){
                reservationItemList.clear();
                currentItemOffset=0;
                reservationItemAdapter.notifyDataSetChanged();
                loading = true;
                new GetPassengerReservation().execute();
            }

        }

        //method to convert input stream to string, used after reading input stream
        String convertStreamToString(java.io.InputStream is) {
            java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
            return s.hasNext() ? s.next() : "";
        }
    }

}
