package com.example.junhao.carpo;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceLikelihoodBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.example.junhao.carpo.R.id.searchDst;
import static com.example.junhao.carpo.SearchInterface.searchData;


public class CreateCarpoolInvitation extends android.support.v4.app.Fragment{

    Fragment currentFragment =this;
    static Intent carpoolData=new Intent();

    EditText
            create_carpool_invitation_date_edittext,
            create_carpool_invitation_time_edittext,
            create_carpool_invitation_cartype_edittext,
            create_carpool_invitation_seat_edittext,
            create_carpool_invitation_costpp_edittext,
            create_carpool_invitation_carpoolpreference_edittext,
            create_carpool_invitation_dst_edittext,
            create_carpool_invitation_src_edittext,
            create_carpool_invitation_drivercomment_edittext;

    TextView
            create_carpool_invitation_src_tick,
            create_carpool_invitation_dst_tick,
            create_carpool_invitation_date_tick,
            create_carpool_invitation_time_tick,
            create_carpool_invitation_cartype_tick,
            create_carpool_invitation_seat_tick,
            create_carpool_invitation_costpp_tick,
            create_carpool_invitation_carpoolpreference_tick;

    Button
            create_carpool_invitation_preview_post,
            animate_to_src,
            animate_to_dst,
            animate_to_boundaries;

    GoogleApiClient mGoogleApiClient;

    GoogleMap mGoogleMap;

    boolean[] validFlag=new boolean[8];

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);

        //get views
        setupEditTextView();
        setupTickTextView();
        setupButtonView();

        //set listeners for all edittexts
        setupListeners();

        //setup googlemap
        setupGoogleMap();

        //get current location and set to src edittext
        getCurrentPlaceInfo();

        //setup zoombutton
        setupZoomButton();

    }



    void setupButtonView(){
        create_carpool_invitation_preview_post=(Button)getView().findViewById(R.id.create_carpool_invitation_preview_post);

        View.OnClickListener previewPostBtnListener=new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (checkFlags()){
                    case -1:{break;}
                    case 0:{Toast.makeText(getContext(),"尚未選擇日期",Toast.LENGTH_SHORT).show();return;}
                    case 1:{Toast.makeText(getContext(),"尚未選擇時間",Toast.LENGTH_SHORT).show();return;}
                    case 2:{Toast.makeText(getContext(),"尚未選擇車型",Toast.LENGTH_SHORT).show();return;}
                    case 3:{Toast.makeText(getContext(),"尚未選擇座位數",Toast.LENGTH_SHORT).show();return;}
                    case 4:{Toast.makeText(getContext(),"尚未選擇個人車資",Toast.LENGTH_SHORT).show();return;}
                    case 5:{Toast.makeText(getContext(),"尚未選擇出起點",Toast.LENGTH_SHORT).show();return;}
                    case 6:{Toast.makeText(getContext(),"尚未選擇迄點",Toast.LENGTH_SHORT).show();return;}
                    case 7:{Toast.makeText(getContext(),"尚未選擇共乘偏好",Toast.LENGTH_SHORT).show();return;}
                }
                if(MainActivity.fb_profile_id==null || MainActivity.fb_profile_id.length()==0){
                    Toast.makeText(getContext(),"請先登入FB",Toast.LENGTH_SHORT).show();
                    return;
                }
                carpoolData.putExtra("driverComment",create_carpool_invitation_drivercomment_edittext.getText()+"");

                //open dialog once all data integrity confirmed
                ConfirmInvitationDialog confirmInvitationDialog=new ConfirmInvitationDialog();
                confirmInvitationDialog.setTargetFragment(currentFragment, 10);
                confirmInvitationDialog.setArguments(setupPreviewTextBundle());
                confirmInvitationDialog.show(getChildFragmentManager(), "");

                showDialog("載入中...");
            }
        };
        create_carpool_invitation_preview_post.setOnClickListener(previewPostBtnListener);
    }

    Bundle setupPreviewTextBundle(){
        Bundle args=new Bundle();
        args.putString("previewSrc",create_carpool_invitation_src_edittext.getText()+"");
        args.putString("previewDst",create_carpool_invitation_dst_edittext.getText()+"");
        args.putString("previewDate",create_carpool_invitation_date_edittext.getText()+"");
        args.putString("previewTime",create_carpool_invitation_time_edittext.getText()+"");
        args.putString("previewVehicle",create_carpool_invitation_cartype_edittext.getText()+"");
        args.putString("previewSeat",create_carpool_invitation_seat_edittext.getText()+"");
        args.putString("previewCostpp",create_carpool_invitation_costpp_edittext.getText()+"");
        args.putString("previewComment",create_carpool_invitation_drivercomment_edittext.getText()+"");
        return args;
    }


    void setupZoomButton(){

        animate_to_src=(Button)getView().findViewById(R.id.animate_to_src);
        animate_to_dst=(Button)getView().findViewById(R.id.animate_to_dst);
        animate_to_boundaries=(Button)getView().findViewById(R.id.animate_to_boundaries);

        //listener definition
        View.OnClickListener src_dst_btn_listener=new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!(carpoolData.hasExtra("src_lat") || carpoolData.hasExtra("dst_lat"))) {
                    Toast.makeText(getContext(),"請先定位迄點",Toast.LENGTH_SHORT).show();
                    return;
                }

                if(!carpoolData.hasExtra("dst_lat")) {
                    Toast.makeText(getContext(),"請先定位迄點",Toast.LENGTH_SHORT).show();
                    return;
                }

                //if have both lat and lng
                if(carpoolData.hasExtra("src_lat") && carpoolData.hasExtra("dst_lat")){

                    //clear google map previous markers
                    mGoogleMap.clear();

                    //create src marker and dst marker set them on map
                    Marker[] markerList=new Marker[2];

                    //setups for latlng variables
                    double srcLat=carpoolData.getExtras().getDouble("src_lat");
                    double srcLng=carpoolData.getExtras().getDouble("src_lng");
                    double dstLat=carpoolData.getExtras().getDouble("dst_lat");
                    double dstLng=carpoolData.getExtras().getDouble("dst_lng");

                    //marker for dst
                    LatLng dstLocation = new LatLng(dstLat,dstLng);
                    markerList[0]=mGoogleMap.addMarker(new MarkerOptions().position(dstLocation).draggable(true));

                    //marker for src
                    LatLng srcLocation = new LatLng(srcLat,srcLng);
                    markerList[1]=mGoogleMap.addMarker(new MarkerOptions().position(srcLocation).draggable(true));

                    //create boundary for google map, so camera knows where to focus
                    LatLngBounds.Builder builder = new LatLngBounds.Builder();
                    for (Marker marker : markerList) {
                        builder.include(marker.getPosition());
                    }
                    LatLngBounds bounds = builder.build();

                    //int padding = getActivity().getResources().getDimensionPixelSize(R.dimen.home_map_padding);
                    // 100 is the offset of markers from edges of the map in pixels
                    //greater the number camera zoom is farther
                    CameraUpdate cameraUpdateTwoMarkers = CameraUpdateFactory.newLatLngBounds(bounds, 100);
                    mGoogleMap.animateCamera(cameraUpdateTwoMarkers);

                    return;
                }
            }
        };

        //listeners definition
        View.OnClickListener src_btn_listener=new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(carpoolData.hasExtra("src_lat")){
                    double dstLat=carpoolData.getExtras().getDouble("src_lat");
                    double dstLng=carpoolData.getExtras().getDouble("src_lng");

                    LatLng defaultLocation = new LatLng(dstLat,dstLng);
                    mGoogleMap.addMarker(new MarkerOptions().position(defaultLocation).draggable(true));
                    CameraUpdate locationCamera = CameraUpdateFactory.newLatLngZoom(defaultLocation, 15.0f);
                    mGoogleMap.animateCamera(locationCamera);
                }
            }
        };

        //listeners definition
        View.OnClickListener dst_btn_listener=new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(carpoolData.hasExtra("dst_lat")){
                    double dstLat=carpoolData.getExtras().getDouble("dst_lat");
                    double dstLng=carpoolData.getExtras().getDouble("dst_lng");

                    LatLng defaultLocation = new LatLng(dstLat,dstLng);
                    mGoogleMap.addMarker(new MarkerOptions().position(defaultLocation).draggable(true));
                    CameraUpdate locationCamera = CameraUpdateFactory.newLatLngZoom(defaultLocation, 15.0f);
                    mGoogleMap.animateCamera(locationCamera);
                }else{
                    Toast.makeText(getContext(),"請先定位迄點",Toast.LENGTH_SHORT).show();
                    return;
                }
            }
        };

        //attach zoom button listeners
        animate_to_src.setOnClickListener(src_btn_listener);
        animate_to_dst.setOnClickListener(dst_btn_listener);
        animate_to_boundaries.setOnClickListener(src_dst_btn_listener);
    }

    public void setupEditTextView(){
        create_carpool_invitation_date_edittext=(EditText)getView().findViewById(R.id.create_carpool_invitation_date_edittext);
        create_carpool_invitation_time_edittext=(EditText)getView().findViewById(R.id.create_carpool_invitation_time_edittext);
        create_carpool_invitation_cartype_edittext=(EditText)getView().findViewById(R.id.create_carpool_invitation_cartype_edittext);
        create_carpool_invitation_seat_edittext=(EditText)getView().findViewById(R.id.create_carpool_invitation_seat_edittext);
        create_carpool_invitation_costpp_edittext=(EditText)getView().findViewById(R.id.create_carpool_invitation_costpp_edittext);
        create_carpool_invitation_carpoolpreference_edittext=(EditText)getView().findViewById(R.id.create_carpool_invitation_carpoolpreference_edittext);
        create_carpool_invitation_dst_edittext =(EditText)getView().findViewById(R.id.create_carpool_invitation_dst_edittxt);
        create_carpool_invitation_src_edittext =(EditText)getView().findViewById(R.id.create_carpool_invitation_src_edittxt);
        create_carpool_invitation_drivercomment_edittext=(EditText)getView().findViewById(R.id.create_carpool_invitation_drivercomment_edittext);
    }

    public void setupTickTextView(){
        create_carpool_invitation_src_tick=(TextView)getView().findViewById(R.id.create_carpool_invitation_src_tick);
        create_carpool_invitation_dst_tick=(TextView)getView().findViewById(R.id.create_carpool_invitation_dst_tick);
        create_carpool_invitation_date_tick=(TextView)getView().findViewById(R.id.create_carpool_invitation_date_tick);
        create_carpool_invitation_time_tick=(TextView)getView().findViewById(R.id.create_carpool_invitation_time_tick);
        create_carpool_invitation_cartype_tick=(TextView)getView().findViewById(R.id.create_carpool_invitation_cartype_tick);
        create_carpool_invitation_seat_tick=(TextView)getView().findViewById(R.id.create_carpool_invitation_seat_tick);
        create_carpool_invitation_costpp_tick=(TextView)getView().findViewById(R.id.create_carpool_invitation_costpp_tick);
        create_carpool_invitation_carpoolpreference_tick=(TextView)getView().findViewById(R.id.create_carpool_invitation_carpoolpreference_tick);

    }

    public void setupListeners(){
        create_carpool_invitation_date_edittext.setOnClickListener(dateEditTextListener);
        create_carpool_invitation_time_edittext.setOnClickListener(timeEditTextListener);
        create_carpool_invitation_cartype_edittext.setOnClickListener(cartypeEditTextListener);
        create_carpool_invitation_seat_edittext.setOnClickListener(seatNumEditTextListener);
        create_carpool_invitation_costpp_edittext.setOnClickListener(costPPEditTextListener);
        create_carpool_invitation_carpoolpreference_edittext.setOnClickListener(carpoolPreferenceListener);
        create_carpool_invitation_dst_edittext.setOnClickListener(dstEditTextListener);
        create_carpool_invitation_src_edittext.setOnClickListener(srcEditTextListener);
    }


    View.OnClickListener dateEditTextListener=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            DatePickerDialog datePickerDialog=new DatePickerDialog();
            datePickerDialog.setTargetFragment(currentFragment,0);
            datePickerDialog.show(getChildFragmentManager(), "");
            showDialog("載入中...");
        }
    };

    View.OnClickListener timeEditTextListener=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            TimePickerDialog timePickerDialog=new TimePickerDialog();
            timePickerDialog.setTargetFragment(currentFragment,1);
            timePickerDialog.show(getChildFragmentManager(), "");
            showDialog("載入中...");
        }
    };

    View.OnClickListener cartypeEditTextListener=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            CartypePickerDialog cartypePickerDialog=new CartypePickerDialog();
            cartypePickerDialog.setTargetFragment(currentFragment, 2);
            cartypePickerDialog.show(getChildFragmentManager(), "");
            showDialog("載入中...");
        }
    };

    View.OnClickListener seatNumEditTextListener=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            SeatNumPickerDialog seatNumPickerDialog=new SeatNumPickerDialog();
            seatNumPickerDialog.setTargetFragment(currentFragment, 3);
            seatNumPickerDialog.show(getChildFragmentManager(), "");
            showDialog("載入中...");
        }
    };

    View.OnClickListener costPPEditTextListener=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            CostPickerDialog costPickerDialog=new CostPickerDialog();
            costPickerDialog.setTargetFragment(currentFragment, 4);
            costPickerDialog.show(getChildFragmentManager(), "");
            showDialog("載入中...");
        }
    };

    View.OnClickListener srcEditTextListener=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int PLACE_AUTOCOMPLETE_REQUEST_CODE = 5;
            try {
                //Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY).build(getActivity());
                Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY).build(getActivity());
                startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);

            }
            catch (GooglePlayServicesRepairableException e) { }
            catch (GooglePlayServicesNotAvailableException e) { }
        }
    };

    View.OnClickListener dstEditTextListener=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int PLACE_AUTOCOMPLETE_REQUEST_CODE = 6;
            try {
                //Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY).build(getActivity());
                Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY).build(getActivity());
                startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);

            }
            catch (GooglePlayServicesRepairableException e) { }
            catch (GooglePlayServicesNotAvailableException e) { }
        }
    };

    View.OnClickListener carpoolPreferenceListener=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            CarpoolRuleDialog carpoolRuleDialog=new CarpoolRuleDialog();
            carpoolRuleDialog.setTargetFragment(currentFragment,7);
            carpoolRuleDialog.show(getChildFragmentManager(), "");
            showDialog("載入中...");
        }
    };

    @Override
    public void onActivityResult(int requestCode,int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode){
            case -1:{MainActivity.dialog.hide();}break;
            case 0:{onDatePickerReturn(data);}break;
            case 1:{onTimePickerReturn(data);}break;
            case 2:{onCarTypePickerReturn(data);}break;
            case 3:{onSeatPickerReturn(data);}break;
            case 4:{onCostPPReturn(data);}break;
            case 5:{onSrcLocationReturn(data,resultCode);}break;
            case 6:{onDstLocationReturn(data,resultCode);}break;
            case 7:{onCarpoolPreferenceReturn(data);}break;
            case 10:{onCarpoolPostConfirmed(data);}break;
        }

    }
    void onCarpoolPostConfirmed(Intent data){
        new SendServerCarpoolData().execute(carpoolData);
    }

    public void showDialog(String msg) {
        MainActivity.dialog.setMessage(msg);
        MainActivity.dialog.setCancelable(true);
        MainActivity.dialog.setInverseBackgroundForced(false);
        MainActivity.dialog.show();
    }
    public void onDatePickerReturn(Intent data){
        //mainly formatting the date view text output
        carpoolData.putExtras(data);

        Calendar calendar = new GregorianCalendar(data.getExtras().getInt("y"),data.getExtras().getInt("m"),data.getExtras().getInt("d"));
        int day = calendar.get(Calendar.DAY_OF_WEEK);

        String dow="";
        switch (day) {
            case Calendar.MONDAY:{dow="一";break;}
            case Calendar.TUESDAY:{dow="二";break;}
            case Calendar.WEDNESDAY:{dow="三";break;}
            case Calendar.THURSDAY:{dow="四";break;}
            case Calendar.FRIDAY:{dow="五";break;}
            case Calendar.SATURDAY:{dow="六";break;}
            case Calendar.SUNDAY:{dow="日";break;}
        }

        String formattedDate=data.getExtras().getInt("y")+"年 "+(data.getExtras().getInt("m")+1)+"月 "+data.getExtras().getInt("d")+"日 （星期"+dow+"）";

        create_carpool_invitation_date_edittext.setText(formattedDate);
        boldAndGreenTick(create_carpool_invitation_date_tick);
        validFlag[0]=true;

    }
    public void onTimePickerReturn(Intent data){
        carpoolData.putExtras(data);
        String formattedTimeText =carpoolData.getExtras().getInt("h")+"點 "+carpoolData.getExtras().getInt("min")+"分 "+carpoolData.getExtras().getString("ampm");
        if(carpoolData.getExtras().getBoolean("interval_checked")){
            formattedTimeText+=" ~ "+carpoolData.getExtras().getInt("h2")+"點 "+carpoolData.getExtras().getInt("m2")+"分 "+carpoolData.getExtras().getString("ampm2");
        }
        String isNegoable=(carpoolData.getExtras().getBoolean("time_isNegoable"))?" (可議)":"";
        create_carpool_invitation_time_edittext.setText(formattedTimeText+isNegoable);
        boldAndGreenTick(create_carpool_invitation_time_tick);
        validFlag[1]=true;
    }
    public void onCarTypePickerReturn(Intent data) {
        carpoolData.putExtras(data);
        create_carpool_invitation_cartype_edittext.setText(carpoolData.getExtras().getString("vehicle_type"));
        boldAndGreenTick(create_carpool_invitation_cartype_tick);
        validFlag[2]=true;

    }
    public void onSeatPickerReturn(Intent data) {
        carpoolData.putExtras(data);
        create_carpool_invitation_seat_edittext.setText(carpoolData.getExtras().getString("seat_num") + " 個座位");
        boldAndGreenTick(create_carpool_invitation_seat_tick);
        validFlag[3]=true;

    }
    public void onCostPPReturn(Intent data) {
        carpoolData.putExtras(data);
        create_carpool_invitation_costpp_edittext.setText("NT$ " + carpoolData.getExtras().getString("costpp"));
        boldAndGreenTick(create_carpool_invitation_costpp_tick);
        validFlag[4]=true;

    }
    public void onSrcLocationReturn(Intent data,int resultCode){

        //do nothing if no results were found
        if(resultCode==0)return;

        Place place = PlaceAutocomplete.getPlace(getContext(), data);

        carpoolData.putExtra("src_lat", place.getLatLng().latitude);
        carpoolData.putExtra("src_lng", place.getLatLng().longitude);
        carpoolData.putExtra("src_name", place.getName());
        carpoolData.putExtra("src_address", place.getAddress());

        //get postcode
        Matcher m = Pattern.compile(".*?([1-9][0-9][0-9])?.*?").matcher(place.getAddress());
        int postCode=(m.matches())?Integer.parseInt(m.group(1)) : 0;
        carpoolData.putExtra("src_postcode", postCode);

        //if(postCode!=0)new FormatLocationName().execute(true);

        setupCoordinatesIfAvailable();
        boldAndGreenTick(create_carpool_invitation_src_tick);
        validFlag[5]=true;

    }
    public void onDstLocationReturn(Intent data,int resultCode){

        //do nothing if no results were found
        if(resultCode==0)return;

        Place place = PlaceAutocomplete.getPlace(getContext(), data);

        carpoolData.putExtra("dst_lat", place.getLatLng().latitude);
        carpoolData.putExtra("dst_lng",place.getLatLng().longitude);
        carpoolData.putExtra("dst_name", place.getName());
        carpoolData.putExtra("dst_address",place.getAddress());

        int postCode=0;
        try{
            Matcher m = Pattern.compile(".*?([1-9][0-9][0-9])?.*?").matcher(place.getAddress());
            postCode=(m.matches())?Integer.parseInt(m.group(1)):0;
        }catch(Exception e){
            Toast.makeText(getContext(),"位置不夠精準",Toast.LENGTH_LONG).show();
            create_carpool_invitation_dst_edittext.setText("");
            carpoolData.removeExtra("dst_postcode");
            carpoolData.removeExtra("dst_lat");
            setupCoordinatesIfAvailable();
            validFlag[6]=false;
            return;
        }
        Log.d("postCode",postCode+"");
        if(postCode==0)return;

        carpoolData.putExtra("dst_postcode",postCode);
        create_carpool_invitation_dst_edittext.setText(place.getName() + "\n" + place.getAddress());
        if (postCode!=0)new FormatLocationName().execute(false);

        setupCoordinatesIfAvailable();

        boldAndGreenTick(create_carpool_invitation_dst_tick);
        validFlag[6]=true;
    }

    public void onCarpoolPreferenceReturn(Intent data){
        carpoolData.putExtras(data);
        create_carpool_invitation_carpoolpreference_edittext.setText("(偏好已保存)");
        setupCoordinatesIfAvailable();
        boldAndGreenTick(create_carpool_invitation_carpoolpreference_tick);
        validFlag[7]=true;
    }


    public CreateCarpoolInvitation() { }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return (ViewGroup)inflater.inflate(R.layout.fragment_create_carpool_invitation, container, false);
    }


    @Override
    public void onDetach() { super.onDetach();}

    public void setupGoogleMap(){
        //pass supportmapfragment's google map to mGoogleMap variable
        if(mGoogleMap!=null)return;
        SupportMapFragment mapFrag = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFrag.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                //Log.d("","\n\n\nsuccessfully implemented");
                mGoogleMap = googleMap;
                mGoogleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                    @Override
                    public void onMapLoaded() {
                        setupCoordinatesIfAvailable();
                    }
                });
            }
        });
    }

    @Override
    public void onDestroyView() {
        mGoogleApiClient.stopAutoManage(getActivity());
        mGoogleApiClient.disconnect();
        super.onDestroyView();
    }

    public void setupCoordinatesIfAvailable(){
        //clear previous markers
        if(mGoogleMap!=null)
            mGoogleMap.clear();

        //add marker and move camera for src location
        if(!(carpoolData.hasExtra("src_lat") || carpoolData.hasExtra("dst_lat"))) return;

        //if have both lat and lng
        if(carpoolData.hasExtra("src_lat") && carpoolData.hasExtra("dst_lat")){

            //clear google map previous markers
            mGoogleMap.clear();

            //create src marker and dst marker set them on map
            Marker[] markerList=new Marker[2];

            //setups for latlng variables
            double srcLat=carpoolData.getExtras().getDouble("src_lat");
            double srcLng=carpoolData.getExtras().getDouble("src_lng");
            double dstLat=carpoolData.getExtras().getDouble("dst_lat");
            double dstLng=carpoolData.getExtras().getDouble("dst_lng");

            //marker for dst
            LatLng dstLocation = new LatLng(dstLat,dstLng);
            markerList[0]=mGoogleMap.addMarker(new MarkerOptions().position(dstLocation).draggable(true));

            //marker for src
            LatLng srcLocation = new LatLng(srcLat,srcLng);
            markerList[1]=mGoogleMap.addMarker(new MarkerOptions().position(srcLocation).draggable(true));

            //create boundary for google map, so camera knows where to focus
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            for (Marker marker : markerList) {
                builder.include(marker.getPosition());
            }
            LatLngBounds bounds = builder.build();

            //int padding = getActivity().getResources().getDimensionPixelSize(R.dimen.home_map_padding);
            // 100 is the offset of markers from edges of the map in pixels
            //greater the number camera zoom is farther
            CameraUpdate cameraUpdateTwoMarkers = CameraUpdateFactory.newLatLngBounds(bounds, 100);
            mGoogleMap.animateCamera(cameraUpdateTwoMarkers);

            create_carpool_invitation_src_edittext.setText(carpoolData.getExtras().getString("src_city_town") + carpoolData.getStringExtra("src_name") + "\n" + carpoolData.getStringExtra("src_address"));
            new FormatLocationName().execute(true);

            create_carpool_invitation_dst_edittext.setText(carpoolData.getExtras().getString("dst_city_town") + carpoolData.getStringExtra("dst_name") + "\n" + carpoolData.getStringExtra("dst_address"));
            new FormatLocationName().execute(false);

            boldAndGreenTick(create_carpool_invitation_dst_tick);
            boldAndGreenTick(create_carpool_invitation_src_tick);

            validFlag[5]=true;
            validFlag[6]=true;

            return;
        }

        if(carpoolData.hasExtra("src_lat")){
            double srcLat=carpoolData.getExtras().getDouble("src_lat");
            double srcLng=carpoolData.getExtras().getDouble("src_lng");
            LatLng defaultLocation = new LatLng(srcLat,srcLng);
            mGoogleMap.addMarker(new MarkerOptions().position(defaultLocation).draggable(true));
            CameraUpdate locationCamera = CameraUpdateFactory.newLatLngZoom(defaultLocation, 15.0f);
            mGoogleMap.animateCamera(locationCamera);

            create_carpool_invitation_src_edittext.setText(carpoolData.getExtras().getString("src_city_town")+carpoolData.getStringExtra("src_name") + "\n" + carpoolData.getStringExtra("src_address"));

            boldAndGreenTick(create_carpool_invitation_src_tick);
        }

        if(carpoolData.hasExtra("dst_lat")){
            double dstLat=carpoolData.getExtras().getDouble("dst_lat");
            double dstLng=carpoolData.getExtras().getDouble("dst_lng");
            LatLng defaultLocation = new LatLng(dstLat,dstLng);
            mGoogleMap.addMarker(new MarkerOptions().position(defaultLocation).draggable(true));
            CameraUpdate locationCamera = CameraUpdateFactory.newLatLngZoom(defaultLocation, 15.0f);
            mGoogleMap.animateCamera(locationCamera);

            create_carpool_invitation_dst_edittext.setText(carpoolData.getExtras().getString("dst_city_town")+carpoolData.getStringExtra("dst_name") + "\n" + carpoolData.getStringExtra("dst_address"));
            boldAndGreenTick(create_carpool_invitation_dst_tick);
        }
    }

    public void boldAndGreenTick(TextView textView){
        textView.setTypeface(null, Typeface.BOLD);
        textView.setTextColor(Color.parseColor("#6FFF5C"));
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    public int checkFlags(){
        int incompleteInput=-1;
        for(int k=0;k<validFlag.length;k++){
            if(!validFlag[k]){
                incompleteInput=k;
                break;
            }
        }
        return incompleteInput;
    }

    //retrieve current location
    public void getCurrentPlaceInfo(){

        //setup google api client, only used in this method
        mGoogleApiClient=new GoogleApiClient.Builder(getContext())
                .enableAutoManage(getActivity(), new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) { }
                })
                .addApi(Places.PLACE_DETECTION_API)
                .build();

        //do a permission check, if permission not granted, prompt user to select by his own
        if ( ContextCompat.checkSelfPermission( getContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION ) != PackageManager.PERMISSION_GRANTED ) {
            //if permission not granted
            Toast.makeText(getContext(),"獲取當地資訊權限不足",Toast.LENGTH_SHORT).show();
        }

        PendingResult<PlaceLikelihoodBuffer> result= Places.PlaceDetectionApi.getCurrentPlace(mGoogleApiClient, null);

        result.setResultCallback(new ResultCallback<PlaceLikelihoodBuffer>() {
            @Override
            public void onResult(PlaceLikelihoodBuffer likelyPlaces) {

                //the first of results is the most possible place where user is at
                Place currentPlace = likelyPlaces.get(0).getPlace();

                carpoolData.putExtra("src_lat", currentPlace.getLatLng().latitude);
                carpoolData.putExtra("src_lng", currentPlace.getLatLng().longitude);
                carpoolData.putExtra("src_name", currentPlace.getName());
                carpoolData.putExtra("src_address", currentPlace.getAddress());
                create_carpool_invitation_src_edittext.setText(currentPlace.getName() + "\n" + currentPlace.getAddress());

                //get postcode
                Matcher m = Pattern.compile(".*([1-9][0-9][0-9]).*").matcher(currentPlace.getAddress());
                int postCode=(m.matches())?Integer.parseInt(m.group(1)) : 0;
                carpoolData.putExtra("src_postcode",postCode);

                if(postCode!=0)new FormatLocationName().execute(true);
                //release resources to prevent mem leak
                likelyPlaces.release();
                setupCoordinatesIfAvailable();
            }
        });
    }

    public class GetLocationName extends AsyncTask<Integer,Void,Integer> {

        String result;

        @Override
        protected Integer doInBackground(Integer... params) {

            try {
                //send query and read response into string
                String GoogleMapAPIQuery="https://maps.googleapis.com/maps/api/geocode/json?latlng="+carpoolData.getExtras().getString("src_lat")+","+carpoolData.getExtras().getString("src_lng")+"&key="+getResources().getString(R.string.GoogleMapAPIKey);
                Log.d("GoogleMapAPIQuery", GoogleMapAPIQuery);
                URL url = new URL(GoogleMapAPIQuery);
                result=convertStreamToString(url.openConnection().getInputStream());

            }
            catch (MalformedURLException e) { e.printStackTrace();}
            catch (IOException e) { e.printStackTrace(); }
            return null;
        }

        @Override
        protected void onPostExecute(Integer uselessParam){

            try{
                //parse json from Google
                //be sure you are clear with returned json structure
                JSONObject locationObj=new JSONObject(result);

                //if status is not ok, end the method immediately
                if (!locationObj.getString("status").equals("OK")) return;

                //get json array from json object, and set the first location name to EditText(searchSrc)
                JSONArray locationJSONResults=new JSONArray(locationObj.get("results").toString());

                //formatted_address_src=locationJSONResults.getJSONObject(0).getString("formatted_address");
                carpoolData.putExtra("src_formatted_address", locationJSONResults.getJSONObject(0).getString("formatted_address"));
                carpoolData.putExtra("src_address_component",locationJSONResults.getJSONObject(0).getString("address_components"));
                create_carpool_invitation_src_edittext.setText(locationJSONResults.getJSONObject(0).getString("formatted_address"));

                new FormatLocationName().execute(true);

            }catch (JSONException e) { e.printStackTrace(); }

        }

        String convertStreamToString(java.io.InputStream is) {
            java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
            return s.hasNext() ? s.next() : "";
        }
    }

    class FormatLocationName extends AsyncTask<Boolean,Void,String>{

        boolean isSource;

        @Override
        protected String doInBackground(Boolean... isSource) {


            this.isSource=isSource[0];

            String postCode=(this.isSource)?carpoolData.getExtras().getInt("src_postcode")+"":carpoolData.getExtras().getInt("dst_postcode")+"";
            String locationName="";
            InputStream inputStream=null;

            try {
                //need to add 'http://' otherwise malform exception is thrown
                String queryString="http://140.116.83.83/api/get_location.php?postcode=" + URLEncoder.encode(postCode, "utf8");
                Log.d("queryString", queryString);

                //open server connection
                URL url = new URL(queryString);
                URLConnection urlConn=url.openConnection();

                //to detect server failure, must do timeout settings, otherwise thsi thread will keep waiting until timeout
                urlConn.setConnectTimeout(1000);
                urlConn.setReadTimeout(1000);

                //read server response
                inputStream=urlConn.getInputStream();
                String tempJSONOArrString=convertStreamToString(inputStream);
                inputStream.close();

                //parse response into json array, if response is 0 length or invalid jsonarr, return '[]' directly
                JSONArray locationNameJSONArr=new JSONArray(tempJSONOArrString);
                if(locationNameJSONArr==null || locationNameJSONArr.length()==0)return "";
                locationName="["+locationNameJSONArr.getJSONObject(0).getString(getResources().getString(R.string.db_city_name))+" "+locationNameJSONArr.getJSONObject(0).getString(getResources().getString(R.string.db_town_name))+"]";
            }
            catch(MalformedURLException mue) {
                mue.printStackTrace();
                return "";
            }
            catch(IOException ioe) {
                ioe.printStackTrace();
                try{
                    inputStream.close();
                }
                catch(NullPointerException npe){ npe.printStackTrace(); }
                catch (IOException e) { e.printStackTrace(); }
                return "";
            }
            catch(JSONException jsone) {
                jsone.printStackTrace();
                return "";
            }
            return locationName;
        }

        @Override
        protected void onPostExecute(String formattedPrefix){
            //Toast.makeText(getContext(),"FormatLocationName",Toast.LENGTH_LONG).show();
            if (isSource){
                carpoolData.putExtra("src_city_town",formattedPrefix);
                create_carpool_invitation_src_edittext.setText(formattedPrefix + " " + carpoolData.getExtras().getString("src_name")+"\n"+carpoolData.getExtras().getString("src_address"));
            }else{
                carpoolData.putExtra("dst_city_town",formattedPrefix);
                create_carpool_invitation_dst_edittext.setText(formattedPrefix + " " + carpoolData.getExtras().getString("dst_name")+"\n"+carpoolData.getExtras().getString("dst_address"));
            }
        }
        String convertStreamToString(java.io.InputStream is) {
            java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
            return s.hasNext() ? s.next() : "";
        }
    }

    class SendServerCarpoolData extends AsyncTask<Intent,Void,Integer>{
        @Override
        protected Integer doInBackground(Intent... params) {
            String data="";
            StringBuilder serverResponse = new StringBuilder();

            try{
                /* DATA PREPARATION SECTION: careful for the namings, might caught column name mismatch in databases */

                /* id of current user who created the carpool, the driver */
                data = URLEncoder.encode("owner", "UTF-8") + "=" + URLEncoder.encode(MainActivity.fb_profile_id, "UTF-8");

                /* source and destination name */
                data +="&"+ URLEncoder.encode("start", "UTF-8") + "=" + URLEncoder.encode(params[0].getExtras().getString("src_name"), "UTF-8");
                data +="&"+ URLEncoder.encode("destination", "UTF-8") + "=" + URLEncoder.encode(params[0].getExtras().getString("dst_name"), "UTF-8");

                /* source and destination detail address */
                data +="&"+ URLEncoder.encode("srcFormattedAddress", "UTF-8") + "=" + URLEncoder.encode(params[0].getExtras().getString("src_address"), "UTF-8");
                data +="&"+ URLEncoder.encode("dstFormattedAddress", "UTF-8") + "=" + URLEncoder.encode(params[0].getExtras().getString("dst_address"), "UTF-8");

                /* date of departure interval 1 */
                int y=params[0].getExtras().getInt("y");
                int m=params[0].getExtras().getInt("m")+1;
                int d=params[0].getExtras().getInt("d");
                int h=params[0].getExtras().getInt("formattedH");
                int min=params[0].getExtras().getInt("min");
                Log.d("departTime","month"+m);


                String departTime=y+"-"+m+"-"+d+" "+ h+":"+min+":00";
                data +="&"+ URLEncoder.encode("timeDepart", "UTF-8") + "=" + URLEncoder.encode(departTime, "UTF-8");

                /* cost per each passenger */
                int costpp=Integer.parseInt(params[0].getExtras().getString("costpp"));
                data +="&"+ URLEncoder.encode("costpp", "UTF-8") + "=" + URLEncoder.encode(costpp+"", "UTF-8");

                /* is cost negotiable */
                boolean isCostNego=params[0].getExtras().getBoolean("isCostNego");
                data +="&"+ URLEncoder.encode("isCostNego", "UTF-8") + "=" + URLEncoder.encode(isCostNego+"", "UTF-8");

                /* comment of driver to passengers */
                String driverComment=params[0].getExtras().getString("driverComment")+"";
                data +="&"+ URLEncoder.encode("comment", "UTF-8") + "=" + URLEncoder.encode(driverComment, "UTF-8");

                /* latlng of source and destination */
                String startLat=params[0].getExtras().getDouble("src_lat")+"";
                String startLng=params[0].getExtras().getDouble("src_lng")+"";
                String dstLng=params[0].getExtras().getDouble("dst_lng")+"";
                String dstLat=params[0].getExtras().getDouble("dst_lat")+"";

                data +="&"+ URLEncoder.encode("startlat", "UTF-8") + "=" + URLEncoder.encode(startLat, "UTF-8");
                data +="&"+ URLEncoder.encode("startlng", "UTF-8") + "=" + URLEncoder.encode(startLng, "UTF-8");
                data +="&"+ URLEncoder.encode("dstlng", "UTF-8") + "=" + URLEncoder.encode(dstLng, "UTF-8");
                data +="&"+ URLEncoder.encode("dstlat", "UTF-8") + "=" + URLEncoder.encode(dstLat, "UTF-8");

                /* seat taken, useless but stated here as data integrity convention */
                data +="&"+ URLEncoder.encode("seatTaken", "UTF-8") + "=" + URLEncoder.encode(0+"", "UTF-8");

                /* position of vehicle position in listview */
                String vehicleType=params[0].getExtras().getInt("vehicle_pos")+"";
                data +="&"+ URLEncoder.encode("cType", "UTF-8") + "=" + URLEncoder.encode(vehicleType, "UTF-8");

                /* is departure time negoable? */
                boolean timeNegoable=params[0].getExtras().getBoolean("time_isNegoable");
                data +="&"+ URLEncoder.encode("timeNego", "UTF-8") + "=" + URLEncoder.encode(timeNegoable+"", "UTF-8");

                /* is departure time anytime? */
                boolean timeAnytime=params[0].getExtras().getBoolean("time_isAnytime");
                data +="&"+ URLEncoder.encode("isAnytime", "UTF-8") + "=" + URLEncoder.encode(timeAnytime+"", "UTF-8");

                /* number of seat given by driver */
                String seatGiven=params[0].getExtras().getString("seat_num")+"";
                data +="&"+ URLEncoder.encode("seatGiven", "UTF-8") + "=" + URLEncoder.encode(seatGiven, "UTF-8");

                /* is there a departure interval given? */
                boolean hasInterval=params[0].getExtras().getBoolean("interval_checked");
                data +="&"+ URLEncoder.encode("hasInterval", "UTF-8") + "=" + URLEncoder.encode(hasInterval+"", "UTF-8");

                /*postcode of sourc and destination */
                String startPostcode=params[0].getExtras().getInt("src_postcode")+"";
                String destinationPostcode=params[0].getExtras().getInt("dst_postcode")+"";

                data +="&"+ URLEncoder.encode("startPostcode", "UTF-8") + "=" + URLEncoder.encode(startPostcode, "UTF-8");
                data +="&"+ URLEncoder.encode("destinationPostcode", "UTF-8") + "=" + URLEncoder.encode(destinationPostcode, "UTF-8");

                /*  the preference of carpool set by driver */
                boolean isFoodAllowed=params[0].getExtras().getBoolean("carpool_food");
                boolean isBigLuggageAllowed=params[0].getExtras().getBoolean("carpool_luggage");
                boolean isStopByAllowed=params[0].getExtras().getBoolean("carpool_stopby");
                boolean isPaymentAfterBefore=params[0].getExtras().getBoolean("carpool_payafterward");
                boolean isIdentityCheckNeeded=params[0].getExtras().getBoolean("carpool_identity_check");

                data +="&"+ URLEncoder.encode("isFoodAllowed", "UTF-8") + "=" + URLEncoder.encode(isFoodAllowed+"", "UTF-8");
                data +="&"+ URLEncoder.encode("isBigLuggageAllowed", "UTF-8") + "=" + URLEncoder.encode(isBigLuggageAllowed+"", "UTF-8");
                data +="&"+ URLEncoder.encode("isStopByAllowed", "UTF-8") + "=" + URLEncoder.encode(isStopByAllowed+"", "UTF-8");
                data +="&"+ URLEncoder.encode("isPaymentAfterBefore", "UTF-8") + "=" + URLEncoder.encode(isPaymentAfterBefore+"", "UTF-8");
                data +="&"+ URLEncoder.encode("isIdentityCheckNeeded", "UTF-8") + "=" + URLEncoder.encode(isIdentityCheckNeeded+"", "UTF-8");

                /* the second interval of departure time */
                String h2=params[0].getExtras().getInt("formattedH2")+"";
                String min2=params[0].getExtras().getInt("m2")+"";

                String timeDepart2=y+"-"+m+"-"+d+" "+ h2+":"+min2+":00";
                data +="&"+ URLEncoder.encode("timeDepart2", "UTF-8") + "=" + URLEncoder.encode(timeDepart2+"", "UTF-8");

            } catch(UnsupportedEncodingException uee){ uee.printStackTrace(); }

            try{
                //Send data to server
                String queryServer="http://140.116.83.83/api/post_carpool.php";
                URLConnection conn = new URL(queryServer).openConnection();

                //must set timeout otherwise by default it wait for somewhile
                conn.setConnectTimeout(1000);
                conn.setReadTimeout(1000);

                conn.setDoOutput(true);

                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write( data );
                wr.flush();

                // setup buffer reader
                String bufferLine="";
                BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));

                // Read server response into serverResponse
                while((bufferLine = reader.readLine()) != null)
                    serverResponse.append(bufferLine + "\n");

                reader.close();
            } catch (MalformedURLException e) {
                e.printStackTrace();
                return 1;
            } catch (IOException e) {
                //show toast and return to CreateCarpoolInvitation
                Toast.makeText(getContext(),"刊登出現問題，請多嘗試一下",Toast.LENGTH_SHORT).show();
                return 1;
            }

            //return 0 for OK, 1 for else responses
            int returnCode=Integer.parseInt(serverResponse.toString().trim());

            return (returnCode==0)?0:1;
        }

        @Override
        protected void onPostExecute(Integer i){

            if(i!=0){
                Toast.makeText(getContext(),"刊登出現問題，請多嘗試一下",Toast.LENGTH_SHORT).show();
                return;
            }

            getFragmentManager().beginTransaction().replace(R.id.container, new SearchInterface()).commit();

        }
    }
}
