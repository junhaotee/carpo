package com.example.junhao.carpo;

/**
 * Created by junhao on 2016/9/23.
 */

public class CarownerTabFactory {
    static ManagePassenger getTabContainer(){
        return new ManagePassenger();
    }

    static CheckUpcomingPassengerTab getUpcomingPassenger(){
        return new CheckUpcomingPassengerTab();
    }

    static CarpoolDetailTab getCarpoolDetail(){
        return new CarpoolDetailTab();
    }

    static CheckPassengerHistoryTab getPassengerHistory(){
        return new CheckPassengerHistoryTab();
    }

    static ApprovePassengerRequestTab getNewRequest(){
        return new ApprovePassengerRequestTab();
    }
}
