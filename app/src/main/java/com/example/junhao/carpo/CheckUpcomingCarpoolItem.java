package com.example.junhao.carpo;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by junhao on 2015/12/7.
 */
public class CheckUpcomingCarpoolItem {

    private JSONObject upcomingCarpoolItem;

    public CheckUpcomingCarpoolItem(JSONObject upcomingCarpoolResult){
        upcomingCarpoolItem=upcomingCarpoolResult;
    }

    //a convenient method for ResultListAdapter to retrieve json values
    public String getItemValue(String JSONKey){

        try {
            return upcomingCarpoolItem.getString(JSONKey).toString();
        } catch (JSONException e) { e.printStackTrace();}
        return null;
    }

    public boolean isItemNull(){
        return (upcomingCarpoolItem==null)?true:false;
    }

    public JSONObject getJSONObject(){
        return upcomingCarpoolItem;
    }

}
