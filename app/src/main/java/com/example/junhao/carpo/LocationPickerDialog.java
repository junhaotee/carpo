package com.example.junhao.carpo;


import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;

import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;

import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;


public class LocationPickerDialog extends DialogFragment{

    GoogleMap location_picker_map;
    SupportMapFragment supportMapFragment;
    SearchView location_picker_searchview;
    ListView location_picker_suggestion_listview;
    ArrayAdapter arrayAdapter;
    ArrayList locationArrayList;
    TextView location_picker_suggestion_count;
    JSONArray locationFormattedAddress=new JSONArray();
    JSONArray locationLatLng=new JSONArray();
    JSONArray locationAddrComponent=new JSONArray();
    int lastChosenLocation;
    GPSLocationListener gpsLocationListener;
    LocationManager locationManager;
    LatLng currentLocation;
    Button location_picker_mylocation_btn;

    public LocationPickerDialog() { }

    @Override
    public void onPause(){
        super.onPause();
        dismiss();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);

        lastChosenLocation=99;

        //textview for result count
        location_picker_suggestion_count=(TextView)getView().findViewById(R.id.location_picker_suggestion_count);

        //setup searchbox
        location_picker_searchview=(SearchView) getView().findViewById(R.id.location_picker_searchview);
        location_picker_searchview.setQuery("", true);
        location_picker_searchview.setFocusable(true);
        location_picker_searchview.setIconified(false);
        location_picker_searchview.requestFocusFromTouch();
        location_picker_searchview.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.length() < 1) return false;

                lastChosenLocation = 99;
                new DownloadLocationList().execute(newText);
                return true;
            }

            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }
        });


        getDialog().setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                getTargetFragment().onActivityResult(-1, -1, null);
            }
        });


        //setup location listview
        location_picker_suggestion_listview=(ListView)getView().findViewById(R.id.location_picker_suggestion_listview);
        locationArrayList=new ArrayList();
        arrayAdapter=new ArrayAdapter(getContext(),R.layout.location_item_textview,locationArrayList);
        location_picker_suggestion_listview.setAdapter(arrayAdapter);
        arrayAdapter.notifyDataSetChanged();

        location_picker_suggestion_listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                try {

                    //if user click the same item again, send data to targetfragment and dismiss dialog fragment
                    if (lastChosenLocation == position) {

                        Intent locationData = new Intent();

                        String prefix = (getTargetRequestCode() == 5) ? "src_" : "dst_";

                        locationData.putExtra(prefix + "formatted_address", locationFormattedAddress.getString(position));
                        locationData.putExtra(prefix + "lat", locationLatLng.getJSONObject(position).getString("lat"));
                        locationData.putExtra(prefix + "lng", locationLatLng.getJSONObject(position).getString("lng"));
                        locationData.putExtra(prefix + "address_component", locationAddrComponent.getString(position).toString());

                        getTargetFragment().onActivityResult(getTargetRequestCode(), getTargetRequestCode(), locationData);
                        getDialog().dismiss();
                    }

                    if (location_picker_map == null) return;
                    double lat = Double.parseDouble(locationLatLng.getJSONObject(position).getString("lat"));
                    double lng = Double.parseDouble(locationLatLng.getJSONObject(position).getString("lng"));
                    LatLng defaultLocation = new LatLng(lat, lng);
                    location_picker_map.clear();
                    location_picker_map.addMarker(new MarkerOptions().position(defaultLocation).draggable(true));
                    CameraUpdate locationCamera = CameraUpdateFactory.newLatLngZoom(defaultLocation, 15.0f);
                    location_picker_map.animateCamera(locationCamera);

                    //remember what user clicks
                    lastChosenLocation = position;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        //setup button
        location_picker_mylocation_btn=(Button)getView().findViewById(R.id.location_picker_mylocation_btn);
        location_picker_mylocation_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DownloadLatLngLocationList().execute();
            }
        });
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {

        getDialog().setTitle("找尋地點");

        FragmentManager fm = getChildFragmentManager();
        supportMapFragment = new SupportMapFragment() {
            @Override
            public void onActivityCreated(Bundle savedInstanceState) {
                super.onActivityCreated(savedInstanceState);
               // location_picker_map = supportMapFragment.getMap();
            }
        };
        fm.beginTransaction().replace(R.id.mapContainer, supportMapFragment).commit();

        return inflater.inflate(R.layout.fragment_location_picker_dialog, container, false);
    }

    public void getCurrentLocationList(){

        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        gpsLocationListener = new GPSLocationListener();

        //setting for update location, updates every x minutes
        if (ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED){
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, gpsLocationListener);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, gpsLocationListener);
        }else{
            return;
        }

        //get data from location listeners
        double lat=locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER).getLatitude();
        double lng=locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER).getLongitude();

        //set to class var latlng, latter used by downloadlatlnglist
        currentLocation=new LatLng(lat,lng);

        //set to null for garbage collection
        locationManager.removeUpdates(gpsLocationListener);
        gpsLocationListener=null;
        locationManager=null;
    }

    class GPSLocationListener implements LocationListener {
        @Override
        public void onLocationChanged(Location location) { }
        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) { }
        @Override
        public void onProviderEnabled(String provider) { }
        @Override
        public void onProviderDisabled(String provider) { }
    }


    //find list by keyword
    class DownloadLocationList extends AsyncTask<String,Void,Integer> {

        String jsonStringResponse;
        JSONObject locationJSONObj;
        JSONArray locationJSONArray;

        @Override
        protected Integer doInBackground(String... params) {
            try {
                String userInput=params[0];
                String queryString="http://maps.googleapis.com/maps/api/geocode/json?address=" + URLEncoder.encode(userInput, "utf8");
                URL url = new URL(queryString);
                jsonStringResponse=convertStreamToString(url.openConnection().getInputStream());
            } catch (Exception e) { e.printStackTrace(); }
            return null;
        }

        //called after do in background finished
        @Override
        protected void onPostExecute(Integer result) {
            try{
                //parse json from Google, clear previous json result
                locationJSONObj = new JSONObject(jsonStringResponse.toString());
                locationArrayList.clear();
                locationLatLng=new JSONArray();
                locationAddrComponent=new JSONArray();
                locationFormattedAddress=new JSONArray();

                //kill method if 0 results from server
                if (!locationJSONObj.getString("status").equals("OK")){
                    arrayAdapter.notifyDataSetChanged();
                    location_picker_suggestion_count.setText(0+"");
                    return;
                }

                locationJSONArray=new JSONArray(locationJSONObj.get("results").toString());
                for (int i = 0; i < locationJSONArray.length(); i++) {
                    locationArrayList.add(i, locationJSONArray.getJSONObject(i).getString("formatted_address"));

                    locationFormattedAddress.put(locationJSONArray.getJSONObject(i).getString("formatted_address"));
                    JSONObject tempJSON=locationJSONArray.getJSONObject(i).getJSONObject("geometry");
                    locationLatLng.put(tempJSON.getJSONObject("location"));
                    locationAddrComponent.put(locationJSONArray.getJSONObject(i).getJSONArray("address_components"));
                }

                //call to notify adapter data changed, refresh listview UI
                arrayAdapter.notifyDataSetChanged();
                location_picker_suggestion_count.setText(locationJSONArray.length()+"");

            }catch(JSONException jsone){
                jsone.printStackTrace();
            }
        }

        String convertStreamToString(java.io.InputStream is) {
            java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
            return s.hasNext() ? s.next() : "";
        }
    }

    //download list based on latlng
    class DownloadLatLngLocationList extends AsyncTask<Void,Void,Integer> {

        String jsonStringResponse;
        JSONObject locationJSONObj;
        JSONArray locationJSONArray;

        @Override
        protected void onPreExecute(){
            getCurrentLocationList();
        }

        @Override
        protected Integer doInBackground(Void... params) {
            try {
                double lat=currentLocation.latitude;
                double lng=currentLocation.longitude;

                String queryString="https://maps.googleapis.com/maps/api/geocode/json?latlng="+lat+","+lng+"&key="+getResources().getString(R.string.GoogleMapAPIKey);
                URL url = new URL(queryString);
                jsonStringResponse=convertStreamToString(url.openConnection().getInputStream());
            } catch (Exception e) { e.printStackTrace(); }
            return null;
        }

        //called after do in background finished
        @Override
        protected void onPostExecute(Integer result) {
            try{
                //parse json from Google, clear previous json result
                locationJSONObj = new JSONObject(jsonStringResponse.toString());
                locationArrayList.clear();
                locationLatLng=new JSONArray();
                locationAddrComponent=new JSONArray();
                locationFormattedAddress=new JSONArray();

                //kill method if 0 results from server
                if (!locationJSONObj.getString("status").equals("OK")){
                    arrayAdapter.notifyDataSetChanged();
                    location_picker_suggestion_count.setText(0 + "");
                    return;
                }

                locationJSONArray=new JSONArray(locationJSONObj.get("results").toString());
                for (int i = 0; i < locationJSONArray.length(); i++) {
                    locationArrayList.add(i, locationJSONArray.getJSONObject(i).getString("formatted_address"));

                    locationFormattedAddress.put(locationJSONArray.getJSONObject(i).getString("formatted_address"));
                    JSONObject tempJSON=locationJSONArray.getJSONObject(i).getJSONObject("geometry");
                    locationLatLng.put(tempJSON.getJSONObject("location"));
                    locationAddrComponent.put(locationJSONArray.getJSONObject(i).getJSONArray("address_components"));
                }

                //call to notify adapter data changed, refresh listview UI
                arrayAdapter.notifyDataSetChanged();
                location_picker_suggestion_count.setText(locationJSONArray.length()+"");

            }catch(JSONException jsone){
                jsone.printStackTrace();
            }
        }

        String convertStreamToString(java.io.InputStream is) {
            java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
            return s.hasNext() ? s.next() : "";
        }
    }
}
