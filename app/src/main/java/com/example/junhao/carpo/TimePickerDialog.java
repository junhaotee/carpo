package com.example.junhao.carpo;


import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Date;


public class TimePickerDialog extends DialogFragment {

    Button timepicker_dialog_savetime_btn;

    NumberPicker
            timepicker_dialog_timepicker_h,
            timepicker_dialog_timepicker_m,
            timepicker_dialog_timepicker_h_2,
            timepicker_dialog_timepicker_m_2;

    Spinner timepicker_dialog_timepicker_ampm,
            timepicker_dialog_timepicker_ampm_2;

    Switch timepicker_dialog_timepicker_is_interval,
            timepicker_dialog_timepicker_is_negotiable,
            timepicker_dialog_timepicker_is_anytime;

    LinearLayout timepicker_dialog_timepicker_timepicker2_ll;


    public TimePickerDialog() { }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);

        getDialog().setTitle("選擇出發時間");

        //listener for save time data button
        timepicker_dialog_savetime_btn=(Button)getView().findViewById(R.id.timepicker_dialog_savetime_btn);
        timepicker_dialog_savetime_btn.setOnClickListener(savetimeBtnListener);

        timepicker_dialog_timepicker_h=(NumberPicker)getView().findViewById(R.id.timepicker_dialog_timepicker_h);
        timepicker_dialog_timepicker_m=(NumberPicker)getView().findViewById(R.id.timepicker_dialog_timepicker_m);

        timepicker_dialog_timepicker_h.setMinValue(1);
        timepicker_dialog_timepicker_h.setMaxValue(12);

        timepicker_dialog_timepicker_m.setMinValue(0);
        timepicker_dialog_timepicker_m.setMaxValue(59);

        timepicker_dialog_timepicker_ampm=(Spinner)getView().findViewById(R.id.timepicker_dialog_timepicker_ampm);
        ArrayAdapter<String> ampmAdapter=new ArrayAdapter<String>(getContext(),android.R.layout.simple_list_item_1);
        timepicker_dialog_timepicker_ampm.setAdapter(ampmAdapter);

        ampmAdapter.add("A. M.");
        ampmAdapter.add("P. M.");

        timepicker_dialog_timepicker_h_2=(NumberPicker)getView().findViewById(R.id.timepicker_dialog_timepicker_h_2);
        timepicker_dialog_timepicker_m_2=(NumberPicker)getView().findViewById(R.id.timepicker_dialog_timepicker_m_2);

        timepicker_dialog_timepicker_h_2.setMinValue(1);
        timepicker_dialog_timepicker_h_2.setMaxValue(12);

        timepicker_dialog_timepicker_m_2.setMinValue(0);
        timepicker_dialog_timepicker_m_2.setMaxValue(59);


        timepicker_dialog_timepicker_ampm_2=(Spinner)getView().findViewById(R.id.timepicker_dialog_timepicker_ampm_2);
        ArrayAdapter<String> ampm2Adapter=new ArrayAdapter<String>(getContext(),android.R.layout.simple_list_item_1);
        timepicker_dialog_timepicker_ampm_2.setAdapter(ampmAdapter);

        ampm2Adapter.add("A. M.");
        ampm2Adapter.add("P. M.");

        ampmAdapter.notifyDataSetChanged();

        getDialog().setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                getTargetFragment().onActivityResult(-1, -1, null);
            }
        });

        timepicker_dialog_timepicker_is_interval=(Switch)getView().findViewById(R.id.timepicker_dialog_timepicker_is_interval);
        timepicker_dialog_timepicker_timepicker2_ll=(LinearLayout)getView().findViewById(R.id.timepicker_dialog_timepicker_timepicker2_ll);

        timepicker_dialog_timepicker_is_interval.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                int visibility=(isChecked)?View.VISIBLE:View.GONE;
                timepicker_dialog_timepicker_timepicker2_ll.setVisibility(visibility);
            }
        });

        timepicker_dialog_timepicker_is_negotiable=(Switch)getView().findViewById(R.id.timepicker_dialog_timepicker_is_negotiable);
        timepicker_dialog_timepicker_is_anytime=(Switch)getView().findViewById(R.id.timepicker_dialog_timepicker_is_anytime);

    }

    View.OnClickListener savetimeBtnListener=new View.OnClickListener(){
        @Override
        public void onClick(View v) {

            String ampm=timepicker_dialog_timepicker_ampm.getSelectedItem().toString();
            boolean isNegoAble=timepicker_dialog_timepicker_is_negotiable.isChecked();

            boolean isAnytime=timepicker_dialog_timepicker_is_anytime.isChecked();

            //if hour is PM and not 12, add 12 to value
            int h=Integer.parseInt(timepicker_dialog_timepicker_h.getValue()+"");
            int formattedH;
            if(ampm.equals("A. M.") && (timepicker_dialog_timepicker_h.getValue()+"").equals("12")){
                formattedH=0;
            }else if(ampm.equals("P. M.") && !(timepicker_dialog_timepicker_h.getValue()+"").equals("12")){
                formattedH=Integer.parseInt(timepicker_dialog_timepicker_h.getValue() + "")+12;
            }else{
                formattedH=Integer.parseInt(timepicker_dialog_timepicker_h.getValue()+"");
            }
            int m=Integer.parseInt(timepicker_dialog_timepicker_m.getValue()+"");

            Intent hmsData=new Intent();
            hmsData.putExtra("h",h);
            hmsData.putExtra("formattedH",formattedH);
            hmsData.putExtra("min",m);
            hmsData.putExtra("ampm", ampm);
            hmsData.putExtra("time_isNegoable",isNegoAble);
            hmsData.putExtra("time_isAnytime",isAnytime);
            hmsData.putExtra("interval_checked",timepicker_dialog_timepicker_is_interval.isChecked());


            if(timepicker_dialog_timepicker_is_interval.isChecked()){
                //convert all data to 24 hours and compare
                String ampm2=timepicker_dialog_timepicker_ampm_2.getSelectedItem().toString();

                //if hour is PM and not 12, add 12 to value, if is am and h==12, make it 0
                int h2=Integer.parseInt(timepicker_dialog_timepicker_h_2.getValue()+"");
                int formattedH2;
                if(ampm2.equals("A. M.") && (timepicker_dialog_timepicker_h_2.getValue()+"").equals("12")){
                    formattedH2=0;
                }else if(ampm2.equals("P. M.") && !(timepicker_dialog_timepicker_h_2.getValue()+"").equals("12")){
                    formattedH2=Integer.parseInt(timepicker_dialog_timepicker_h_2.getValue() + "")+12;
                }else{
                    formattedH2=Integer.parseInt(timepicker_dialog_timepicker_h_2.getValue()+"");
                }
                int m2 = Integer.parseInt(timepicker_dialog_timepicker_m_2.getValue()+"");

                //for example 23-22 > 0, 22-22=0
                boolean hour_correct=formattedH2-formattedH>=0;
                boolean min_correct=m2-m>=0;

                if(!hour_correct){
                    Toast.makeText(getContext(),"區間時間(時)不對",Toast.LENGTH_SHORT).show();
                    return;
                }

                if(h2-h==0 && !min_correct){
                    Toast.makeText(getContext(),"區間時間(分)不對",Toast.LENGTH_SHORT).show();
                    return;
                }

                hmsData.putExtra("h2",h2);
                hmsData.putExtra("formattedH2",formattedH2);
                hmsData.putExtra("m2",m2);
                hmsData.putExtra("ampm2", ampm2+"");
            }
            getTargetFragment().onActivityResult(getTargetRequestCode(),getTargetRequestCode(),hmsData);
            getDialog().dismiss();
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onPause(){
        super.onPause();
        dismiss();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_time_picker_dialog, container, false);
    }


}
