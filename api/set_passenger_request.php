<?php
	/* THE DATA RETRIEVE FROM THIS INTERFACE IS USED BY ApprovePassengerRequest */
	if(!isset($_GET["passenger_request_id"])) exit();
	
	require_once("../libcp/php/func.php");
	
	try {
		
		/* DATA PREPARACTION */
		$passenger_request_id		= $_GET["passenger_request_id"];
		$fk_carpool_id					= $_GET["fk_carpool_id"];
		$num_seat_requested			= $_GET["num_seat_requested"];
		$request_status					= ($_GET["request_status"]==0)?"driver_approved":"driver_declined";
		$fb									= $_GET["fb"];

		/* DATABASE MANIPUATION */
		$db=getDb();

		//No quotation for string needed when using PDO bind param, just use :param
		$stmt = $db->prepare("
		UPDATE 
			passenger_request 
		SET 
			date_driver_reply=now(),request_status=:request_status 
		WHERE 
			passenger_request_id=:passenger_request_id");

		$stmt->bindValue(':request_status', $request_status, PDO::PARAM_STR);
		$stmt->bindValue(':passenger_request_id', $passenger_request_id, PDO::PARAM_INT);

		$stmt->execute();
		
		if($stmt->rowCount()==0){
			exit();
		}
		
		//update seat number
		$stmt = $db->prepare("
		UPDATE 
			carpool 
		SET 
			seatTaken=seatTaken+:num_seat_requested 
		WHERE 
			cId=:fk_carpool_id");

		$stmt->bindValue(':num_seat_requested', $num_seat_requested, PDO::PARAM_INT);
		$stmt->bindValue(':fk_carpool_id', $fk_carpool_id, PDO::PARAM_INT);
		
		$stmt->execute();
		
		if($stmt->rowCount()==0){
			exit();
		}
		
		$stmt = $db->prepare("
		SELECT 
			* 
		FROM 
			passenger_request,carpool,user,hometown 
		WHERE 
			passenger_request.fk_carpool_id=carpool.cId AND 
			passenger_request.fk_user_fb=user.fb AND 
			user.fk_hometown_id=hometown.hometown_id AND 
			passenger_request.request_status='passenger_sent' AND 
			carpool.owner=:fb 
		ORDER BY 
			date_passenger_request 
		DESC");

		$stmt->bindValue(':fb', $fb, PDO::PARAM_STR);
		
		/* ECHO NEW PASSENGER REQUEST, FOR UPDATE NEW PASSENGER REQUEST IN LISTVIEW */
		$stmt->execute();
		$results=$stmt->fetchAll(PDO::FETCH_ASSOC);
		print json_encode($results,JSON_UNESCAPED_UNICODE);
		
	} 
	catch(PDOException $ex) {
		/* EXCEPTION LOGGING */
		try{
			
			/* DATA PREPRATION */
			$exception_page="api/set_passenger_request.php";
			$exception_section="first try block";
			$exception_msg=$ex->getMessage();
			
			/* DATABASE MANIPULATION */
			$db=getDb();
			$stmt = $db->prepare("INSERT INTO exception(exception_page,exception_section,exception_msg) VALUES(:exception_page,:exception_section,:exception_msg)");
			
			$stmt->bindValue(':exception_page', $exception_page, PDO::PARAM_STR);
			$stmt->bindValue(':exception_section', $exception_section, PDO::PARAM_STR);
			$stmt->bindValue(':exception_msg', $exception_msg, PDO::PARAM_STR);

			$stmt->execute();
			
			echo ""; //echoes nothing if error happens

		}
		catch(PDOException $ex) { /*does nothing*/ }
	}


?>