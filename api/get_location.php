<?php
	/* THE DATA RETRIEVE FROM THIS INTERFACE IS USED BY ApprovePassengerRequestItemAdapter */

	if(!isset($_GET["postcode"])) exit();
	
	require_once("../libcp/php/func.php");
	
	try {
		
		/* DATA PREPARACTION */
		$town_id=$_GET["postcode"];

		/* DATABASE MANIPUATION */
		$db=getDb();

		$stmt = $db->prepare("
		SELECT 
			c.city_name,t.town_name 
		FROM 
			city c,town t 
		WHERE 
			c.id=t.city AND 
			t.town_id=:town_id");

		$stmt->bindValue(':town_id', $town_id, PDO::PARAM_INT);
		
		/* ECHO STATUS */
		$stmt->execute();
		$results=$stmt->fetchAll(PDO::FETCH_ASSOC);
		print json_encode($results,JSON_UNESCAPED_UNICODE);
		
		
	} 
	catch(PDOException $ex) {
		/* EXCEPTION LOGGING */
		try{
			
			/* DATA PREPRATION */
			$exception_page="api/get_location.php";
			$exception_section="first try block";
			$exception_msg=$ex->getMessage();
			
			/* DATABASE MANIPULATION */
			$db=getDb();
			$stmt = $db->prepare("INSERT INTO exception(exception_page,exception_section,exception_msg) VALUES(:exception_page,:exception_section,:exception_msg)");
			
			$stmt->bindValue(':exception_page', $exception_page, PDO::PARAM_STR);
			$stmt->bindValue(':exception_section', $exception_section, PDO::PARAM_STR);
			$stmt->bindValue(':exception_msg', $exception_msg, PDO::PARAM_STR);

			$stmt->execute();
			
			echo ""; //echoes nothing if error happens

		}
		catch(PDOException $ex) { /*does nothing*/ }
	}


?>

