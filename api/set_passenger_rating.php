<?php
	/* THE DATA RETRIEVE FROM THIS INTERFACE IS USED BY ViewPassengerHistory */
	if(!isset($_GET["user_given_rating"])) exit();
	
	require_once("../libcp/php/func.php");
	
	try {
		
		/* DATA PREPARACTION */
		$fk_carpool_id				=	$_GET["fk_carpool_id"];
		$user_comment				=	$_GET["user_comment"];
		$fk_user_fb_rates			=	$_GET["fk_user_fb_rates"];
		$fk_user_fb_rated			=	$_GET["fk_user_fb_rated"];
		$user_given_rating			=	$_GET["user_given_rating"];
		$fb								=	$_GET["fk_user_fb_rated"];
		$passenger_request_id	=	$_GET["passenger_request_id"];

		/* DATABASE MANIPUATION */
		$db	=	getDb();
		
		//New rating insert to user_rating table
		$stmt = $db->prepare("
		INSERT INTO 
			user_rating(fk_carpool_id,user_comment,fk_user_fb_rates,fk_user_fb_rated,user_given_rating) 
		VALUES 
			(:fk_carpool_id,:user_comment,:fk_user_fb_rates,:fk_user_fb_rated,:user_given_rating)");

		$stmt->bindValue(':fk_carpool_id', $fk_carpool_id, PDO::PARAM_INT);
		$stmt->bindValue(':user_comment', $user_comment, PDO::PARAM_STR);
		$stmt->bindValue(':fk_user_fb_rates', $fk_user_fb_rates, PDO::PARAM_STR);
		$stmt->bindValue(':fk_user_fb_rated', $fk_user_fb_rated, PDO::PARAM_STR);
		$stmt->bindValue(':user_given_rating', $user_given_rating, PDO::PARAM_STR);

		$stmt->execute();
		
		if($stmt->rowCount()==0){
			exit();
		}
		
		//Update passenger_request to mark that the carpool has been rated
		$stmt = $db->prepare("
		UPDATE 
			passenger_request 
		SET 
			fk_user_rating_id_on_passenger=(
				SELECT 
					max(user_rating_id) 
				FROM 
					user_rating) 
		WHERE 
			passenger_request_id=:passenger_request_id");
		
		$stmt->bindValue(':passenger_request_id', $passenger_request_id, PDO::PARAM_INT);
		
		$stmt->execute();
		
		if($stmt->rowCount()==0){
			exit();
		}
		
		//Update user table for user rating count accordingly
		$stmt = $db->prepare("UPDATE 
		user 
		SET ".$user_given_rating."_count=".$user_given_rating."_count+1 
		WHERE 
		fb=:fb");

		$stmt->bindValue(':fb', $fb, PDO::PARAM_STR);		
		$stmt->execute();
		
		if($stmt->rowCount()==0){
			exit();
		}
		

		//Get passenger history
		$stmt = $db->prepare("
		SELECT 
			* 
		FROM 
			passenger_request,carpool,user,hometown,user_rating where user_rating.user_rating_id=passenger_request.fk_user_rating_id_on_passenger AND 
			(carpool.timeDepart2 < now() or  passenger_request.request_status!='passenger_sent' ) AND 
			passenger_request.fk_carpool_id=carpool.cId AND 
			passenger_request.fk_user_fb=user.fb AND 
			user.fk_hometown_id=hometown.hometown_id AND 
			carpool.owner=:fb 
		ORDER BY 
			date_passenger_request 
		DESC");

		$stmt->bindValue(':fb', $fb, PDO::PARAM_STR);
		
		/* ECHO STATUS */
		$stmt->execute();
		$results=$stmt->fetchAll(PDO::FETCH_ASSOC);
		print json_encode($results,JSON_UNESCAPED_UNICODE);
		
	} 
	catch(PDOException $ex) {
		/* EXCEPTION LOGGING */
		try{
			
			/* DATA PREPRATION */
			$exception_page="api/set_passenger_rating.php";
			$exception_section="first try block";
			$exception_msg=$ex->getMessage();
			
			/* DATABASE MANIPULATION */
			$db=getDb();
			$stmt = $db->prepare("INSERT INTO exception(exception_page,exception_section,exception_msg) VALUES(:exception_page,:exception_section,:exception_msg)");
			
			$stmt->bindValue(':exception_page', $exception_page, PDO::PARAM_STR);
			$stmt->bindValue(':exception_section', $exception_section, PDO::PARAM_STR);
			$stmt->bindValue(':exception_msg', $exception_msg, PDO::PARAM_STR);

			$stmt->execute();
			
			echo ""; //echoes nothing if error happens

		}
		catch(PDOException $ex) { /*does nothing*/ }
	}


?>