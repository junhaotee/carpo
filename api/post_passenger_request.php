<?php

	if(!isset($_POST["fk_carpool_id"])) exit();
	
	require_once("../libcp/php/func.php");
	
	try {
		
		/* DATA PREPARACTION */
		$fk_carpool_id			= $_POST["fk_carpool_id"];
		$fk_user_fb				= $_POST["fk_user_fb"];
		$num_seat_requested	= $_POST["num_seat_requested"];

		/* DATABASE MANIPUATION */
		$db=getDb();
		$stmt = $db->prepare("
		INSERT INTO 
			passenger_request(fk_carpool_id,fk_user_fb,num_seat_requested) 
		VALUES
			(:fk_carpool_id,:fk_user_fb,:num_seat_requested)");

		$stmt->bindValue(':fk_carpool_id', $fk_carpool_id, PDO::PARAM_INT);
		$stmt->bindValue(':fk_user_fb', $fk_user_fb, PDO::PARAM_STR);
		$stmt->bindValue(':num_seat_requested', $num_seat_requested, PDO::PARAM_INT);
		
		/* ECHO STATUS */
		$responseMsg=($stmt->execute())?"0":"1";
		echo $responseMsg;
		
	} 
	catch(PDOException $ex) {
		/* EXCEPTION LOGGING */
		try{
			
			/* DATA PREPRATION */
			$exception_page="api/post_passsenger_request.php";
			$exception_section="first try block";
			$exception_msg=$ex->getMessage();
			
			/* DATABASE MANIPULATION */
			$db=getDb();
			$stmt = $db->prepare("INSERT INTO exception(exception_page,exception_section,exception_msg) VALUES(:exception_page,:exception_section,:exception_msg)");
			
			$stmt->bindValue(':exception_page', $exception_page, PDO::PARAM_STR);
			$stmt->bindValue(':exception_section', $exception_section, PDO::PARAM_STR);
			$stmt->bindValue(':exception_msg', $exception_msg, PDO::PARAM_STR);

			$stmt->execute();
			
			echo "1"; //let known of a failed insert

		}
		catch(PDOException $ex) { /*does nothing*/ }
	}


?>