<?php
	/* THE DATA RETRIEVE FROM THIS INTERFACE IS USED BY ViewPassengerHistory */
	if(!isset($_GET["startLng"])) exit();
	
	require_once("../libcp/php/func.php");
	
	try {
		
		/* DATA PREPARACTION */
		$start					= $_GET["start"];
		$dst						= $_GET["dst"];
		$timeDepart			= $_GET["timeDepart"];
		$startLat				= $_GET["startLat"];
		$startLng				= $_GET["startLng"];
		$dstLat					= $_GET["dstLat"];
		$dstLng					= $_GET["dstLng"];
		$currentItemOffset	= $_GET["currentItemOffset"];

		/* DATABASE MANIPUATION */
		$db	=	getDb();
		
		//get related result based on coordinates
		$stmt = $db->prepare("
		SELECT 
			carpool.*,
			user.*,
			hometown.*,
			vehicletype.vehicle as vehicle_name,
			c1.city_name as src_city_name,
			t1.town_name as src_town_name,
			c2.city_name as dst_city_name,
			t2.town_name as dst_town_name 
 		FROM 
			carpool,
			user,
			vehicletype,
			city as c1,
			city as c2,
			town as t1,
			town as t2,
			hometown
		WHERE 
			user.fk_hometown_id=hometown.hometown_id AND
			carpool.startPostcode=t1.town_id AND
			t1.city=c1.id AND
			carpool.destinationPostcode=t2.town_id AND
			t2.city=c2.id AND
			carpool.owner=user.fb AND
			carpool.startlat < :startLat1+0.02 AND carpool.startlat > :startLat2-0.02 AND
			carpool.startlng < :startLng1+0.02 AND carpool.startlng > :startLng2-0.02 AND
			carpool.dstlat < :dstLat1+0.02 AND carpool.dstlat > :dstLat2-0.02 AND
			carpool.dstlng < :dstLng1+0.02 AND carpool.dstlng > :dstLng2-0.02 AND
			carpool.cType=vehicletype.id
		limit :currentItemOffset,5
		");

		$stmt->bindValue(':startLat1', $startLat, PDO::PARAM_STR);
		$stmt->bindValue(':startLat2', $startLat, PDO::PARAM_STR);

		$stmt->bindValue(':startLng1', $startLng, PDO::PARAM_STR);
		$stmt->bindValue(':startLng2', $startLng, PDO::PARAM_STR);
		
		$stmt->bindValue(':dstLat1', $dstLat, PDO::PARAM_STR);
		$stmt->bindValue(':dstLat2', $dstLat, PDO::PARAM_STR);
		
		$stmt->bindValue(':dstLng1', $dstLng, PDO::PARAM_STR);
		$stmt->bindValue(':dstLng2', $dstLng, PDO::PARAM_STR);
		
		$stmt->bindValue(':currentItemOffset', $currentItemOffset, PDO::PARAM_INT);

		$stmt->execute();

		$results=$stmt->fetchAll(PDO::FETCH_ASSOC);
		print json_encode($results,JSON_UNESCAPED_UNICODE);
		
	}
	catch(PDOException $ex) {
		/* EXCEPTION LOGGING */
		try{
			
			/* DATA PREPRATION */
			$exception_page="api/get_carpool_result.php";
			$exception_section="first try block";
			$exception_msg=$ex->getMessage();
			
			/* DATABASE MANIPULATION */
			$db=getDb();
			$stmt = $db->prepare("INSERT INTO exception(exception_page,exception_section,exception_msg) VALUES(:exception_page,:exception_section,:exception_msg)");
			
			$stmt->bindValue(':exception_page', $exception_page, PDO::PARAM_STR);
			$stmt->bindValue(':exception_section', $exception_section, PDO::PARAM_STR);
			$stmt->bindValue(':exception_msg', $exception_msg, PDO::PARAM_STR);

			$stmt->execute();
			
			echo ""; //echoes nothing if error happens

		}
		catch(PDOException $ex) { /*does nothing*/ }
	}


?>