<?php
	/* THE DATA RETRIEVE FROM THIS INTERFACE IS USED BY ApprovePassengerRequest */
	if(!isset($_GET["passenger_request_id"])) exit();
	
	require_once("../libcp/php/func.php");
	
	try {
		
		/* DATA PREPARACTION */
		$passenger_request_id	= $_GET["passenger_request_id"];
		$cId								= $_GET["cId"];
		$num_seat_requested		= $_GET["num_seat_requested"];
		$owner							= $_GET["owner"];
		$fb								= $_GET["fb"];

		/* DATABASE MANIPUATION */
		$db=getDb();

		//Set request_status to 'driver_canceled'
		$stmt = $db->prepare("
		UPDATE 
			passenger_request 
		SET 
			request_status='driver_canceled',date_driver_reply=now() 
		WHERE 
			passenger_request_id=:passenger_request_id");
			
		$stmt->bindValue(':passenger_request_id', $passenger_request_id, PDO::PARAM_INT);
		$stmt->execute();
		
		if($stmt->rowCount()==0){
			exit();
		}
		
		//Reduce taken seat accordingly
		$stmt = $db->prepare("
		UPDATE 
			carpool 
		SET 
			seatTaken=seatTaken-:num_seat_requested 
		WHERE 
			cId=:cId");
		$stmt->bindValue(':num_seat_requested', $num_seat_requested, PDO::PARAM_INT);
		$stmt->bindValue(':cId', $cId, PDO::PARAM_INT);

		$stmt->execute();
		
		if($stmt->rowCount()==0){
			exit();
		}
		
		//Increment cancel_passenger_count by 1, for the record
		$stmt = $db->prepare("
		UPDATE 
			user 
		SET 
			cancel_passenger_count=cancel_passenger_count+1 
		WHERE 
			fb=:owner");
			
		$stmt->bindValue(':owner', $owner, PDO::PARAM_STR);
		$stmt->execute();
		
		if($stmt->rowCount()==0){
			exit();
		}
		
		/* Echo list of passenger request that not yet proceed by drivers */
		
		$stmt = $db->prepare("
		SELECT 
			* 
		FROM 
			passenger_request,carpool,user,hometown 
		WHERE 
			carpool.timeDepart>now()  AND 
			passenger_request.fk_carpool_id=carpool.cId AND
			passenger_request.fk_user_fb=user.fb AND 
			user.fk_hometown_id=hometown.hometown_id AND 
			passenger_request.request_status='driver_approved' AND 
			carpool.owner=:fb 
		ORDER BY 
			date_passenger_request 
		DESC");

		$stmt->bindValue(':fb', $fb, PDO::PARAM_STR);
		
		/* ECHO NEW PASSENGER REQUEST, FOR UPDATE NEW PASSENGER REQUEST IN LISTVIEW */
		$stmt->execute();
		$results=$stmt->fetchAll(PDO::FETCH_ASSOC);
		print json_encode($results,JSON_UNESCAPED_UNICODE);
		
	} 
	catch(PDOException $ex) {
		/* EXCEPTION LOGGING */
		try{
			
			/* DATA PREPRATION */
			$exception_page="api/cancel_passenger_request.php";
			$exception_section="first try block";
			$exception_msg=$ex->getMessage();
			
			/* DATABASE MANIPULATION */
			$db=getDb();
			$stmt = $db->prepare("INSERT INTO exception(exception_page,exception_section,exception_msg) VALUES(:exception_page,:exception_section,:exception_msg)");
			
			$stmt->bindValue(':exception_page', $exception_page, PDO::PARAM_STR);
			$stmt->bindValue(':exception_section', $exception_section, PDO::PARAM_STR);
			$stmt->bindValue(':exception_msg', $exception_msg, PDO::PARAM_STR);

			$stmt->execute();
			
			echo ""; //echoes nothing if error happens

		}
		catch(PDOException $ex) { /*does nothing*/ }
	}


?>