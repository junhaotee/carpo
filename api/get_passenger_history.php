<?php
	/* THE DATA RETRIEVE FROM THIS INTERFACE IS USED BY ApprovePassengerRequestItemAdapter */

	if(!isset($_GET["fb"])) exit();
	
	require_once("../libcp/php/func.php");
	
	try {
		
		/* DATA PREPARACTION */
		$fb=$_GET["fb"];

		/* DATABASE MANIPUATION */
		$db=getDb();

		$stmt = $db->prepare("
		SELECT 
			* 
		FROM 
			passenger_request,carpool,user,hometown,user_rating 
		WHERE 
			user_rating.user_rating_id=passenger_request.fk_user_rating_id_on_passenger AND 
			(carpool.timeDepart2 < now() OR  passenger_request.request_status!='passenger_sent' ) AND 
			passenger_request.fk_carpool_id=carpool.cId AND 
			passenger_request.fk_user_fb=user.fb AND 
			user.fk_hometown_id=hometown.hometown_id AND 
			carpool.owner=:fb 
		ORDER BY 
			date_passenger_request 
		DESC");

		$stmt->bindValue(':fb', $fb, PDO::PARAM_STR);
		
		/* ECHO STATUS */
		$stmt->execute();
		$results=$stmt->fetchAll(PDO::FETCH_ASSOC);
		print json_encode($results,JSON_UNESCAPED_UNICODE);
		
		
	} 
	catch(PDOException $ex) {
		/* EXCEPTION LOGGING */
		try{
			
			/* DATA PREPRATION */
			$exception_page="api/get_passenger_request.php";
			$exception_section="first try block";
			$exception_msg=$ex->getMessage();
			
			/* DATABASE MANIPULATION */
			$db=getDb();
			$stmt = $db->prepare("INSERT INTO exception(exception_page,exception_section,exception_msg) VALUES(:exception_page,:exception_section,:exception_msg)");
			
			$stmt->bindValue(':exception_page', $exception_page, PDO::PARAM_STR);
			$stmt->bindValue(':exception_section', $exception_section, PDO::PARAM_STR);
			$stmt->bindValue(':exception_msg', $exception_msg, PDO::PARAM_STR);

			$stmt->execute();
			
			echo ""; //echoes nothing if error happens

		}
		catch(PDOException $ex) { /*does nothing*/ }
	}


?>