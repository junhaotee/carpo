<?php
	/* THE DATA RETRIEVE FROM THIS INTERFACE IS USED BY ApprovePassengerRequestItemAdapter */
	if(!isset($_GET["fk_user_fb_rated"])) exit();
	
	require_once("../libcp/php/func.php");
	
	try {
		
		/* DATA PREPARACTION */
		$fk_user_fb_rated			= $_GET["fk_user_fb_rated"];
		$currentItemOffset			= $_GET["currentItemOffset"];
		$user_given_rating_0		= "";
		$user_given_rating_1		= "";
		$user_given_rating_2		= "";
		
		if(strcmp($_GET["role"],"p")==0){
			$user_given_rating_0	= "p_rating_good";
			$user_given_rating_1	= "p_rating_average";
			$user_given_rating_2	= "p_rating_bad";
		}
		
		if(strcmp($_GET["role"],"d")==0){
			$user_given_rating_0	= "d_rating_good";
			$user_given_rating_1	= "d_rating_average";
			$user_given_rating_2	= "d_rating_bad";
		}
	
		/* DATABASE MANIPUATION */
		$db=getDb();

		$stmt = $db->prepare("
		SELECT 
			* 
		FROM 
			user_rating,user 
		WHERE 
			user_rating.fk_user_fb_rates=user.fb AND 
			user_rating.user_given_rating in(:user_given_rating_0,:user_given_rating_1,:user_given_rating_2) AND 
			user_rating.fk_user_fb_rated=:fk_user_fb_rated 
		ORDER BY 
			user_rating_timestamp 
		DESC 
		LIMIT 
		:currentItemOffset,5");

		$stmt->bindValue(':user_given_rating_0', $user_given_rating_0, PDO::PARAM_STR);
		$stmt->bindValue(':user_given_rating_1', $user_given_rating_1, PDO::PARAM_STR);
		$stmt->bindValue(':user_given_rating_2', $user_given_rating_2, PDO::PARAM_STR);

		$stmt->bindValue(':fk_user_fb_rated', $fk_user_fb_rated, PDO::PARAM_STR);
		$stmt->bindValue(':currentItemOffset', $currentItemOffset, PDO::PARAM_INT);

		
		/* ECHO STATUS */
		$stmt->execute();
		$results=$stmt->fetchAll(PDO::FETCH_ASSOC);
		print json_encode($results,JSON_UNESCAPED_UNICODE);
		
		
	} 
	catch(PDOException $ex) {
		/* EXCEPTION LOGGING */
		try{
			
			/* DATA PREPRATION */
			$exception_page="api/get_user_rating.php";
			$exception_section="first try block";
			$exception_msg=$ex->getMessage();
			
			/* DATABASE MANIPULATION */
			$db=getDb();
			$stmt = $db->prepare("INSERT INTO exception(exception_page,exception_section,exception_msg) VALUES(:exception_page,:exception_section,:exception_msg)");
			
			$stmt->bindValue(':exception_page', $exception_page, PDO::PARAM_STR);
			$stmt->bindValue(':exception_section', $exception_section, PDO::PARAM_STR);
			$stmt->bindValue(':exception_msg', $exception_msg, PDO::PARAM_STR);

			$stmt->execute();
			
			echo ""; //echoes nothing if error happens

		}
		catch(PDOException $ex) { /*does nothing*/ }
	}


?>