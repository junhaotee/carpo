 <?php
	/* THE DATA RETRIEVE FROM THIS INTERFACE IS USED BY ViewPassengerHistory */
	if(!isset($_POST["owner"])) exit();
	
	require_once("../libcp/php/func.php");
	
	try {
		
		/* DATA PREPARACTION */
		$owner						= $_POST["owner"];
		$start						= $_POST["start"];
		$destination				= $_POST["destination"];
		$timeDepart				= $_POST["timeDepart"];
		$costpp						= $_POST["costpp"];
		$comment					= $_POST["comment"];
		$startlat						= $_POST["startlat"];
		$startlng					= $_POST["startlng"];
		$dstlng						= $_POST["dstlng"];
		$dstlat						= $_POST["dstlat"];
		$seatTaken				= $_POST["seatTaken"];
		$cType						= $_POST["cType"];
		$seatGiven					= $_POST["seatGiven"];
		$startPostcode			= $_POST["startPostcode"];
		$destinationPostcode	= $_POST["destinationPostcode"];
		$srcFormattedAddress	= $_POST["srcFormattedAddress"];
		$dstFormattedAddress	= $_POST["dstFormattedAddress"];
		
		
		//need to convert string 'false' to 1 and true to 0
    	$hasInterval				=(strcmp($_POST["hasInterval"],"true"))?"0":"1";
		$timeNego					=(strcmp($_POST["timeNego"],"true"))?"0":"1";
		$isFoodAllowed			=(strcmp($_POST["isFoodAllowed"],"true"))?"0":"1";
		$isBigLuggageAllowed	=(strcmp($_POST["isBigLuggageAllowed"],"true"))?"0":"1";
		$isStopByAllowed		=(strcmp($_POST["isStopByAllowed"],"true"))?"0":"1";
		$isPaymentAfterBefore=(strcmp($_POST["isPaymentAfterBefore"],"true"))?"0":"1";
		$isIdentityCheckNeeded=(strcmp($_POST["isIdentityCheckNeeded"],"true"))?"0":"1";
		$isCostNego				=(strcmp($_POST["isCostNego"],"true"))?"0":"1";
		$isAnytime					=(strcmp($_POST["isAnytime"],"true"))?"0":"1";
		
		$timeDepart2				=$_POST["timeDepart2"];

		/* DATABASE MANIPUATION */
		$db	=	getDb();
		
		//New rating insert to user_rating table
		$stmt=$db->prepare("
		INSERT INTO carpool(
			created,
			owner,
			start,
			destination,
			timeDepart,
			costpp,
			comment,
			startlat,
			startlng,
			dstlng,
			dstlat,
			seatTaken,
			cType,
			timeNego,
			seatGiven,
			hasInterval,
			startPostcode,
			destinationPostcode,
			isFoodAllowed,
			isBigLuggageAllowed,
			isStopByAllowed,
			isPaymentAfterBefore,
			isIdentityCheckNeeded,
			timeDepart2,
			srcFormattedAddress,
			dstFormattedAddress,
			isCostNego,
			isAnytime
			) 
		VALUES(
			NOW(),
			:owner,
			:start,
			:destination,
			:timeDepart,
			:costpp,
			:comment,
			:startlat,
			:startlng,
			:dstlng,
			:dstlat,
			:seatTaken,
			:cType,
			:timeNego,
			:seatGiven,
			:hasInterval,
			:startPostcode,
			:destinationPostcode,
			:isFoodAllowed,
			:isBigLuggageAllowed,
			:isStopByAllowed,
			:isPaymentAfterBefore,
			:isIdentityCheckNeeded,
			:timeDepart2,
			:srcFormattedAddress,
			:dstFormattedAddress,
			:isCostNego,
			:isAnytime
			)");
		
		$stmt->bindValue(':owner', $owner, PDO::PARAM_STR);
		$stmt->bindValue(':start', $start, PDO::PARAM_STR);
		$stmt->bindValue(':destination', $destination, PDO::PARAM_STR);
		$stmt->bindValue(':timeDepart', $timeDepart, PDO::PARAM_STR);
		$stmt->bindValue(':costpp', $costpp, PDO::PARAM_INT);
		$stmt->bindValue(':comment', $comment, PDO::PARAM_STR);
		$stmt->bindValue(':startlat', $startlat, PDO::PARAM_STR);
		$stmt->bindValue(':startlng', $startlng, PDO::PARAM_STR);
		$stmt->bindValue(':dstlng', $dstlng, PDO::PARAM_STR);
		$stmt->bindValue(':dstlat', $dstlat, PDO::PARAM_STR);
		$stmt->bindValue(':seatTaken', $seatTaken, PDO::PARAM_INT);
		$stmt->bindValue(':cType', $cType,PDO::PARAM_INT);
		$stmt->bindValue(':timeNego', $timeNego,PDO::PARAM_INT);
		$stmt->bindValue(':seatGiven', $seatGiven,PDO::PARAM_INT);
		$stmt->bindValue(':hasInterval', $hasInterval,PDO::PARAM_INT);
		$stmt->bindValue(':startPostcode', $startPostcode,PDO::PARAM_INT);
		$stmt->bindValue(':destinationPostcode', $destinationPostcode,PDO::PARAM_INT);
		$stmt->bindValue(':isFoodAllowed', $isFoodAllowed,PDO::PARAM_INT);
		$stmt->bindValue(':isBigLuggageAllowed', $isBigLuggageAllowed,PDO::PARAM_INT);
		$stmt->bindValue(':isStopByAllowed', $isStopByAllowed,PDO::PARAM_INT);
		$stmt->bindValue(':isPaymentAfterBefore', $isPaymentAfterBefore,PDO::PARAM_INT);
		$stmt->bindValue(':isIdentityCheckNeeded', $isIdentityCheckNeeded,PDO::PARAM_INT);
		$stmt->bindValue(':timeDepart2', $timeDepart2,PDO::PARAM_STR);
		$stmt->bindValue(':srcFormattedAddress', $srcFormattedAddress,PDO::PARAM_STR);
		$stmt->bindValue(':dstFormattedAddress', $dstFormattedAddress,PDO::PARAM_STR);
		$stmt->bindValue(':isCostNego', $isCostNego,PDO::PARAM_INT);
		$stmt->bindValue(':isAnytime', $isAnytime,PDO::PARAM_INT);

		$stmt->execute();
		
		if($stmt->rowCount()>0){
			echo "0";
		}
		
		
	} 
	catch(PDOException $ex) {
		/* EXCEPTION LOGGING */
		try{
			
			/* DATA PREPRATION */
			$exception_page="api/set_passenger_rating.php";
			$exception_section="first try block";
			$exception_msg=$ex->getMessage();
			
			/* DATABASE MANIPULATION */
			$db=getDb();
			$stmt = $db->prepare("INSERT INTO exception(exception_page,exception_section,exception_msg) VALUES(:exception_page,:exception_section,:exception_msg)");
			
			$stmt->bindValue(':exception_page', $exception_page, PDO::PARAM_STR);
			$stmt->bindValue(':exception_section', $exception_section, PDO::PARAM_STR);
			$stmt->bindValue(':exception_msg', $exception_msg, PDO::PARAM_STR);

			$stmt->execute();
			
			echo ""; //echoes nothing if error happens

		}
		catch(PDOException $ex) { /*does nothing*/ }
	}
	catch(Exception $e) {
		/* EXCEPTION LOGGING */
		try{
			
			/* DATA PREPRATION */
			$exception_page="api/set_passenger_rating.php";
			$exception_section="first try block";
			$exception_msg=$e->getMessage();
			
			/* DATABASE MANIPULATION */
			$db=getDb();
			$stmt = $db->prepare("INSERT INTO exception(exception_page,exception_section,exception_msg) VALUES(:exception_page,:exception_section,:exception_msg)");
			
			$stmt->bindValue(':exception_page', $exception_page, PDO::PARAM_STR);
			$stmt->bindValue(':exception_section', $exception_section, PDO::PARAM_STR);
			$stmt->bindValue(':exception_msg', $exception_msg, PDO::PARAM_STR);

			$stmt->execute();
			
			echo ""; //echoes nothing if error happens

		}
		catch(Exception $e) { /*does nothing*/ }
	}


?>